# @rainbird/sdk-js

[![pipeline status](https://gitlab.com/rainbird-ai/sdk-js/badges/master/pipeline.svg)](https://gitlab.com/rainbird-ai/sdk-js/badges/master/pipeline.svg)   [![coverage status](https://gitlab.com/rainbird-ai/sdk-js/badges/master/coverage.svg)](https://gitlab.com/rainbird-ai/sdk-js/badges/master/coverage.svg)

The rainbird sdk is made up of the following three parts:
- [@rainbird/sdk](https://gitlab.com/rainbird-ai/sdk-js/-/tree/master/packages/base)
  - Pure JS functions to call the api.
  - Recommended for bespoke front-end solutions
- [@rainbird/sdk-react](https://gitlab.com/rainbird-ai/sdk-js/-/tree/master/packages/react)
  - Builds on @rainbird/sdk. Includes hooks, decorators and components to integrate Rainbird into React.
  - Recommended for bespoke React integrations.
- [@rainbird/sdk-react-material](https://gitlab.com/rainbird-ai/sdk-js/-/tree/master/packages/react-material)
  - Builds on @rainbird/sdk-react. Includes themable form components, interaction, and kitchen sink using material-ui.
  - Recommended to get started quickly.

