export const RESPONSE_TYPE_QUESTION = Symbol("RESPONSE_TYPE_QUESTION");
export const RESPONSE_TYPE_RESULT = Symbol("RESPONSE_TYPE_RESULT");
export const RESPONSE_TYPE_UNKNOWN = Symbol("RESPONSE_TYPE_UNKNOWN");

export function parse(data) {
  if (data.question !== undefined) {
    return {
      type: RESPONSE_TYPE_QUESTION,
      data: {
        question: data.question,
        extraQuestions: data.extraQuestions || [],
      },
    };
  }
  if (data.result !== undefined) {
    return {
      type: RESPONSE_TYPE_RESULT,
      data: data.result,
    };
  }
  return { type: RESPONSE_TYPE_UNKNOWN, data };
}
