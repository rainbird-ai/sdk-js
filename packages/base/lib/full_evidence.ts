import { evidence } from "./evidence";
import {
  OptionsEvidence,
  EvidenceResponse,
  isConditionRelationship,
} from "./domain";

const factMap: Map<string, EvidenceResponse> = new Map();

const buildFacts = async (
  baseURL: string,
  sessionID: string,
  factID: string,
  options: OptionsEvidence = {
    engine: undefined,
    evidenceKey: undefined,
  },
): Promise<any> => {
  const factEvidence = await evidence(baseURL, sessionID, factID, options);
  if (!factEvidence) return [];
  if (factMap.has(factEvidence.factID)) return [];

  factMap.set(factEvidence.factID, factEvidence);

  if (!factEvidence.rule) return [];

  const promisedFacts = factEvidence.rule.conditions.map(async (item) => {
    if (isConditionRelationship(item)) {
      return buildFacts(baseURL, sessionID, item.factID, options);
    }
    return [];
  });

  await Promise.all(promisedFacts);
  return promisedFacts;
};

export const fullEvidence = async (
  baseURL: string,
  sessionID: string,
  factID: string,
  options = {},
): Promise<EvidenceResponse[]> => {
  await buildFacts(baseURL, sessionID, factID, options);
  return Array.from(factMap.values());
};
