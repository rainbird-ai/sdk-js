import "core-js/stable";
import "regenerator-runtime/runtime";

import { query } from "../query";
import {
  RESPONSE_TYPE_QUESTION,
  RESPONSE_TYPE_RESULT,
} from "../response_types";

afterEach(() => {
  global.fetch.mockClear();
});

describe("query", () => {
  it("should make a simple query for object", async () => {
    global.fetch = jest.fn(async () => ({
      ok: true,
      status: 200,
      json: async () => ({
        result: { foo: "bar" },
      }),
    }));

    const result = await query(
      "thisistheurl",
      "thisisthesessionid",
      "thisisthesubject",
      "thisistherelationship",
      null,
    );
    expect(global.fetch).toHaveBeenCalledTimes(1);
    const call = global.fetch.mock.calls[0][1];
    expect(Object.keys(call).sort()).toEqual(["body", "headers", "method"]);
    expect(call.method).toEqual("POST");
    expect(call.headers).toMatchObject({ "Content-Type": "application/json" });
    expect(JSON.parse(call.body)).toMatchObject({
      subject: "thisisthesubject",
      relationship: "thisistherelationship",
    });
    expect(result).toEqual({
      type: RESPONSE_TYPE_RESULT,
      data: { foo: "bar" },
    });
  });

  it("should make a simple query for subject", async () => {
    global.fetch = jest.fn(async () => ({
      ok: true,
      status: 200,
      json: async () => ({
        result: { foo: "bar" },
      }),
    }));

    const result = await query(
      "thisistheurl",
      "thisisthesessionid",
      null,
      "thisistherelationship",
      "thisistheobject",
    );
    expect(global.fetch).toHaveBeenCalledTimes(1);
    const call = global.fetch.mock.calls[0][1];
    expect(Object.keys(call).sort()).toEqual(["body", "headers", "method"]);
    expect(call.method).toEqual("POST");
    expect(call.headers).toMatchObject({ "Content-Type": "application/json" });
    expect(JSON.parse(call.body)).toMatchObject({
      relationship: "thisistherelationship",
      object: "thisistheobject",
    });
    expect(result).toEqual({
      type: RESPONSE_TYPE_RESULT,
      data: { foo: "bar" },
    });
  });

  it("should make a simple query with both subject and object", async () => {
    global.fetch = jest.fn(async () => ({
      ok: true,
      status: 200,
      json: async () => ({
        result: { foo: "bar" },
      }),
    }));

    const result = await query(
      "thisistheurl",
      "thisisthesessionid",
      "thisisthesubject",
      "thisistherelationship",
      "thisistheobject",
    );
    expect(global.fetch).toHaveBeenCalledTimes(1);
    const call = global.fetch.mock.calls[0][1];
    expect(Object.keys(call).sort()).toEqual(["body", "headers", "method"]);
    expect(call.method).toEqual("POST");
    expect(call.headers).toMatchObject({ "Content-Type": "application/json" });
    expect(JSON.parse(call.body)).toMatchObject({
      subject: "thisisthesubject",
      relationship: "thisistherelationship",
      object: "thisistheobject",
    });
    expect(result).toEqual({
      type: RESPONSE_TYPE_RESULT,
      data: { foo: "bar" },
    });
  });

  it("should handle a question response", async () => {
    global.fetch = jest.fn(async () => ({
      ok: true,
      status: 200,
      json: async () => ({
        question: { foo: "bar" },
        extraQuestions: [],
      }),
    }));

    const result = await query(
      "thisistheurl",
      "thisisthesessionid",
      null,
      "thisistherelationship",
      "thisistheobject",
    );
    expect(global.fetch).toHaveBeenCalledTimes(1);
    const call = global.fetch.mock.calls[0][1];
    expect(Object.keys(call).sort()).toEqual(["body", "headers", "method"]);
    expect(call.method).toEqual("POST");
    expect(call.headers).toMatchObject({ "Content-Type": "application/json" });
    expect(JSON.parse(call.body)).toMatchObject({
      relationship: "thisistherelationship",
      object: "thisistheobject",
    });
    expect(result).toEqual({
      type: RESPONSE_TYPE_QUESTION,
      data: {
        question: { foo: "bar" },
        extraQuestions: [],
      },
    });
  });

  it("should make a query for specific engine", async () => {
    global.fetch = jest.fn(async () => ({
      ok: true,
      status: 200,
      json: async () => ({
        result: { foo: "bar" },
      }),
    }));

    const result = await query(
      "thisistheurl",
      "thisisthesessionid",
      "thisisthesubject",
      "thisistherelationship",
      null,
      { engine: "thisistheengine" },
    );
    expect(global.fetch).toHaveBeenCalledTimes(1);
    const call = global.fetch.mock.calls[0][1];
    expect(Object.keys(call).sort()).toEqual(["body", "headers", "method"]);
    expect(call.method).toEqual("POST");
    expect(call.headers).toMatchObject({
      "Content-Type": "application/json",
      "x-rainbird-engine": "thisistheengine",
    });
    expect(JSON.parse(call.body)).toMatchObject({
      subject: "thisisthesubject",
      relationship: "thisistherelationship",
    });
    expect(result).toEqual({
      type: RESPONSE_TYPE_RESULT,
      data: { foo: "bar" },
    });
  });

  it("should handle a question response with extra questions for grouped relationships", async () => {
    global.fetch = jest.fn(async () => ({
      ok: true,
      status: 200,
      json: async () => ({
        question: { foo: "bar" },
        extraQuestions: [
          { qux: "quux" },
          { quuz: "corge" },
          { grault: "garply" },
        ],
      }),
    }));

    const result = await query(
      "thisistheurl",
      "thisisthesessionid",
      null,
      "thisistherelationship",
      "thisistheobject",
    );
    expect(global.fetch).toHaveBeenCalledTimes(1);
    const call = global.fetch.mock.calls[0][1];
    expect(Object.keys(call).sort()).toEqual(["body", "headers", "method"]);
    expect(call.method).toEqual("POST");
    expect(call.headers).toMatchObject({ "Content-Type": "application/json" });
    expect(JSON.parse(call.body)).toMatchObject({
      relationship: "thisistherelationship",
      object: "thisistheobject",
    });
    expect(result).toEqual({
      type: RESPONSE_TYPE_QUESTION,
      data: {
        question: { foo: "bar" },
        extraQuestions: [
          { qux: "quux" },
          { quuz: "corge" },
          { grault: "garply" },
        ],
      },
    });
  });

  it("should throw fetch-generated errors", async () => {
    global.fetch = jest.fn(async () => {
      throw new Error("This was generated by fetch");
    });

    await expect(async () =>
      query(
        "thisistheurl",
        "thisistheid",
        null,
        "thisistherelationship",
        "thisistheobject",
      ),
    ).rejects.toHaveProperty("message", "This was generated by fetch");
  });

  it("should handle 404", async () => {
    global.fetch = jest.fn(async () => ({
      ok: false,
      status: 404,
      json: async () => ({}),
    }));

    await expect(async () =>
      query(
        "thisistheurl",
        "thisistheid",
        null,
        "thisistherelationship",
        "thisistheobject",
      ),
    ).rejects.toHaveProperty("message", "API error code: 404");
  });

  it("should handle 500", async () => {
    global.fetch = jest.fn(async () => ({
      ok: false,
      status: 500,
      json: async () => ({}),
    }));

    await expect(async () =>
      query(
        "thisistheurl",
        "thisistheid",
        null,
        "thisistherelationship",
        "thisistheobject",
      ),
    ).rejects.toHaveProperty("message", "API error code: 500");
  });

  it("should handle 400", async () => {
    global.fetch = jest.fn(async () => ({
      ok: false,
      status: 400,
      json: async () => ({}),
    }));

    await expect(async () =>
      query(
        "thisistheurl",
        "thisistheid",
        null,
        "thisistherelationship",
        "thisistheobject",
      ),
    ).rejects.toHaveProperty("message", "API error code: 400");
  });
});
