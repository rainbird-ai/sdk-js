import "core-js/stable";
import "regenerator-runtime/runtime";
import "./polyfill";

export const URL_COMMUNITY = "https://api.rainbird.ai";
export const URL_ENTERPRISE = "https://enterprise-api.rainbird.ai";
export const ENGINE_CORE = "Core";
export const ENGINE_BETA = "Experimental (Beta)";
export {
  RESPONSE_TYPE_QUESTION,
  RESPONSE_TYPE_RESULT,
  RESPONSE_TYPE_UNKNOWN,
} from "./response_types";

export { evidence } from "./evidence";
export { fullEvidence } from "./full_evidence";
export { inject } from "./inject";
export { query } from "./query";
export { response } from "./response";
export { start } from "./start";
export { undo } from "./undo";
export { interactionLog } from "./interaction_log";
export { knowledgeMap, facts } from "./session";
