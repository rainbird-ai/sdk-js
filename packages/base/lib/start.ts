interface StartResponse {
  sessionID: string;
  kmVersionID: string;
}

interface Options {
  engine?: string;
  context?: string;
  useDraft?: boolean;
  credentials?: RequestCredentials;
}

const encodeFragment = (context: string) => {
  return `${encodeURIComponent("contextId")}=${encodeURIComponent(context)}`;
};

export const start = async (
  baseURL: string,
  apiKey: string,
  kmID: string,
  options: Options = {
    engine: undefined,
    context: undefined,
    useDraft: undefined,
    credentials: undefined,
  },
): Promise<StartResponse> => {
  const { fetch, btoa, encodeURIComponent } = global;
  const { engine, context, useDraft, credentials } = options;

  const headers = {
    "Content-Type": "application/json",
    Authorization: `Basic ${btoa(`${apiKey}:`)}`,
    ...(engine && { "x-rainbird-engine": engine }),
  };
  let urlQueries;
  const draft = "useDraft=true";

  if (context && useDraft) {
    urlQueries = `?${encodeFragment(context)}&${draft}`;
  } else if (context) urlQueries = `?${encodeFragment(context)}`;
  else if (useDraft) urlQueries = `?${draft}`;

  const url = `${baseURL}/start/${kmID}${urlQueries || ""}`;
  const response = await fetch(url, { credentials, headers });
  if (!response.ok) throw new Error(`API error code: ${response.status}`);
  const data = await response.json();
  return { sessionID: data.id, kmVersionID: data.kmVersion.id };
};
