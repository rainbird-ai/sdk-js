import { Fact, Options } from "./domain";

interface WarningLog {
  message: string;
  timestamp: string;
}

interface InjectResponse {
  result: string;
  warningLog?: WarningLog[];
}

export const inject = async (
  baseURL: string,
  sessionID: string,
  data: Fact[],
  options: Options = { engine: undefined, context: undefined },
): Promise<InjectResponse> => {
  const { fetch } = global;
  const { engine } = options;
  const headers = {
    "Content-Type": "application/json",
    ...(engine && { "x-rainbird-engine": engine }),
  };

  const response = await fetch(`${baseURL}/${sessionID}/inject`, {
    body: JSON.stringify(data),
    headers,
    method: "POST",
  });
  if (!response.ok) throw new Error(`API error code: ${response.status}`);
  return response.json() as Promise<InjectResponse>;
};
