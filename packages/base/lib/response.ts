import { parse } from "./response_types";
import { Fact, Options, ResultResponse, QuestionResponse } from "./domain";

// Returns question(s) or result
export async function response(
  baseURL: string,
  sessionID: string,
  answers: Fact[],
  options: Options = { engine: undefined, context: undefined },
): Promise<ResultResponse | QuestionResponse> {
  const { fetch } = global;
  const { engine } = options;

  const headers = {
    "Content-Type": "application/json",
    ...(engine && { "x-rainbird-engine": engine }),
  };

  const ret = await fetch(`${baseURL}/${sessionID}/response`, {
    method: "POST",
    headers,
    body: JSON.stringify({ answers }),
  });
  if (!ret.ok) throw new Error(`API error code: ${ret.status}`);
  return parse(await ret.json());
}
