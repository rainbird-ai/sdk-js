export const interactionLog = async (
  baseURL: string,
  sessionID: string,
  interactionKey?: string,
): Promise<any> => {
  const { fetch } = global;

  const headers = {
    "Content-Type": "application/json",
    ...(interactionKey && { "x-interaction-key": interactionKey }),
  };

  const response = await fetch(
    `${baseURL}/analysis/interactions/${sessionID}`,
    {
      method: "GET",
      headers,
    },
  );

  if (!response.ok) throw new Error(`API error code: ${response.status}`);
  return response.json();
};
