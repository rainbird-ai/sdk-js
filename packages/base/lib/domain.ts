export type ConceptValue = string | boolean | number;

type ConceptType = "number" | "string" | "boolean" | "date";

interface ConceptInstance {
  value: ConceptValue;
  dataType: ConceptType;
}

interface SessionConceptInstance extends ConceptInstance {
  concept: string;
}

interface EvidenceConceptInstance extends ConceptInstance {
  type: string;
}

export interface Options {
  engine?: string;
  context?: string;
}

export interface OptionsEvidence {
  engine?: string;
  evidenceKey?: string;
}

export interface Fact {
  subject: ConceptValue;
  relationship: string;
  object: ConceptValue;
  certainty?: number; // default at 100 if not specified //
}

interface Metadata {
  [language: string]: {
    dataType: string;
    data: string;
  }[];
}

interface Result {
  subject: ConceptValue;
  relationshipType: string;
  object: ConceptValue;
  certainty: number;
  factID: string;
  relationship: string;
  subjectMetadata: Metadata;
  objectMetadata: Metadata;
}

export interface ResultResponse {
  type: symbol; // added by parsing
  data: Result[];
}

interface KnownAnswer {
  subject: ConceptValue;
  relationship: {
    name: string;
    subject: ConceptValue;
    object: ConceptValue;
    plural: boolean;
    allowCertainty: boolean;
    allowUnknown: boolean;
    canAdd: string;
    askable: string;
    // ToDo type questions properly
    questions: any;
    metadata: Metadata;
    fsid: number;
    allowCF: boolean;
    // ToDo type subjectDatasources properly
    subjectDatasources: any[];
    objectType: ConceptType;
    subjectType: ConceptType;
  };
  object: ConceptValue;
  cf: number;
}

interface ConceptsList {
  conceptType: string;
  name: ConceptValue;
  type: string;
  value: ConceptValue;
}

interface Question {
  subject: ConceptValue;
  dataType: string;
  relationship: string;
  type: string;
  plural: boolean;
  allowCF: boolean;
  allowUnknown: boolean;
  canAdd: boolean;
  prompt: string;
  knownAnswers: KnownAnswer[];
  concepts: ConceptsList[];
}

export interface QuestionResponse {
  type: symbol; // added by parsing
  data: {
    question: Question[];
    // ToDo type extraQuestions properly
    extraQuestions: any[];
  };
}

export interface ConditionRelationship {
  certainty: number;
  factID: string;
  factKey: number;
  object: ConceptValue;
  objectType: ConceptType;
  relationship: string;
  salience: number;
  subject: ConceptValue;
}

export interface ConditionExpression {
  wasMet: boolean;
  salience: number;
  expression: { text: string };
}

export const isConditionRelationship = (
  condition: ConditionExpression | ConditionRelationship,
): condition is ConditionRelationship => {
  return (condition as ConditionRelationship).relationship !== undefined;
};

export interface EvidenceResponse {
  factID: string;
  fact: {
    certainty: number;
    object: EvidenceConceptInstance;
    relationship: {
      type: string;
    };
    subject: EvidenceConceptInstance;
  };
  rule: {
    bindings: {
      [variable: string]: ConceptValue;
    };
    conditions: [ConditionRelationship | ConditionExpression];
  };
  source: string;
  time: number;
}
export interface Km {
  id: string;
  name: string;
  versionID: string;
  versionNumber?: number;
  versionCreated?: string;
  versionStatus: string;
}

interface SessionFact {
  id: string;
  source: string;
  subject: SessionConceptInstance;
  relationship: string;
  object: SessionConceptInstance;
  certainty: number;
}

export interface Facts {
  global: SessionFact[];
  context: SessionFact[];
  local: SessionFact[];
}
