import { Facts, Km } from "./domain";

async function session(
  baseURL: string,
  apiKey: string,
  sessionID: string,
  filter: "version" | "facts",
): Promise<Response> {
  const { fetch } = global;

  const headers = {
    "Content-Type": "application/json",
    Authorization: `Basic ${btoa(`${apiKey}:`)}`,
  };

  const response = await fetch(
    `${baseURL}/analysis/session/${sessionID}?filter=${filter}`,
    {
      method: "GET",
      headers,
    },
  );
  if (!response.ok) throw new Error(`API error code: ${response.status}`);
  return response;
}

export async function knowledgeMap(
  baseURL: string,
  apiKey: string,
  sessionID: string,
): Promise<Km> {
  const sessionResponse = await session(baseURL, apiKey, sessionID, "version");
  return sessionResponse.json() as Promise<Km>;
}

export async function facts(
  baseURL: string,
  apiKey: string,
  sessionID: string,
): Promise<Facts> {
  const sessionResponse = await session(baseURL, apiKey, sessionID, "facts");
  return sessionResponse.json() as Promise<Facts>;
}
