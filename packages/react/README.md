> # @rainbird/sdk-react
[![pipeline status](https://gitlab.com/rainbird-ai/sdk-js/badges/master/pipeline.svg)](https://gitlab.com/rainbird-ai/sdk-js/badges/master/pipeline.svg)   [![coverage status](https://gitlab.com/rainbird-ai/sdk-js/badges/master/coverage.svg)](https://gitlab.com/rainbird-ai/sdk-js/badges/master/coverage.svg)

A package full of React components, hooks, and decorators to ease your way into Rainbird.

> ## Installation

```bash
    yarn add @rainbird/sdk-react
```

```bash
    npm i @rainbird/sdk-react
```

> ## Usage
[context](#context) | [hooks](#hooks) | [components](#components) | [decorators](#decorators) | [helpers](#helpers)

|Name   	|Type   	|Description   	|Example/Help   	|
|---	|---	|---	|---	|
|baseURL   	|String   	|The url to be targeted without any params   	|'https://api.rainbird.ai'  	|
|apiKey   	|String   	|The api key that allows access to the api   	|This can be found in the 'account' section of the Rainbird Studio   	|
|kmID   	|String   	|The ID of the specific knowledge map that you will query   	|This can be found in the map view of the Rainbird Studio  	|
|factID     |String     |The id of the resulting answer from Rainbird                   |This can be found in from the response endpoint from a RESULT type|
|evidenceKey|String     |A key to gain access to secured evidence                       |This can be found in the map view of the Rainbird Studio           |
|options   	|Object (optional)   	|Options provided to Rainbird, currently used to interact with different versions of the engine   	|`{ engine: 'v2.0' }`  	|

### Context
[RainbirdProvider](#rainbirdprovider) | [InteractionProvider](#interactionprovider) | [RainbirdContext](#rainbirdcontext) | [InteractionContext](#interactioncontext)

In order to use any of the hooks, components or decorators in this package they need to be a descendent of a RainbirdContext. Some of the hooks/components/decorators concerning the back-and-forth questioning of Rainbird also need to be a descendent of an InteractionContext as explained below. To make this easier, you can use a RainbirdProvider and InteractionProvider rather than setting the context directly.

##### RainbirdProvider
The RainbirdProvider wraps the [RainbirdContext](#rainbirdcontext) which stores API config info. It's used by the `start` hook to store the sessionID for later requests. Click [here]() to learn more about the Rainbird API.

```jsx
import { RainbirdProvider } from '@rainbird/sdk-react';

export default () => (
    <RainbirdProvider
        baseURL="https://api.rainbird.ai"
        apiKey="myApiKey"
        kmID="myKmId"
        options={{ engine: 'v2.0' }}
    >
        <p>Hello!</p>
    </RainbirdProvider>
)
```

##### InteractionProvider
The InteractionProvider wraps the [InteractionContext](#interactioncontext) and sends the contents of a query or response call to Rainbird to its direct child, as well as a loading and error state. The InteractionProvider can be instantiated anywhere in the tree, so long as it's a descendent of the RainbirdProvider. For more info on the shape of the data returned from a query or response, [click here](https://gitlab.com/rainbird-ai/sdk-js/-/tree/master/packages/base)

```jsx
import { RainbirdProvider, InteractionProvider } from '@rainbird/sdk-react';

export default () => (
    <RainbirdProvider
        baseURL="https://api.rainbird.ai"
        apiKey="myApiKey"
        kmID="myKmId"
        options={{ engine: 'v2.0' }}
    >
        <InteractionProvider >
            {({data, loading, error}) => (
                // ...
            )}
        </InteractionProvider>
    </RainbirdProvider>
)
```

##### RainbirdContext
The RainbirdContext can be accessed directly using React.useContext if you need to extract any information about the sessionID, kmID, baseURL or apiKey
```jsx
import React, { useContext } from 'react'
import { RainbirdContext } from '@rainbird/sdk-react';

export default () => {
    const { apiKey, baseURL, kmID, sessionID } = useContext(RainbirdContext)

    return (
        // ...
    )
}
```

##### InteractionContext
The InteractionContext can be accessed directly using React.useContext if you need to extract any information about the current interaction. However, there is a [hook](#useinteraction) and a [decorator](#withinteraction) available to make this easier.

```jsx
import React, { useContext } from 'react'
import { InteractionContext } from '@rainbird/sdk-react';

export default () => {
    const { data, loading, error } = useContext(InteractionContext)

    return (
        // ...
    )
}
```

### Hooks
[useStart](#usestart) | [useQuery](#usequery) | [useInject](#useinject) | [useResponse](#useresponse) | [useUndo](#useundo) | [useEvidence](#useevidence) | [useInteraction](#useinteraction)

#### useStart
Invoked immediately on mount and responds to changing arguments. Returns data, loading and error state.

```jsx
import React from 'react'
import { RainbirdProvider, useStart } from '@rainbird/sdk-react';

const Start = ({ children }) => {
    const { data, loading, error } = useStart();

    return (
        <div>
            {loading && <Loading />}
            {error && <Error />}
            {data && children}
        </div>
    )
}

export const App = (props) => (
    <RainbirdProvider {...props}>
        <Start>
            <p>Session started!</p>
        </Start>
    </RainbirdProvider>
)
```

##### useQuery
Invoked immediately on mount. Returns data, loading and error state. Needs to be a descendent of an InteractionProvider.

```jsx
import React from 'react'
import { RainbirdProvider, InteractionProvider, useQuery } from '@rainbird/sdk-react';

const Query = ({ children }) => {
    const { data, loading, error } = useQuery();

    return (
        <div>
            {loading && <Loading />}
            {error && <Error />}
            {data && children}
        </div>
    )
}

export const App = (props) => (
    <RainbirdProvider {...props}>
        <InteractionProvider>
            <Start>
                <Query>
                    <p>Query started!</p>
                </Query>
            </Start>
        </InteractionProvider>
    </RainbirdProvider>
)
```

##### useInject
Returns the state of the inject api call as well as a function to send facts.

```jsx
import React from 'react'
import { RainbirdProvider, useInject } from '@rainbird/sdk-react';

const Inject = () => {
    const [inject, sendInject] = useInject();

    return (
        <div>
            {inject.loading && <Loading />}
            {inject.error && <Error />}
            {inject.data && inject.data.ok && <p>Success!</p>}
            <button onClick={() => sendInject([{ subject: 'Bob', relationship: 'speaks', object: 'French', cf: 100}])}>
        </div>
    )
}

export const App = (props) => (
    <RainbirdProvider {...props}>
         <Inject />
    </RainbirdProvider>
)
```

##### useResponse
Returns a function to send a response to Rainbird. Needs to be a descendent of an InteractionProvider which will return the data, loading and error state of the interaction (useInteraction is used here to access the InteractionContext).

```jsx
import React from 'react'
import { RainbirdProvider, InteractionProvider, useResponse, useInteraction } from '@rainbird/sdk-react';

const Respond = () => {
    const { data, loading, error } = useInteraction()
    const sendResponse = useResponse();

    return (
        <div>
            {loading && <Loading />}
            {error && <Error />}
            {data && data.ok && <p>Success!</p>}
            <button onClick={() => sendResponse([{ subject: 'Bob', relationship: 'speaks', object: 'French', cf: 100}])}>Respond</button>
        </div>
    )
}

export const App = (props) => (
    <RainbirdProvider {...props}>
        <Start>
            <InteractionProvider>
                <Query>
                    <Respond />
                </Query>
            </InteractionProvider>
        </Start>
    </RainbirdProvider>
)
```

##### useUndo
Returns a function to undo a response sent to Rainbird. Needs to be a descendent of an InteractionProvider which will return the data, loading and error state of the interaction.

```jsx
import React from 'react'
import { RainbirdProvider, useResponse, useInteraction, useUndo } from '@rainbird/sdk-react';

const RespondAndUndo = () => {
    const { data, loading, error } = useInteraction()
    const sendResponse = useResponse();
    const undo = useUndo()

    return (
        <div>
            {loading && <Loading />}
            {error && <Error />}
            {data && data.ok && <p>Success!</p>}
            <button onClick={undo}>Undo</button>
            <button onClick={() => sendResponse([{ subject: 'Bob', relationship: 'speaks', object: 'French', cf: 100}])}>Respond</button>
        </div>
    )
}

export const App = (props) => (
    <RainbirdProvider {...props}>
        <Start>
            <Query>
                <RespondAndUndo />
            </Query>
        </Start>
    </RainbirdProvider>
)
```

##### useEvidence
Returns the state of the api call as well as a function to call the evidence endpoint.

```jsx
import React from 'react'
import { RainbirdProvider, useEvidence } from '@rainbird/sdk-react';

const Evidence = () => {
    const [evidence, getEvidence] = useEvidence()

    return (
        <div>
            {evidence.loading && <Loading />}
            {evidence.error && <Error />}
            {evidence.data && <p>Success!</p>}
            <button onClick={() => getEvidence(factID, evidenceKey?)}>Evidence</button>
        </div>
    )
}

export const App = (props) => (
    <RainbirdProvider {...props}>
        <Start>
            <Query>
                <Evidence />
            </Query>
        </Start>
    </RainbirdProvider>
)
```

##### useInteraction
Returns the current state of the interaction anywhere in the tree. Must be a descendent of an InteractionProvider.

```jsx
import React from 'react'
import { RainbirdProvider, InteractionProvider, useInteraction } from '@rainbird/sdk-react';

const Interaction = () => {
    const { data, loading, error } = useInteraction()

    return (
        <div>
            {loading && <Loading />}
            {error && <Error />}
            {data && <p>Success!</p>}
        </div>
    )
}

export const App = (props) => (
    <RainbirdProvider {...props}>
        <Start>
            <InteractionProvider>
                <Interaction />
            </InteractionProvider>
        </Start>
    </RainbirdProvider>
)
```

### Decorators
[withStart](#withstart) | [withQuery](#withquery) | [withInject](#withinject) | [withResponse](#withresponse) | [withUndo](#withundo) | [withEvidence](#withevidence) | [withInteraction](#withinteraction)

All the decorators follow the same pattern as the hooks, however they are initialised differently. Rather than instantiating the hooks within the component, the state/functions that are returned from the hooks are injected. This may make testing easier as it's easier to mock the prop in the component.

##### withStart

```jsx
import React from 'react'
import { RainbirdProvider, withStart } from '@rainbird/sdk-react';

const Interaction = withStart(({ start }) =>  (
    <div>
        {start.loading && <Loading />}
        {start.error && <Error />}
        {start.data && <p>Success!</p>}
    </div>
))


export const App = (props) => (
    <RainbirdProvider {...props}>
        <Start/>
    </RainbirdProvider>
)
```

##### withQuery

```jsx
import React from 'react'
import { RainbirdProvider, InteractionProvider, withQuery } from '@rainbird/sdk-react';

const Query = withQuery(({ query }) =>  (
    <div>
        {query.loading && <Loading />}
        {query.error && <Error />}
        {query.data && <p>Success!</p>}
    </div>
))

export const App = (props) => (
    <RainbirdProvider {...props}>
        <InteractionProvider>
            <Start>
                <Query/>
            </Start>
        </InteractionProvider>
    </RainbirdProvider>
)
```

##### withInject

```jsx
import React from 'react'
import { RainbirdProvider, InteractionProvider, withInject } from '@rainbird/sdk-react';

const Inject = withInject(({ inject, sendInject }) =>  (
    <div>
        {inject.loading && <Loading />}
        {inject.error && <Error />}
        {inject.data && <p>Success!</p>}
        <button onClick={() => sendInject([{ subject: 'Bob', relationship: 'speaks', object: 'English' }])}>Inject</button>
    </div>
))

export const App = (props) => (
    <RainbirdProvider {...props}>
        <InteractionProvider>
            <Start>
                <Inject>
            </Start>
        </InteractionProvider>
    </RainbirdProvider>
)
```

##### withResponse

```jsx
import React from 'react'
import { RainbirdProvider, InteractionProvider, withResponse } from '@rainbird/sdk-react';

const Respond = withResponse(({ sendResponse }) =>  (
    <div>
        <button onClick={() => sendResponnse([{ subject: 'Bob', relationship: 'speaks', object: 'English' }])}>Inject</button>
    </div>
))

export const App = (props) => (
    <RainbirdProvider {...props}>
        <InteractionProvider>
            <Start>
                <Query>
                    <Respond />
                </Query>
            </Start>
        </InteractionProvider>
    </RainbirdProvider>
)
```

##### withUndo

```jsx
import React from 'react'
import { RainbirdProvider, InteractionProvider, withUndo } from '@rainbird/sdk-react';

const Undo = withUndo(({ undo }) =>  (
    <div>
        <button onClick={undo}>Inject</button>
    </div>
))

export const App = (props) => (
    <RainbirdProvider {...props}>
        <InteractionProvider>
            <Start>
                <Query>
                    <Undo />
                </Query>
            </Start>
        </InteractionProvider>
    </RainbirdProvider>
)
```

##### withEvidence

```jsx
import React from 'react'
import { RainbirdProvider, InteractionProvider, withEvidence } from '@rainbird/sdk-react';

const Evidence = withEvidence(({ evidence, getEvidence, factID = '100sdXX0302' }) =>  (
    <div>
        {evidence.loading && <Loading />}
        {evidence.error && <Error />}
        {evidence.data && <Success data={evidence.data} />}
        <button onClick={() => getEvidence(factID, evidenceKey?)}>Inject</button>
    </div>
))

export const App = (props) => (
    <RainbirdProvider {...props}>
        <Start>
            <Evidence />
        </Start>
    </RainbirdProvider>
)
```

##### withInteraction

```jsx
import React from 'react'
import { RainbirdProvider, InteractionProvider, withInteraction } from '@rainbird/sdk-react';

const Interaction = withInteraction(({ interaction }) =>  (
    <div>
        {interaction.loading && <Loading />}
        {interaction.error && <Error />}
        {interaction.data && <Success data={interaction.data} />}
    </div>
))

export const App = (props) => (
    <RainbirdProvider {...props}>
        <InteractionProvider>
            <Start>
                <Interaction />
            </Start>
        </InteractionProvider>
    </RainbirdProvider>
)
```

### Components
[Start](#start) | [Query](#query) | [Inject](#inject) | [Response](#response) | [Undo](#undo) | [Evidence](#evidence) | [Interaction](#interaction) | [FormControl](#formcontrol) | [Rainbird](#rainbird)

The components that follow the naming convention of the api (start, query, inject, response, undo and evidence) follow the same pattern as the hooks and decorators in what they return. They are initialised as follows:

##### Start

```jsx
import React from 'react'
import { RainbirdProvider, Start } from '@rainbird/sdk-react';

export const App = (props) => (
    <RainbirdProvider {...props}>
        <Start>
            {({loading, error, data}) => (
                // ...
            )}
        </Start>
    </RainbirdProvider>
)
```

##### Query

```jsx
import React from 'react'
import { RainbirdProvider, Start, Query } from '@rainbird/sdk-react';

export const App = (props) => (
    <RainbirdProvider {...props}>
        <InteractionProvider>
            {({data, loading, error}) => (
                <Start>
                    {(start) => (
                        <Query>
                            {() => {
                                if (loading) return <Loading>
                                // ...
                            }}
                        </Query>
                    )}
                </Start>
            )}
        </InteractionProvider>
    </RainbirdProvider>
)
```

##### Inject

```jsx
import React from 'react'
import { RainbirdProvider, Start, Inject } from '@rainbird/sdk-react';

export const App = (props) => (
    <RainbirdProvider {...props}>
        <InteractionProvider>
            {(interaction) => (
                <Start>
                    {(start) => (
                        <Inject>
                            {({data, loading, error, request}) => {
                                if (loading) return <Loading />
                                if (error) return <Error />
                                if (data) return <Success />
                                return <button onClick={
                                    () => request(
                                        [{
                                            subject: 'Bob',
                                            relationship: 'speaks',
                                            object: 'French',
                                            cf: 100
                                        }]
                                    )}>
                                    inject
                                </button>
                            }}
                        </Inject>
                    )}
                </Start>
            )}
        </InteractionProvider>
    </RainbirdProvider>
)
```

##### Response

```jsx
import React from 'react'
import { RainbirdProvider, Start, Query, Response } from '@rainbird/sdk-react';

export const App = (props) => (
    <RainbirdProvider {...props}>
        <InteractionProvider>
            {({ data, loading, error }) => (
                <Start>
                    {(start) => (
                        <Query>
                            {() => (
                                <Response>
                                    {response => (
                                        <button onClick={
                                            () => request([{
                                                    subject: 'Bob',
                                                    relationship: 'speaks',
                                                    object: 'French',
                                                    cf: 100 }]
                                              )}>response</button>
                                    )}
                                </Response>
                            )}
                        </Query>
                    )}
                </Start>
            )}
        </InteractionProvider>
    </RainbirdProvider>
)
```

##### Undo

```jsx
import React from 'react'
import { RainbirdProvider, Start, Query, Undo } from '@rainbird/sdk-react';

export const App = (props) => (
    <RainbirdProvider {...props}>
        <InteractionProvider>
            {({ data, loading, error }) => (
                <Start>
                    {(start) => (
                        <Query>
                            {() => (
                                <Undo>
                                    {undo => <button onClick={undo}>Undo</button>}
                                </Undo>
                            )}
                        </Query>
                    )}
                </Start>
            )}
        </InteractionProvider>
    </RainbirdProvider>
)
```

##### Evidence

```jsx
import React from 'react'
import { RainbirdProvider, Start, Evidence } from '@rainbird/sdk-react';

export const App = (props) => (
    <RainbirdProvider {...props}>
        <InteractionProvider>
            {(interaction) => (
                <Start>
                    {(start) => (
                        <Evidence>
                            {({data, loading, error, request}) => {
                                if (loading) return <Loading />
                                if (error) return <Error />
                                if (data) return <Success />
                                return (
                                    <button onClick={() => request(factID, evidenceKey?)}>
                                        Evidence
                                    </button>
                                )}}
                        </Evidence>
                    )}
                </Start>
            )}
        </InteractionProvider>
    </RainbirdProvider>
)
```

##### Interaction

```jsx
import React from 'react'
import { RainbirdProvider, Start, Interaction } from '@rainbird/sdk-react';

export const App = (props) => (
    <RainbirdProvider {...props}>
        <InteractionProvider>
            {(interaction) => (
                <Start>
                    {(start) => (
                        <Interaction>
                            {({ data, loading, error }) => {
                                if (loading) return <Loading />
                                if (error) return <Error />
                                if (data) return <Success />
                        </Interaction>
                    )}
                </Start>
            )}
        </InteractionProvider>
    </RainbirdProvider>
)
```

##### FormControl
Deciding the correct form input to present can be difficult with the response data. The following component accepts the data from a response to determine which control to present to the user. If this is too rigid, there is a [helper method](#getforminputtype) which this is based on that is more flexible.

| Prop            | Explanation                                                                                                                                                  |
| --------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| multiString     | The user can select multiple strings from a predetermined list. For example, the user can select 'French', 'English' and 'German'                            |
| multiStringAdd  | The user can select multiple strings and add their own. For example, the user can select 'French', 'English', 'German' but the user can also add 'Afrikaans' |
| singleString    | The user can only select one string from a predetermined list.                                                                                               |
| singleStringAdd | The user can only select one string from a predetermined list, but can also add their own.                                                                   |
| singleDate      | The user can select one date                                                                                                                                 |
| singleNumber    | The user can select one number                                                                                                                               |
| singleTruth     | The user can make one boolean choice                                                                                                                         |
| certaintySelect | The user can choose a number from 1-100 to represent the certainty of their decision. Only appears when `allowCF: true` is returned on the response.         |

```jsx
import React from 'react';
import { FormControl } from '@rainbird/sdk-react';

export const Form = ({ data }) => {
    const [value, setValue] = React.useState('')

    const handleChange = e => setValue(e.target.value)

    return (
        <FormControl
            data={data}
            multiString={<Input onChange={handleChange} value={value} />}
            multiStringAdd={<Input onChange={handleChange} value={value} />}
            singleString={<Input onChange={handleChange} value={value} />}
            singleStringAdd={<Input onChange={handleChange} value={value} />}
            singleDate={<Input type="date" onChange={handleChange} value={value} />}
            singleNumber={<Input type="number" onChange={handleChange} value={value} />}
            singleTruth={<Input onChange={handleChange} value={value} />}
            certaintySelect={<Slider onChange={handleChange} value={value} />}
        />
    )
}
```

##### Rainbird
Starting an interaction can be cumbersome and time-consuming, so Rainbird is a component that combines the [RainbirdProvider](#rainbirdprovider), [InteractionProvider](#interactionprovider), [Start](#start) and [Query](#query) to make it quicker to get started.

```jsx
import React from 'react'
import {Rainbird} from '@rainbird/sdk-react';

export const App = () => (
    <Rainbird
        apiKey="myApiKey"
        baseURL="https://api.rainbird.ai"
        options={{}}
        kmID="myKmID"
        subject="Bob"
        relationship="speaks"
        object=""
        onError={e => <Error>{e.message}</Error>}
        onLoad={<Loading />}
    >
        {({ data }) => ( // this is a response from the /query endpoint
            // ...rest of the app
        )}
    </Rainbird>
)

```

### Helpers
Designed to be flexible but take away some of the pain of interacting with the API

##### getFormInputType
A function that takes a question and returns an array of the different input types that a user might expect to see given the shape of the response

Currently an array to support multiple questions in the future.

```jsx
import { getInputTypes } from '@rainbird/sdk-react';

const data = {
    question: {
        plural: true,
        dataType: 'string',
        canAdd: true,
        allowCF: true,
        subject: 'Bob',
        relationship: 'speaks',
        type: 'Second Form Object',
        knownAnswers: []
    }
}

const inputTypes = getInputTypes(data)
// [{ control: 'multi-string-add', certainty: true }]
```
