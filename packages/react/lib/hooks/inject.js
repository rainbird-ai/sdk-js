import { useContext, useReducer } from "react";
import { inject } from "@rainbird/sdk";

import { RainbirdContext } from "../context";

const reducer = (_, action) => {
  if (action.error) return { loading: false, error: action.error, data: false };
  return { loading: false, error: false, data: action.payload };
};

export const useInject = () => {
  const { baseURL, options, sessionID } = useContext(RainbirdContext);
  const [state, dispatch] = useReducer(reducer, {
    data: null,
    error: false,
    loading: true,
  });

  const callInject = async (data) => {
    try {
      const body = await inject(baseURL, sessionID, data, options);
      dispatch({ payload: body });
    } catch (err) {
      dispatch({ error: err });
    }
  };

  return [state, callInject];
};
