import { useContext, useEffect, useCallback } from "react";
import { query } from "@rainbird/sdk";
import { InteractionContext, RainbirdContext } from "../context";

export const useQuery = (subject, relationship, object) => {
  const { baseURL, options, sessionID } = useContext(RainbirdContext);
  const { setInteraction } = useContext(InteractionContext);

  const makeQuery = useCallback(async () => {
    try {
      const body = await query(
        baseURL,
        sessionID,
        subject,
        relationship,
        object,
        options,
      );
      setInteraction({ payload: body });
    } catch (e) {
      setInteraction({ error: e });
    }
  }, [baseURL, sessionID, subject, relationship, object]);

  useEffect(() => {
    makeQuery();
  }, [baseURL, sessionID, subject, relationship, object, makeQuery]);

  return makeQuery;
};
