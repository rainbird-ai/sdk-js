import React, { useContext } from "react";
import { render, waitFor, screen, fireEvent } from "@testing-library/react";
import * as base from "@rainbird/sdk";

import {
  RainbirdProvider,
  InteractionProvider,
  InteractionContext,
} from "../../context";
import { useUndo } from "..";

jest.mock("@rainbird/sdk");

describe("useUndo", () => {
  afterEach(() => jest.clearAllMocks());

  it("calls base SDK undo", async () => {
    const Undo = () => {
      const undo = useUndo();
      return (
        <button type="button" data-testid="undo" onClick={undo}>
          Undo
        </button>
      );
    };

    render(
      <RainbirdProvider
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
        sessionID="12345"
        kmVersionID="124394"
      >
        <InteractionProvider>{() => <Undo />}</InteractionProvider>
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("undo");
    fireEvent.click(button);
    await waitFor(() => {
      expect(base.undo).toHaveBeenCalledTimes(1);
      expect(base.undo).toHaveBeenCalledWith(
        "/rainbird",
        "12345",
        {},
        undefined,
      );
    });
  });

  it("provides options to undo", async () => {
    const Undo = () => {
      const undo = useUndo();
      return (
        <button type="button" data-testid="undo" onClick={undo}>
          Undo
        </button>
      );
    };

    render(
      <RainbirdProvider
        options={{ engine: "v2.0" }}
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
        sessionID="12345"
        kmVersionID="124394"
      >
        <InteractionProvider>{() => <Undo />}</InteractionProvider>
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("undo");
    fireEvent.click(button);
    await waitFor(() => {
      expect(base.undo).toHaveBeenCalledTimes(1);
      expect(base.undo).toHaveBeenCalledWith(
        "/rainbird",
        "12345",
        {},
        { engine: "v2.0" },
      );
    });
  });

  it("Sets the interaction context to loading", async () => {
    const Undo = () => {
      const undo = useUndo();
      const { loading } = useContext(InteractionContext);
      return (
        <div>
          <button type="button" data-testid="undo" onClick={undo}>
            Undo
          </button>
          {loading && <p data-testid="loading">loading</p>}
        </div>
      );
    };

    render(
      <RainbirdProvider
        options={{ engine: "v2.0" }}
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
        sessionID="12345"
        kmVersionID="124394"
      >
        <InteractionProvider>{() => <Undo />}</InteractionProvider>
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("undo");
    fireEvent.click(button);
    await waitFor(() => {
      const { textContent } = screen.getByTestId("loading");
      expect(textContent).toEqual("loading");
    });
  });

  it("Sets interaction context to error", async () => {
    base.undo = jest.fn(async () => {
      throw new Error();
    });

    const Undo = () => {
      const undo = useUndo();
      const { error } = useContext(InteractionContext);
      return (
        <div>
          <button type="button" data-testid="undo" onClick={undo}>
            Undo
          </button>
          {error && <p data-testid="error">error</p>}
        </div>
      );
    };

    render(
      <RainbirdProvider
        options={{ engine: "v2.0" }}
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
        sessionID="12345"
        kmVersionID="124394"
      >
        <InteractionProvider>{() => <Undo />}</InteractionProvider>
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("undo");
    fireEvent.click(button);
    await waitFor(() => {
      const { textContent } = screen.getByTestId("error");
      expect(textContent).toEqual("error");
    });
  });

  it("returns success state", async () => {
    base.undo = jest.fn(async () => ({ text: "hello" }));

    const Undo = () => {
      const undo = useUndo();
      const { data } = useContext(InteractionContext);
      return (
        <div>
          <button type="button" data-testid="undo" onClick={undo}>
            Undo
          </button>
          {data && <p data-testid="data">{data.text}</p>}
        </div>
      );
    };

    render(
      <RainbirdProvider
        options={{ engine: "v2.0" }}
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
        sessionID="12345"
        kmVersionID="124394"
      >
        <InteractionProvider>{() => <Undo />}</InteractionProvider>
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("undo");
    fireEvent.click(button);
    await waitFor(() => {
      const { textContent } = screen.getByTestId("data");
      expect(textContent).toEqual("hello");
    });
  });
});
