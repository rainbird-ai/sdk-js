import React from "react";
import { render, waitFor, screen, fireEvent } from "@testing-library/react";
import * as base from "@rainbird/sdk";

import { RainbirdProvider } from "../../context";
import { useEvidence } from "..";

jest.mock("@rainbird/sdk");

describe("useEvidence", () => {
  beforeEach(async () => {
    base.start = jest.fn(async () => ({
      sessionID: "thisIsTheSessionId",
      kmVersionID: "thisIsTheKmVersionId",
    }));
  });

  afterEach(() => jest.clearAllMocks());

  it("calls base SDK evidence", async () => {
    const Evidence = () => {
      const getEvidence = useEvidence()[1];
      return (
        <div>
          <button
            type="button"
            data-testid="evidence"
            onClick={() => getEvidence("thisIsTheFactID")}
          >
            evidence
          </button>
        </div>
      );
    };

    render(
      <RainbirdProvider
        sessionID="sessionID"
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
      >
        <Evidence />
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("evidence");
    fireEvent.click(button);
    await waitFor(() => {
      expect(base.evidence).toHaveBeenCalledTimes(1);
      expect(base.evidence).toHaveBeenCalledWith(
        "/rainbird",
        "sessionID",
        "thisIsTheFactID",
        {},
      );
    });
  });

  it("provides options to evidence", async () => {
    const Evidence = () => {
      const getEvidence = useEvidence()[1];
      return (
        <div>
          <button
            type="button"
            data-testid="evidence"
            onClick={() => getEvidence("thisIsTheFactID")}
          >
            evidence
          </button>
        </div>
      );
    };

    render(
      <RainbirdProvider
        options={{ engine: "v2.0" }}
        sessionID="sessionID"
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
      >
        <Evidence />
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("evidence");
    fireEvent.click(button);
    await waitFor(() => {
      expect(base.evidence).toHaveBeenCalledTimes(1);
      expect(base.evidence).toHaveBeenCalledWith(
        "/rainbird",
        "sessionID",
        "thisIsTheFactID",
        { engine: "v2.0" },
      );
    });
  });

  it("returns loading state", async () => {
    const Evidence = () => {
      const [data, getEvidence] = useEvidence();
      return (
        <div>
          <button
            type="button"
            data-testid="evidence"
            onClick={() => getEvidence("thisIsTheFactID")}
          >
            evidence
          </button>
          {data.loading && <p data-testid="loading">loading</p>}
        </div>
      );
    };

    render(
      <RainbirdProvider
        sessionID="sessionID"
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
      >
        <Evidence />
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("evidence");
    fireEvent.click(button);
    await waitFor(() => {
      const loading = screen.getByTestId("loading");
      expect(loading.textContent).toEqual("loading");
    });
  });

  it("returns error state", async () => {
    base.evidence = jest.fn(async () => {
      throw new Error();
    });
    const Evidence = () => {
      const [data, getEvidence] = useEvidence();
      return (
        <div>
          <button
            type="button"
            data-testid="evidence"
            onClick={() => getEvidence("thisIsTheFactID")}
          >
            evidence
          </button>
          {data.error && <p data-testid="error">error</p>}
        </div>
      );
    };

    render(
      <RainbirdProvider
        sessionID="sessionID"
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
      >
        <Evidence />
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("evidence");
    fireEvent.click(button);
    await waitFor(() => {
      const error = screen.getByTestId("error");
      expect(error.textContent).toEqual("error");
    });
  });

  it("returns success state", async () => {
    base.evidence = jest.fn(async () => {
      return { data: "hello" };
    });
    const Evidence = () => {
      const [data, getEvidence] = useEvidence();
      return (
        <div>
          <button
            type="button"
            data-testid="evidence"
            onClick={() => getEvidence("thisIsTheFactID")}
          >
            evidence
          </button>
          {data.data && <p data-testid="success">success</p>}
        </div>
      );
    };

    render(
      <RainbirdProvider
        sessionID="sessionID"
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
      >
        <Evidence />
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("evidence");
    fireEvent.click(button);

    await waitFor(() => {
      const success = screen.getByTestId("success");
      expect(success.textContent).toEqual("success");
    });
  });

  it("Calls the evidence endpoint with the evidence key if provided", async () => {
    const Evidence = () => {
      const getEvidence = useEvidence()[1];
      return (
        <div>
          <button
            type="button"
            data-testid="evidence"
            onClick={() => {
              getEvidence("thisIsTheFactID", "thisIsTheEvidenceKey");
            }}
          >
            evidence
          </button>
        </div>
      );
    };

    render(
      <RainbirdProvider
        sessionID="sessionID"
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
      >
        <Evidence />
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("evidence");
    fireEvent.click(button);
    await waitFor(() => {
      expect(base.evidence).toHaveBeenCalledTimes(1);
      expect(base.evidence).toHaveBeenCalledWith(
        "/rainbird",
        "sessionID",
        "thisIsTheFactID",
        { evidenceKey: "thisIsTheEvidenceKey" },
      );
    });
  });
});
