import React, { useContext } from "react";
import { render, waitFor, fireEvent, screen } from "@testing-library/react";
import * as base from "@rainbird/sdk";

import {
  RainbirdProvider,
  InteractionContext,
  InteractionProvider,
} from "../../context";
import { useResponse } from "..";

jest.mock("@rainbird/sdk");

describe("useResponse", () => {
  const answers = [
    { subject: "foo", relationship: "bar", object: "baz", certainty: 95 },
    { subject: "bar", relationship: "baz", object: "foo", certainty: 50 },
    { subject: "baz", relationship: "foo", object: "bar" },
  ];

  beforeEach(async () => {
    base.start = jest.fn(async () => ({
      sessionID: "thisIsTheSessionID",
      kmVersionID: "thisIsTheKmVersionID",
    }));
  });

  afterEach(() => jest.clearAllMocks());

  it("should call base sdk response", async () => {
    base.response = jest.fn(async () => ({}));
    const Response = () => {
      const { data } = useContext(InteractionContext);
      const respond = useResponse();
      return (
        <div>
          <button
            type="button"
            data-testid="respond"
            onClick={() => respond(answers)}
          >
            Respond
          </button>
          {data && <p>response</p>}
        </div>
      );
    };

    render(
      <RainbirdProvider
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
        sessionID="thisIsTheSessionID"
      >
        <InteractionProvider>{() => <Response />}</InteractionProvider>
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("respond");
    fireEvent.click(button);
    await waitFor(() => {
      expect(base.response).toHaveBeenCalledTimes(1);
      expect(base.response).toHaveBeenCalledWith(
        "/rainbird",
        "thisIsTheSessionID",
        [
          { certainty: 95, object: "baz", relationship: "bar", subject: "foo" },
          { certainty: 50, object: "foo", relationship: "baz", subject: "bar" },
          { object: "bar", relationship: "foo", subject: "baz" },
        ],
        undefined,
      );
    });
  });

  it("provides options to response", async () => {
    base.response = jest.fn(async () => ({}));
    const Response = () => {
      const { data } = useContext(InteractionContext);
      const respond = useResponse();
      return (
        <div>
          <button
            type="button"
            data-testid="respond"
            onClick={() => respond(answers)}
          >
            Respond
          </button>
          {data && <p>response</p>}
        </div>
      );
    };

    render(
      <RainbirdProvider
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
        sessionID="thisIsTheSessionID"
        options={{ engine: "Experimental (Beta)" }}
      >
        <InteractionProvider>{() => <Response />}</InteractionProvider>
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("respond");
    fireEvent.click(button);
    await waitFor(() => {
      expect(base.response).toHaveBeenCalledTimes(1);
      expect(base.response).toHaveBeenCalledWith(
        "/rainbird",
        "thisIsTheSessionID",
        [
          { certainty: 95, object: "baz", relationship: "bar", subject: "foo" },
          { certainty: 50, object: "foo", relationship: "baz", subject: "bar" },
          { object: "bar", relationship: "foo", subject: "baz" },
        ],
        { engine: "Experimental (Beta)" },
      );
    });
  });

  it("sets interaction error state", async () => {
    base.response = jest.fn(async () => {
      throw new Error();
    });

    const Response = () => {
      const { error } = useContext(InteractionContext);
      const respond = useResponse();
      return (
        <div>
          <button
            type="button"
            data-testid="respond"
            onClick={() => respond(answers)}
          >
            Respond
          </button>
          {error && <p data-testid="error">error</p>}
        </div>
      );
    };

    render(
      <RainbirdProvider
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
        sessionID="thisIsTheSessionID"
        options={{ engine: "Experimental (Beta)" }}
      >
        <InteractionProvider>{() => <Response />}</InteractionProvider>
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("respond");
    fireEvent.click(button);
    await waitFor(() => {
      const error = screen.getByTestId("error");
      expect(error.textContent).toEqual("error");
    });
  });

  it("shows loading state", async () => {
    base.response = jest.fn(async () => {});

    const Response = () => {
      const { loading } = useContext(InteractionContext);
      const respond = useResponse();
      return (
        <div>
          <button
            type="button"
            data-testid="respond"
            onClick={() => respond(answers)}
          >
            Respond
          </button>
          {loading && <p data-testid="loading">loading</p>}
        </div>
      );
    };

    render(
      <RainbirdProvider
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
        sessionID="thisIsTheSessionID"
        options={{ engine: "Experimental (Beta)" }}
      >
        <InteractionProvider>{() => <Response />}</InteractionProvider>
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("respond");
    fireEvent.click(button);
    await waitFor(() => {
      const loading = screen.getByTestId("loading");
      expect(loading.textContent).toEqual("loading");
    });
  });

  it("shows success state", async () => {
    base.response = jest.fn(async () => {
      return { message: "hello" };
    });

    const Response = () => {
      const { data } = useContext(InteractionContext);
      const respond = useResponse();
      return (
        <div>
          <button
            type="button"
            data-testid="respond"
            onClick={() => respond(answers)}
          >
            Respond
          </button>
          {data && <p data-testid="data">{data.message}</p>}
        </div>
      );
    };

    render(
      <RainbirdProvider
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
        sessionID="thisIsTheSessionID"
        options={{ engine: "Experimental (Beta)" }}
      >
        <InteractionProvider>{() => <Response />}</InteractionProvider>
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("respond");
    fireEvent.click(button);
    await waitFor(() => {
      const data = screen.getByTestId("data");
      expect(data.textContent).toEqual("hello");
    });
  });
});
