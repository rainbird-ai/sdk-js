import React, { useContext } from "react";
import { render, waitFor, screen } from "@testing-library/react";
import * as base from "@rainbird/sdk";

import {
  RainbirdProvider,
  InteractionContext,
  InteractionProvider,
} from "../../context";
import { useQuery } from "..";

jest.mock("@rainbird/sdk");

describe("useQuery", () => {
  beforeEach(async () => {
    base.start = jest.fn(async () => ({
      sessionID: "thisIsTheSessionID",
      kmVersionID: "thisIsTheKmVersionID",
    }));
  });

  afterEach(() => jest.clearAllMocks());

  it("should call base sdk response", async () => {
    base.query = jest.fn(async () => ({}));
    const Query = () => {
      const { data } = useContext(InteractionContext);
      useQuery("Ellie", "speaks", "french");
      return <div>{data && <p>response</p>}</div>;
    };

    render(
      <RainbirdProvider
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
        sessionID="thisIsTheSessionID"
      >
        <InteractionProvider>
          {() => (
            <Query>
              <p>Hello!</p>
            </Query>
          )}
        </InteractionProvider>
      </RainbirdProvider>,
    );

    await waitFor(() => {
      expect(base.query).toHaveBeenCalledTimes(1);
      expect(base.query).toHaveBeenCalledWith(
        "/rainbird",
        "thisIsTheSessionID",
        "Ellie",
        "speaks",
        "french",
        undefined,
      );
    });
  });

  it("provides options to response", async () => {
    base.query = jest.fn(async () => ({}));
    const Query = () => {
      const { data } = useContext(InteractionContext);
      useQuery("Ellie", "speaks", "french");
      return <div>{data && <p>response</p>}</div>;
    };

    render(
      <RainbirdProvider
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
        sessionID="thisIsTheSessionID"
        options={{}}
      >
        <InteractionProvider>
          {() => (
            <Query>
              <p>Hello!</p>
            </Query>
          )}
        </InteractionProvider>
      </RainbirdProvider>,
    );

    await waitFor(() => {
      expect(base.query).toHaveBeenCalledTimes(1);
      expect(base.query).toHaveBeenCalledWith(
        "/rainbird",
        "thisIsTheSessionID",
        "Ellie",
        "speaks",
        "french",
        {},
      );
    });
  });

  it("sets interaction error state", async () => {
    base.query = jest.fn(async () => {
      throw new Error();
    });
    const Query = ({ query }) => {
      useQuery("Ellie", "speaks", "french");
      return <div>{query.error && <p data-testid="error">error</p>}</div>;
    };

    render(
      <RainbirdProvider
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
        sessionID="thisIsTheSessionID"
        options={{}}
      >
        <InteractionProvider>
          {(query) => <Query query={query} />}
        </InteractionProvider>
      </RainbirdProvider>,
    );

    await waitFor(() => {
      const error = screen.getByTestId("error");
      expect(error.textContent).toEqual("error");
    });
  });

  it("shows loading state", async () => {
    base.query = jest.fn(async () => {});
    const Query = ({ query }) => {
      useQuery("Ellie", "speaks", "french");
      return <div>{query.loading && <p data-testid="loading">loading</p>}</div>;
    };

    render(
      <RainbirdProvider
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
        sessionID="thisIsTheSessionID"
        options={{}}
      >
        <InteractionProvider>
          {(query) => <Query query={query} />}
        </InteractionProvider>
      </RainbirdProvider>,
    );

    await waitFor(() => {
      const loading = screen.getByTestId("loading");
      expect(loading.textContent).toEqual("loading");
    });
  });

  it("shows success state", async () => {
    base.query = jest.fn(async () => ({ message: "hello" }));
    const Query = ({ query }) => {
      useQuery("Ellie", "speaks", "french");
      return (
        <div>
          {query.data && <p data-testid="success">{query.data.message}</p>}
        </div>
      );
    };

    render(
      <RainbirdProvider
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
        sessionID="thisIsTheSessionID"
        options={{}}
      >
        <InteractionProvider>
          {(query) => <Query query={query} />}
        </InteractionProvider>
      </RainbirdProvider>,
    );

    await waitFor(() => {
      const success = screen.getByTestId("success");
      expect(success.textContent).toEqual("hello");
    });
  });
});
