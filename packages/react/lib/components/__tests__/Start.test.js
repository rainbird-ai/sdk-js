import React from "react";
import { render, waitFor, screen } from "@testing-library/react";
import * as base from "@rainbird/sdk";

import { RainbirdProvider } from "../../context";
import { Start } from "..";

jest.mock("@rainbird/sdk");

describe("Start", () => {
  afterEach(() => jest.clearAllMocks());

  it("Does not crash if there are no children", () => {
    render(
      <RainbirdProvider>
        <Start />
      </RainbirdProvider>,
    );
  });

  it("Passes children the data and renders loading when waiting for data to be returned", async () => {
    render(
      <RainbirdProvider>
        <Start>
          {(data) => {
            if (data.loading) return <p data-testid="loading">loading</p>;
            if (data.error) return <p data-testid="error">error</p>;
            return <p data-testid="hello">hello</p>;
          }}
        </Start>
      </RainbirdProvider>,
    );

    expect(base.start).toHaveBeenCalled();
    await waitFor(() => {
      const loading = screen.getByTestId("loading");
      expect(loading.textContent).toEqual("loading");
    });
  });

  it("Renders error when an error is present", async () => {
    base.start = jest.fn(() => {
      throw new Error();
    });
    render(
      <RainbirdProvider>
        <Start>
          {(data) => {
            if (data.loading) return <p data-testid="loading">loading</p>;
            if (data.error) return <p data-testid="error">error</p>;
            return <p data-testid="hello">hello</p>;
          }}
        </Start>
      </RainbirdProvider>,
    );

    expect(base.start).toHaveBeenCalled();
    await waitFor(() => {
      const error = screen.getByTestId("error");
      expect(error.textContent).toEqual("error");
    });
  });

  it("Renders data when data is returned", async () => {
    base.start = jest.fn(() => {
      return { text: "hello" };
    });
    render(
      <RainbirdProvider>
        <Start>
          {(start) => {
            if (start.loading) return <p data-testid="loading">loading</p>;
            if (start.error) return <p data-testid="error">error</p>;
            if (start.data)
              return <p data-testid="success">{start.data.text}</p>;
            return <p>hello</p>;
          }}
        </Start>
      </RainbirdProvider>,
    );

    expect(base.start).toHaveBeenCalled();
    await waitFor(() => {
      const error = screen.getByTestId("success");
      expect(error.textContent).toEqual("hello");
    });
  });
});
