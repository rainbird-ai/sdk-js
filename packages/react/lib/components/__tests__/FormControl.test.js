import React from "react";
import { render, screen } from "@testing-library/react";

import { FormControl } from "../FormControl";

describe("FormControl", () => {
  it("Presents the singleString component when the question matches", () => {
    const data = {
      subject: "ellie",
      dataType: "string",
      relationship: "lives in",
      type: "Second Form Object",
      plural: false,
      allowCF: false,
      allowUnknown: false,
      canAdd: false,
      prompt: "Which country is the lives in of ellie?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <FormControl
        data={data}
        singleString={<p data-testid="singleString">Single string</p>}
      />,
    );

    const singleString = screen.getByTestId("singleString");
    expect(singleString.textContent).toEqual("Single string");
  });

  it("Presents the singleString component and the certainty select when the question matches", () => {
    const data = {
      subject: "ellie",
      dataType: "string",
      relationship: "lives in",
      type: "Second Form Object",
      plural: false,
      allowCF: true,
      allowUnknown: false,
      canAdd: false,
      prompt: "Which country is the lives in of ellie?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <div>
        <FormControl
          data={data}
          singleString={<p data-testid="singleString">Single string</p>}
          certaintySelect={
            <p data-testid="certaintySelect">Certainty select</p>
          }
        />
      </div>,
    );

    const singleString = screen.getByTestId("singleString");
    expect(singleString.textContent).toEqual("Single string");
    const certaintySelect = screen.getByTestId("certaintySelect");
    expect(certaintySelect.textContent).toEqual("Certainty select");
  });

  it("Presents the singleStringAdd component when the question matches", () => {
    const data = {
      subject: "ellie",
      dataType: "string",
      relationship: "lives in",
      type: "Second Form Object",
      plural: false,
      allowCF: false,
      allowUnknown: false,
      canAdd: true,
      prompt: "Which country is the lives in of ellie?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <FormControl
        data={data}
        singleStringAdd={<p data-testid="singleStringAdd">Single string add</p>}
      />,
    );

    const singleString = screen.getByTestId("singleStringAdd");
    expect(singleString.textContent).toEqual("Single string add");
  });

  it("Presents the singleStringAdd component and the certainty select when the question matches", () => {
    const data = {
      subject: "ellie",
      dataType: "string",
      relationship: "lives in",
      type: "Second Form Object",
      plural: false,
      allowCF: true,
      allowUnknown: false,
      canAdd: true,
      prompt: "Which country is the lives in of ellie?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <div>
        <FormControl
          data={data}
          singleStringAdd={
            <p data-testid="singleStringAdd">Single string add</p>
          }
          certaintySelect={
            <p data-testid="certaintySelect">Certainty select</p>
          }
        />
      </div>,
    );

    const singleStringAdd = screen.getByTestId("singleStringAdd");
    expect(singleStringAdd.textContent).toEqual("Single string add");
    const certaintySelect = screen.getByTestId("certaintySelect");
    expect(certaintySelect.textContent).toEqual("Certainty select");
  });

  it("Presents the multiStringAdd component and the certainty when the question matches", () => {
    const data = {
      subject: "ellie",
      dataType: "string",
      relationship: "lives in",
      type: "Second Form Object",
      plural: true,
      allowCF: true,
      allowUnknown: false,
      canAdd: true,
      prompt: "Which country is the lives in of ellie?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <FormControl
        data={data}
        multiStringAdd={<p data-testid="multiStringAdd">Multi string add</p>}
        certaintySelect={<p data-testid="certaintySelect">Certainty select</p>}
      />,
    );

    const singleString = screen.getByTestId("multiStringAdd");
    expect(singleString.textContent).toEqual("Multi string add");
    const certaintySelect = screen.getByTestId("certaintySelect");
    expect(certaintySelect.textContent).toEqual("Certainty select");
  });

  it("Presents the multiStringAdd component when the question matches", () => {
    const data = {
      subject: "ellie",
      dataType: "string",
      relationship: "lives in",
      type: "Second Form Object",
      plural: true,
      allowCF: false,
      allowUnknown: false,
      canAdd: true,
      prompt: "Which country is the lives in of ellie?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <FormControl
        data={data}
        multiStringAdd={<p data-testid="multiStringAdd">Multi string add</p>}
      />,
    );

    const singleString = screen.getByTestId("multiStringAdd");
    expect(singleString.textContent).toEqual("Multi string add");
  });

  it("Presents the multiString component when the question matches", () => {
    const data = {
      subject: "ellie",
      dataType: "string",
      relationship: "lives in",
      type: "Second Form Object",
      plural: true,
      allowCF: false,
      allowUnknown: false,
      canAdd: false,
      prompt: "Which country is the lives in of ellie?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <FormControl
        data={data}
        multiString={<p data-testid="multiString">Multi string</p>}
      />,
    );

    const singleString = screen.getByTestId("multiString");
    expect(singleString.textContent).toEqual("Multi string");
  });

  it("Presents the multiString component and the certainty when the question matches", () => {
    const data = {
      subject: "ellie",
      dataType: "string",
      relationship: "lives in",
      type: "Second Form Object",
      plural: true,
      allowCF: true,
      allowUnknown: false,
      canAdd: false,
      prompt: "Which country is the lives in of ellie?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <FormControl
        data={data}
        multiString={<p data-testid="multiString">Multi string</p>}
        certaintySelect={<p data-testid="certaintySelect">Certainty select</p>}
      />,
    );

    const singleString = screen.getByTestId("multiString");
    expect(singleString.textContent).toEqual("Multi string");
    const certaintySelect = screen.getByTestId("certaintySelect");
    expect(certaintySelect.textContent).toEqual("Certainty select");
  });

  it("Presents the multiNumberAdd component and the certainty when the question matches", () => {
    const data = {
      subject: "dan",
      dataType: "number",
      relationship: "is years old",
      type: "Second Form Object",
      plural: true,
      allowCF: true,
      allowUnknown: false,
      canAdd: true,
      prompt: "What age is Dan?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <FormControl
        data={data}
        multiNumberAdd={<p data-testid="multiNumberAdd">Multi number add</p>}
        certaintySelect={<p data-testid="certaintySelect">Certainty select</p>}
      />,
    );

    const multiNumber = screen.getByTestId("multiNumberAdd");
    expect(multiNumber.textContent).toEqual("Multi number add");
    const certaintySelect = screen.getByTestId("certaintySelect");
    expect(certaintySelect.textContent).toEqual("Certainty select");
  });

  it("Presents the multiNumberAdd component when the question matches", () => {
    const data = {
      subject: "dan",
      dataType: "number",
      relationship: "is years old",
      type: "Second Form Object",
      plural: true,
      allowCF: true,
      allowUnknown: false,
      canAdd: true,
      prompt: "What age is Dan?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <FormControl
        data={data}
        multiNumberAdd={<p data-testid="multiNumberAdd">Multi number add</p>}
      />,
    );

    const multiNumber = screen.getByTestId("multiNumberAdd");
    expect(multiNumber.textContent).toEqual("Multi number add");
  });

  it("Presents the multiNumber component when the question matches", () => {
    const data = {
      subject: "dan",
      dataType: "number",
      relationship: "is years old",
      type: "Second Form Object",
      plural: true,
      allowCF: true,
      allowUnknown: false,
      canAdd: false,
      prompt: "What age is Dan?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <FormControl
        data={data}
        multiNumber={<p data-testid="multiNumber">Multi number</p>}
      />,
    );

    const multiNumber = screen.getByTestId("multiNumber");
    expect(multiNumber.textContent).toEqual("Multi number");
  });

  it("Presents the multiNumber component and the certainty when the question matches", () => {
    const data = {
      subject: "dan",
      dataType: "number",
      relationship: "is years old",
      type: "Second Form Object",
      plural: true,
      allowCF: true,
      allowUnknown: false,
      canAdd: false,
      prompt: "What age is Dan?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <FormControl
        data={data}
        multiNumber={<p data-testid="multiNumber">Multi number</p>}
        certaintySelect={<p data-testid="certaintySelect">Certainty select</p>}
      />,
    );

    const multiNumber = screen.getByTestId("multiNumber");
    expect(multiNumber.textContent).toEqual("Multi number");
    const certaintySelect = screen.getByTestId("certaintySelect");
    expect(certaintySelect.textContent).toEqual("Certainty select");
  });

  it("Presents the singleDate component when the question matches", () => {
    const data = {
      subject: "ellie",
      dataType: "date",
      relationship: "lives in",
      type: "Second Form Object",
      plural: false,
      allowCF: false,
      allowUnknown: false,
      canAdd: false,
      prompt: "Which country is the lives in of ellie?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <FormControl
        data={data}
        singleDate={<p data-testid="singleDate">Single date</p>}
      />,
    );

    const singleString = screen.getByTestId("singleDate");
    expect(singleString.textContent).toEqual("Single date");
  });

  it("Presents the singleDate component and the certainty when the question matches", () => {
    const data = {
      subject: "ellie",
      dataType: "date",
      relationship: "lives in",
      type: "Second Form Object",
      plural: false,
      allowCF: true,
      allowUnknown: false,
      canAdd: false,
      prompt: "Which country is the lives in of ellie?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <FormControl
        data={data}
        singleDate={<p data-testid="singleDate">Single date</p>}
        certaintySelect={<p data-testid="certaintySelect">Certainty select</p>}
      />,
    );

    const singleString = screen.getByTestId("singleDate");
    expect(singleString.textContent).toEqual("Single date");
    const certaintySelect = screen.getByTestId("certaintySelect");
    expect(certaintySelect.textContent).toEqual("Certainty select");
  });

  it("Presents the multiDate component when the question matches", () => {
    const data = {
      subject: "dan",
      dataType: "date",
      relationship: "is years old",
      type: "Second Form Object",
      plural: true,
      allowCF: false,
      allowUnknown: false,
      canAdd: false,
      prompt: "What age is Dan?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <FormControl
        data={data}
        multiDate={<p data-testid="multiDate">Multi date</p>}
      />,
    );

    const multiDate = screen.getByTestId("multiDate");
    expect(multiDate.textContent).toEqual("Multi date");
  });

  it("Presents the multiDate component and the certainty when the question matches", () => {
    const data = {
      subject: "dan",
      dataType: "date",
      relationship: "is years old",
      type: "Second Form Object",
      plural: true,
      allowCF: true,
      allowUnknown: false,
      canAdd: true,
      prompt: "What age is Dan?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <FormControl
        data={data}
        multiDate={<p data-testid="multiDate">Multi date</p>}
        certaintySelect={<p data-testid="certaintySelect">Certainty select</p>}
      />,
    );

    const multiDate = screen.getByTestId("multiDate");
    expect(multiDate.textContent).toEqual("Multi date");
    const certaintySelect = screen.getByTestId("certaintySelect");
    expect(certaintySelect.textContent).toEqual("Certainty select");
  });

  it("Presents the singleNumber component when the question matches", () => {
    const data = {
      subject: "ellie",
      dataType: "number",
      relationship: "lives in",
      type: "Second Form Object",
      plural: false,
      allowCF: false,
      allowUnknown: false,
      canAdd: false,
      prompt: "Which country is the lives in of ellie?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <FormControl
        data={data}
        singleNumber={<p data-testid="singleNumber">Single number</p>}
      />,
    );

    const singleString = screen.getByTestId("singleNumber");
    expect(singleString.textContent).toEqual("Single number");
  });

  it("Presents the singleNumber component and the certainty slider when the question matches", () => {
    const data = {
      subject: "ellie",
      dataType: "number",
      relationship: "lives in",
      type: "Second Form Object",
      plural: false,
      allowCF: true,
      allowUnknown: false,
      canAdd: false,
      prompt: "Which country is the lives in of ellie?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <FormControl
        data={data}
        singleNumber={<p data-testid="singleNumber">Single number</p>}
        certaintySelect={<p data-testid="certaintySelect">Certainty select</p>}
      />,
    );

    const singleString = screen.getByTestId("singleNumber");
    expect(singleString.textContent).toEqual("Single number");
    const certaintySelect = screen.getByTestId("certaintySelect");
    expect(certaintySelect.textContent).toEqual("Certainty select");
  });

  it("Presents the singleTruth component when the question matches", () => {
    const data = {
      subject: "ellie",
      dataType: "truth",
      relationship: "lives in",
      type: "Second Form Object",
      plural: false,
      allowCF: false,
      allowUnknown: false,
      canAdd: false,
      prompt: "Which country is the lives in of ellie?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <FormControl
        data={data}
        singleTruth={<p data-testid="singleTruth">Single truth</p>}
      />,
    );

    const singleString = screen.getByTestId("singleTruth");
    expect(singleString.textContent).toEqual("Single truth");
  });

  it("Presents the singleTruth component and the certainty slider when the question matches", () => {
    const data = {
      subject: "ellie",
      dataType: "truth",
      relationship: "lives in",
      type: "Second Form Object",
      plural: false,
      allowCF: true,
      allowUnknown: false,
      canAdd: false,
      prompt: "Which country is the lives in of ellie?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <FormControl
        data={data}
        singleTruth={<p data-testid="singleTruth">Single truth</p>}
        certaintySelect={<p data-testid="certaintySelect">Certainty select</p>}
      />,
    );

    const singleString = screen.getByTestId("singleTruth");
    expect(singleString.textContent).toEqual("Single truth");
    const certaintySelect = screen.getByTestId("certaintySelect");
    expect(certaintySelect.textContent).toEqual("Certainty select");
  });

  it("Presents the singleDate component when the question is canAdd", () => {
    const data = {
      subject: "ellie",
      dataType: "date",
      relationship: "lives in",
      type: "Second Form Object",
      plural: false,
      allowCF: false,
      allowUnknown: false,
      canAdd: true,
      prompt: "Which country is the lives in of ellie?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <FormControl
        data={data}
        singleDate={<p data-testid="singleDate">Single date</p>}
      />,
    );

    const singleString = screen.getByTestId("singleDate");
    expect(singleString.textContent).toEqual("Single date");
  });

  it("Presents the singleDate component and certainty slider when the question is canAdd", () => {
    const data = {
      subject: "ellie",
      dataType: "date",
      relationship: "lives in",
      type: "Second Form Object",
      plural: false,
      allowCF: true,
      allowUnknown: false,
      canAdd: true,
      prompt: "Which country is the lives in of ellie?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <FormControl
        data={data}
        singleDate={<p data-testid="singleDate">Single date</p>}
        certaintySelect={<p data-testid="certaintySelect">Certainty select</p>}
      />,
    );

    const singleString = screen.getByTestId("singleDate");
    expect(singleString.textContent).toEqual("Single date");
    const certaintySelect = screen.getByTestId("certaintySelect");
    expect(certaintySelect.textContent).toEqual("Certainty select");
  });

  it("Presents the singleNumber component when the question is canAdd", () => {
    const data = {
      subject: "ellie",
      dataType: "number",
      relationship: "lives in",
      type: "Second Form Object",
      plural: false,
      allowCF: false,
      allowUnknown: false,
      canAdd: true,
      prompt: "Which country is the lives in of ellie?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <FormControl
        data={data}
        singleNumber={<p data-testid="singleNumber">Single number</p>}
      />,
    );

    const singleString = screen.getByTestId("singleNumber");
    expect(singleString.textContent).toEqual("Single number");
  });

  it("Presents the singleNumber component and the certainty slider when the question is canAdd", () => {
    const data = {
      subject: "ellie",
      dataType: "number",
      relationship: "lives in",
      type: "Second Form Object",
      plural: false,
      allowCF: true,
      allowUnknown: false,
      canAdd: true,
      prompt: "Which country is the lives in of ellie?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <FormControl
        data={data}
        singleNumber={<p data-testid="singleNumber">Single number</p>}
        certaintySelect={<p data-testid="certaintySelect">Certainty select</p>}
      />,
    );

    const singleString = screen.getByTestId("singleNumber");
    expect(singleString.textContent).toEqual("Single number");
    const certaintySelect = screen.getByTestId("certaintySelect");
    expect(certaintySelect.textContent).toEqual("Certainty select");
  });

  it("Presents the singleTruth component when the question is canAdd", () => {
    const data = {
      subject: "ellie",
      dataType: "truth",
      relationship: "lives in",
      type: "Second Form Object",
      plural: false,
      allowCF: false,
      allowUnknown: false,
      canAdd: true,
      prompt: "Which country is the lives in of ellie?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <FormControl
        data={data}
        singleTruth={<p data-testid="singleTruth">Single truth</p>}
      />,
    );

    const singleString = screen.getByTestId("singleTruth");
    expect(singleString.textContent).toEqual("Single truth");
  });

  it("Presents the singleTruth component and certainty slider when the question is canAdd", () => {
    const data = {
      subject: "ellie",
      dataType: "truth",
      relationship: "lives in",
      type: "Second Form Object",
      plural: false,
      allowCF: true,
      allowUnknown: false,
      canAdd: true,
      prompt: "Which country is the lives in of ellie?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <FormControl
        data={data}
        singleTruth={<p data-testid="singleTruth">Single truth</p>}
        certaintySelect={<p data-testid="certaintySelect">Certainty select</p>}
      />,
    );

    const singleString = screen.getByTestId("singleTruth");
    expect(singleString.textContent).toEqual("Single truth");
    const certaintySelect = screen.getByTestId("certaintySelect");
    expect(certaintySelect.textContent).toEqual("Certainty select");
  });

  it("Presents the fallback if the question does not match a type", () => {
    const data = {
      subject: "ellie",
      dataType: "oops",
      relationship: "lives in",
      type: "Second Form Object",
      plural: false,
      allowCF: false,
      allowUnknown: false,
      canAdd: false,
      prompt: "Which country is the lives in of ellie?",
      knownAnswers: [],
      concepts: [],
    };

    render(
      <FormControl
        data={data}
        fallback={<p data-testid="fallback">Fallback</p>}
      />,
    );

    const singleString = screen.getByTestId("fallback");
    expect(singleString.textContent).toEqual("Fallback");
  });
});
