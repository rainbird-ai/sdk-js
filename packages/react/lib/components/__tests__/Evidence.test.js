import React from "react";
import { render, waitFor, screen, fireEvent } from "@testing-library/react";
import * as base from "@rainbird/sdk";

import { RainbirdProvider } from "../../context";
import { Evidence } from "..";

jest.mock("@rainbird/sdk");

describe("Evidence", () => {
  afterEach(() => jest.clearAllMocks());

  it("Does not crash if initialised with no children", () => {
    render(
      <RainbirdProvider>
        <Evidence />
      </RainbirdProvider>,
    );
  });

  it("Renders loading state when loading", async () => {
    base.evidence = jest.fn(async () => {});

    render(
      <RainbirdProvider>
        <Evidence>
          {(evidence) => (
            <div>
              <button
                type="button"
                data-testid="evidence"
                onClick={evidence.request}
              >
                evidence
              </button>
              {evidence.loading && <p data-testid="loading">loading</p>}
            </div>
          )}
        </Evidence>
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("evidence");
    fireEvent.click(button);
    await waitFor(() => {
      const loading = screen.getByTestId("loading");
      expect(loading.textContent).toEqual("loading");
    });
  });

  it("Renders error properly when error is encountered", async () => {
    base.evidence = jest.fn(async () => {
      throw new Error("error");
    });

    render(
      <RainbirdProvider>
        <Evidence>
          {(evidence) => (
            <div>
              <button
                type="button"
                data-testid="evidence"
                onClick={evidence.request}
              >
                evidence
              </button>
              {evidence.error && <p data-testid="error">error</p>}
            </div>
          )}
        </Evidence>
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("evidence");
    fireEvent.click(button);
    await waitFor(() => {
      const error = screen.getByTestId("error");
      expect(error.textContent).toEqual("error");
    });
  });

  it("Renders data correctly if response comes back with data", async () => {
    base.evidence = jest.fn(async () => {
      return { message: "hello" };
    });

    render(
      <RainbirdProvider>
        <Evidence>
          {(evidence) => (
            <div>
              <button
                type="button"
                data-testid="evidence"
                onClick={evidence.request}
              >
                evidence
              </button>
              {evidence.data && (
                <p data-testid="success">{evidence.data.message}</p>
              )}
            </div>
          )}
        </Evidence>
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("evidence");
    fireEvent.click(button);
    await waitFor(() => {
      const success = screen.getByTestId("success");
      expect(success.textContent).toEqual("hello");
    });
  });
});
