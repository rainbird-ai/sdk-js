import React from "react";
import { render, waitFor, screen } from "@testing-library/react";
import * as base from "@rainbird/sdk";

import { Rainbird } from "..";

jest.mock("@rainbird/sdk");

describe("Rainbird", () => {
  afterEach(() => jest.clearAllMocks());

  it("Renders the onLoad prop when loading start", async () => {
    base.start = jest.fn(async () => {
      return { sessionID: "1", kmVersionID: "1" };
    });
    render(
      <Rainbird
        onLoad={<p data-testid="loading">loading</p>}
        baseURL="www.rainbird.ai"
        kmID="1234"
        apiKey="4321"
        subject="Person"
        relationship="speaks"
        object="Language"
      />,
    );

    await waitFor(() => {
      const loading = screen.getByTestId("loading");
      expect(loading.textContent).toEqual("loading");
    });
  });

  it("Renders the onLoad prop when loading query", async () => {
    base.start = jest.fn(() => {
      return { sessionID: "1", kmVersionID: "1" };
    });
    base.query = jest.fn(async () => {
      return {};
    });
    render(
      <Rainbird
        onLoad={<p data-testid="loading">loading</p>}
        baseURL="www.rainbird.ai"
        kmID="1234"
        apiKey="4321"
        subject="Person"
        relationship="speaks"
        object="Language"
      />,
    );

    await waitFor(() => {
      const loading = screen.getByTestId("loading");
      expect(loading.textContent).toEqual("loading");
    });
  });

  it("Renders the onError prop when an error is returned from start", async () => {
    base.start = jest.fn(async () => {
      throw new Error("error!");
    });
    render(
      <Rainbird
        onError={(err) => <p data-testid="error">{err.message}</p>}
        baseURL="www.rainbird.ai"
        kmID="1234"
        apiKey="4321"
        subject="Person"
        relationship="speaks"
        object="Language"
      />,
    );

    await waitFor(() => {
      const error = screen.getByTestId("error");
      expect(error.textContent).toEqual("error!");
    });
  });

  it("Renders the onError prop when an error is returned from query", async () => {
    base.start = jest.fn(async () => {
      return { sessionID: "1", kmVersionID: "1" };
    });
    base.query = jest.fn(async () => {
      throw new Error("error!");
    });
    render(
      <Rainbird
        onError={(err) => <p data-testid="error">{err.message}</p>}
        baseURL="www.rainbird.ai"
        kmID="1234"
        apiKey="4321"
        subject="Person"
        relationship="speaks"
        object="Language"
      />,
    );

    await waitFor(() => {
      const error = screen.getByTestId("error");
      expect(error.textContent).toEqual("error!");
    });
  });

  it("Renders children when data has been received from both requests", async () => {
    base.start = jest.fn(async () => {
      return { sessionID: "1", kmVersionID: "1" };
    });
    base.query = jest.fn(async () => {
      return { message: "hello" };
    });

    const Child = ({ data }) => {
      return <p data-testid="child">{data.message}</p>;
    };
    render(
      <Rainbird
        baseURL="www.rainbird.ai"
        kmID="1234"
        apiKey="4321"
        subject="Person"
        relationship="speaks"
        object="Language"
      >
        {(query) => <Child data={query} />}
      </Rainbird>,
    );

    await waitFor(() => {
      const child = screen.getByTestId("child");
      expect(child.textContent).toEqual("hello");
    });
  });
});
