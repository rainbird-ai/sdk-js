import { useResponse } from "../hooks";

export const Response = ({ children }) => {
  const request = useResponse();

  return children ? children({ request }) : null;
};
