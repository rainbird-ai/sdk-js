import { useInject } from "../hooks";

export const Inject = ({ children }) => {
  const [data, request] = useInject();

  return children ? children({ ...data, request }) : null;
};
