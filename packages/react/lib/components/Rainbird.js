import React from "react";

import { InteractionProvider, RainbirdProvider } from "../context";
import { Query } from "./Query";
import { Start } from "./Start";

export const Rainbird = ({
  apiKey,
  baseURL,
  children,
  kmID,
  kmVersionID,
  object,
  onError,
  onLoad,
  options,
  relationship,
  sessionID,
  subject,
}) => (
  <RainbirdProvider
    apiKey={apiKey}
    baseURL={baseURL}
    kmID={kmID}
    kmVersionID={kmVersionID}
    options={options}
    sessionID={sessionID}
  >
    {!sessionID ? (
      <Start onLoad={onLoad} onError={onError}>
        {(start) => (
          <>
            {start.loading && <>{onLoad}</>}
            {start.error && <>{onError(start.error)}</>}
            {start.data && (
              <InteractionProvider>
                {({ data, loading, error }) => (
                  <Query
                    subject={subject}
                    relationship={relationship}
                    object={object}
                    onLoad={onLoad}
                    onError={onError}
                  >
                    {() => (
                      <>
                        {loading && <>{onLoad}</>}
                        {error && <>{onError(error)}</>}
                        {data && (
                          <>
                            {children
                              ? children({
                                  ...data,
                                  sessionID: start.data.sessionID,
                                })
                              : null}
                          </>
                        )}
                      </>
                    )}
                  </Query>
                )}
              </InteractionProvider>
            )}
          </>
        )}
      </Start>
    ) : (
      <InteractionProvider>
        {({ data, loading, error }) => (
          <Query
            subject={subject}
            relationship={relationship}
            object={object}
            onLoad={onLoad}
            onError={onError}
          >
            {() => (
              <>
                {loading && <>{onLoad}</>}
                {error && <>{onError(error)}</>}
                {data && (
                  <>{children ? children({ ...data, sessionID }) : null}</>
                )}
              </>
            )}
          </Query>
        )}
      </InteractionProvider>
    )}
  </RainbirdProvider>
);
