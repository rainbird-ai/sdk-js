import React from "react";
import { getFormInputType } from "../helpers";

export const FormControl = ({
  data,
  multiString,
  singleString,
  multiStringAdd,
  singleStringAdd,
  singleNumber,
  singleDate,
  multiDate,
  singleTruth,
  certaintySelect,
  multiNumber,
  multiNumberAdd,
  fallback,
}) => {
  const formInputs = getFormInputType(data);

  return (
    <>
      {formInputs.map((input) => {
        switch (input.control) {
          case "single-string-canAdd":
            return (
              <React.Fragment key={input.control}>
                <>{singleStringAdd}</>
                <>{input.certainty && certaintySelect}</>
              </React.Fragment>
            );
          case "single-string":
            return (
              <React.Fragment key={input.control}>
                <>{singleString}</>
                <>{input.certainty && certaintySelect}</>
              </React.Fragment>
            );
          case "multi-string-canAdd":
            return (
              <React.Fragment key={input.control}>
                <>{multiStringAdd}</>
                <>{input.certainty && certaintySelect}</>
              </React.Fragment>
            );
          case "multi-string":
            return (
              <React.Fragment key={input.control}>
                <>{multiString}</>
                <>{input.certainty && certaintySelect}</>
              </React.Fragment>
            );
          case "single-date-canAdd":
            return (
              <React.Fragment key={input.control}>
                <>{singleDate}</>
                <>{input.certainty && certaintySelect}</>
              </React.Fragment>
            );
          case "single-number-canAdd":
            return (
              <React.Fragment key={input.control}>
                <>{singleNumber}</>
                <>{input.certainty && certaintySelect}</>
              </React.Fragment>
            );
          case "single-date":
            return (
              <React.Fragment key={input.control}>
                <>{singleDate}</>
                <>{input.certainty && certaintySelect}</>
              </React.Fragment>
            );
          case "multi-date":
          case "multi-date-canAdd":
            return (
              <React.Fragment key={input.control}>
                <>{multiDate}</>
                <>{input.certainty && certaintySelect}</>
              </React.Fragment>
            );
          case "single-number":
            return (
              <React.Fragment key={input.control}>
                <>{singleNumber}</>
                <>{input.certainty && certaintySelect}</>
              </React.Fragment>
            );
          case "multi-number-canAdd":
            return (
              <React.Fragment key={input.control}>
                <>{multiNumberAdd}</>
                <>{input.certainty && certaintySelect}</>
              </React.Fragment>
            );
          case "multi-number":
            return (
              <React.Fragment key={input.control}>
                <>{multiNumber}</>
                <>{input.certainty && certaintySelect}</>
              </React.Fragment>
            );
          case "single-truth":
          case "single-truth-canAdd":
          case "multi-truth":
            return (
              <React.Fragment key={input.control}>
                <>{singleTruth}</>
                <>{input.certainty && certaintySelect}</>
              </React.Fragment>
            );
          default:
            return (
              <React.Fragment key={input.control}>{fallback}</React.Fragment>
            );
        }
      })}
    </>
  );
};
