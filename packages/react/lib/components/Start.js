import { useStart } from "../hooks";

export const Start = ({ children }) => {
  const data = useStart();

  return children ? children(data) : null;
};
