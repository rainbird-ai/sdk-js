import React from "react";

import { useInteraction } from "../hooks";

export const withInteraction = (WrappedComponent) => (props) => {
  const interaction = useInteraction();
  return <WrappedComponent {...props} interaction={interaction} />;
};
