import React from "react";
import { render, waitFor, screen, fireEvent } from "@testing-library/react";
import * as base from "@rainbird/sdk";

import { RainbirdProvider } from "../../context";
import { withInject } from "../inject";

jest.mock("@rainbird/sdk");

describe("withInject", () => {
  it("Passes the sendInject function and evidence data to the decorated component", async () => {
    base.inject = jest.fn(() => {
      return { text: "hello" };
    });
    const Component = withInject(({ inject, sendInject }) => (
      <div>
        <button
          type="button"
          data-testid="inject"
          onClick={() => sendInject("hello")}
        >
          inject
        </button>
        {inject.data && <p data-testid="injectData">{inject.data.text}</p>}
      </div>
    ));

    render(
      <RainbirdProvider
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
        sessionID="thisIsTheSessionID"
      >
        <Component />
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("inject");
    fireEvent.click(button);
    await waitFor(() => {
      expect(base.inject).toHaveBeenCalledTimes(1);
      const injectData = screen.getByTestId("injectData");
      expect(injectData.textContent).toEqual("hello");
    });
  });
});
