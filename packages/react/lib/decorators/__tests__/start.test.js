import React from "react";
import { render, waitFor, screen } from "@testing-library/react";
import * as base from "@rainbird/sdk";

import { RainbirdProvider } from "../../context";
import { withStart } from "../start";

jest.mock("@rainbird/sdk");

describe("withQuery", () => {
  it("Passes the start data to the decorated component", async () => {
    base.start = jest.fn(() => {
      return { text: "hello" };
    });
    const Component = withStart(({ start }) => (
      <div>
        {start.data && <p data-testid="startData">{start.data.text}</p>}
      </div>
    ));

    render(
      <RainbirdProvider
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
        sessionID="thisIsTheSessionID"
        options={{}}
      >
        <Component />
      </RainbirdProvider>,
    );

    await waitFor(() => {
      expect(base.start).toHaveBeenCalledTimes(1);
      expect(base.start).toHaveBeenCalledWith(
        "/rainbird",
        "myApiKey",
        "1234",
        {},
      );
      const startData = screen.getByTestId("startData");
      expect(startData.textContent).toEqual("hello");
    });
  });
});
