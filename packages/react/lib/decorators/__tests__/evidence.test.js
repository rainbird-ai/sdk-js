import React from "react";
import { render, waitFor, screen, fireEvent } from "@testing-library/react";
import * as base from "@rainbird/sdk";

import { RainbirdProvider, InteractionProvider } from "../../context";
import { withEvidence } from "../evidence";

jest.mock("@rainbird/sdk");

describe("withEvidence", () => {
  it("Passes the getEvidence function and evidence data to the decorated component", async () => {
    base.evidence = jest.fn(() => {
      return { text: "hello" };
    });
    const Component = withEvidence(({ evidence, getEvidence }) => (
      <div>
        <button
          type="button"
          data-testid="evidence"
          onClick={() => getEvidence("hello")}
        >
          evidence
        </button>
        {evidence.data && (
          <p data-testid="evidenceData">{evidence.data.text}</p>
        )}
      </div>
    ));

    render(
      <RainbirdProvider
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
        sessionID="thisIsTheSessionID"
      >
        <InteractionProvider>{() => <Component />}</InteractionProvider>
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("evidence");
    fireEvent.click(button);
    await waitFor(() => {
      expect(base.evidence).toHaveBeenCalledTimes(1);
      const evidenceData = screen.getByTestId("evidenceData");
      expect(evidenceData.textContent).toEqual("hello");
    });
  });
});
