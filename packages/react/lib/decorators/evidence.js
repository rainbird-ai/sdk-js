import React from "react";

import { useEvidence } from "../hooks";

export const withEvidence = (WrappedComponent) => (props) => {
  const [evidence, getEvidence] = useEvidence();
  return (
    <WrappedComponent
      {...props}
      evidence={evidence}
      getEvidence={getEvidence}
    />
  );
};
