import React from "react";

import { useResponse } from "../hooks";

export const withResponse = (WrappedComponent) => (props) => {
  const sendResponse = useResponse();
  return <WrappedComponent {...props} sendResponse={sendResponse} />;
};
