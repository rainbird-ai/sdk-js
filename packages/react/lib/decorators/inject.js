import React from "react";

import { useInject } from "../hooks";

export const withInject = (WrappedComponent) => (props) => {
  const [inject, sendInject] = useInject();
  return (
    <WrappedComponent {...props} inject={inject} sendInject={sendInject} />
  );
};
