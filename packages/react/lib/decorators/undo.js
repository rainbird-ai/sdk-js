import React from "react";

import { useUndo } from "../hooks";

export const withUndo = (WrappedComponent) => (props) => {
  const undo = useUndo();
  return <WrappedComponent {...props} undo={undo} />;
};
