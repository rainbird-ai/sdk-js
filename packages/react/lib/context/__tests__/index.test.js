import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";

import { RainbirdContext, RainbirdProvider } from "..";

describe("RainbirdContext", () => {
  describe("Provider", () => {
    it("Renders children", () => {
      render(
        <RainbirdProvider kmID="123" apiKey="1234" baseURL="/hello">
          <p data-testid="child">hello</p>
        </RainbirdProvider>,
      );

      const child = screen.getByTestId("child");
      expect(child.textContent).toEqual("hello");
    });

    it("Stores the api key, sessionID, kmID and baseURL", () => {
      const Child = () => {
        const { kmID, apiKey, baseURL, sessionID } =
          React.useContext(RainbirdContext);

        return (
          <p>
            <li data-testid="kmID">{kmID}</li>
            <li data-testid="apiKey">{apiKey}</li>
            <li data-testid="baseURL">{baseURL}</li>
            <li data-testid="sessionID">{sessionID}</li>
          </p>
        );
      };

      render(
        <RainbirdProvider
          sessionID="sessionID"
          kmID="123"
          apiKey="1234"
          baseURL="/hello"
        >
          <Child />
        </RainbirdProvider>,
      );
      const kmIDLi = screen.getByTestId("kmID");
      expect(kmIDLi.textContent).toEqual("123");
      const apiKeyLi = screen.getByTestId("apiKey");
      expect(apiKeyLi.textContent).toEqual("1234");
      const baseURLLi = screen.getByTestId("baseURL");
      expect(baseURLLi.textContent).toEqual("/hello");
      const sessionIDLi = screen.getByTestId("sessionID");
      expect(sessionIDLi.textContent).toEqual("sessionID");
    });

    it("The sessionID can be updated", () => {
      const Child = () => {
        const { sessionID, setSessionID } = React.useContext(RainbirdContext);

        return (
          <div>
            <button
              type="button"
              onClick={() => setSessionID("updatedSessionID")}
            >
              Update Session
            </button>
            <li data-testid="sessionID">{sessionID}</li>
          </div>
        );
      };

      const { getByText } = render(
        <RainbirdProvider sessionID="sessionID">
          <Child />
        </RainbirdProvider>,
      );
      fireEvent.click(getByText(/Update Session/));
      const sessionIDLi = screen.getByTestId("sessionID");
      expect(sessionIDLi.textContent).toEqual("updatedSessionID");
    });
  });
});
