export const getFormInputType = (question) => {
  // No question so escape
  if (!question) return [];

  // Determine which properties will build up the ordered control string
  const { allowCF, canAdd, dataType, plural, type: qType } = question;
  const control = [
    ...[plural ? "multi" : "single"],
    ...[dataType || (qType === "First Form" && "truth") || []],
    ...(canAdd ? ["canAdd"] : []),
  ].join("-");

  return [{ certainty: allowCF, control }];
};
