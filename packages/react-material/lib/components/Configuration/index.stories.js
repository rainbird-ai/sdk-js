import React, { useState } from "react";

import { Configuration } from ".";

// eslint-disable-next-line import/no-default-export
export default {
  component: Configuration,
  title: "Configuration",
  argTypes: {
    allowCustomSession: { type: "boolean" },
  },
};

const Template = (args) => {
  const [, setObject] = useState();
  const [, setSubject] = useState();

  return <Configuration {...args} setObject={setObject} subject={setSubject} />;
};

export const Default = Template.bind({});
export const SessionIdEnabled = Template.bind({});

Default.args = { allowCustomSession: false };
SessionIdEnabled.args = { allowCustomSession: true };
