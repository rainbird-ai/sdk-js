import React, { useState } from "react";
import { Typography, Box, Button, TextField } from "@material-ui/core";

export const Configuration = ({
  allowCustomSession,
  setObject,
  setSessionID,
  setSubject,
}) => {
  const [configObject, setConfigObject] = useState();
  const [configSessionID, setConfigSessionID] = useState();
  const [configSubject, setConfigSubject] = useState();

  // Update subject, object and sessionID if applicable.
  const handleSubmit = (event) => {
    event.preventDefault();
    if (configObject) setObject(configObject);
    if (configSessionID) setSessionID(configSessionID);
    if (configSubject) setSubject(configSubject);
  };

  return (
    <>
      <Box
        autoComplete="off"
        noValidate
        style={{ height: "calc(100% - 2.3rem)" }}
        sx={{ "& .MuiTextField-root": { m: 1, width: "100%" } }}
      >
        <ConfigTitle />
        <ConfigTextField
          handleSubmit={handleSubmit}
          label="Subject"
          setState={setConfigSubject}
        />
        <ConfigTextField
          handleSubmit={handleSubmit}
          label="Object"
          setState={setConfigObject}
        />
        {allowCustomSession && (
          <ConfigTextField
            handleSubmit={handleSubmit}
            label="Session ID (Optional)"
            setState={setConfigSessionID}
          />
        )}
      </Box>
      <SubmitButton
        configObject={configObject}
        configSubject={configSubject}
        handleSubmit={handleSubmit}
      />
    </>
  );
};

const ConfigTitle = () => (
  <Typography component="h2" data-testid="prompt" id="prompt" variant="body1">
    Subject and/or Object not found. Please enter either/both.
  </Typography>
);

const ConfigTextField = ({ handleSubmit, setState, label }) => (
  <Box
    autoComplete="off"
    component="form"
    noValidate
    onSubmit={(event) => handleSubmit(event)}
    sx={{ "& .MuiTextField-root": { m: 1, width: "100%" } }}
  >
    <TextField
      data-testid={`${label.toLowerCase()}-pre-query`}
      fullWidth
      id="outlined-basic"
      label={label}
      margin="normal"
      name={label.toLowerCase()}
      onChange={(e) => setState(e.target.value)}
      variant="outlined"
    />
  </Box>
);

const SubmitButton = ({ configSubject, configObject, handleSubmit }) => (
  <Button
    aria-disabled={!configSubject && !configObject}
    aria-label="Submit"
    color="primary"
    data-testid="submit-config"
    disabled={!configSubject && !configObject}
    onClick={(event) => handleSubmit(event)}
    title="Submit config"
    type="button"
    variant="outlined"
    style={{ float: "right" }}
  >
    Submit
    <span style={{ display: "none" }}>config</span>
  </Button>
);
