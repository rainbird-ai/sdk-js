import { Rainbird as ReactRainbird } from "@rainbird/sdk-react";
import { RESPONSE_TYPE_QUESTION, RESPONSE_TYPE_RESULT } from "@rainbird/sdk";
import { Snackbar, CircularProgress } from "@material-ui/core";
import { ThemeProvider } from "@material-ui/core/styles";
import React from "react";
import Alert from "@material-ui/lab/Alert";

import { ErrorBoundary } from "../Error";
import { Interaction } from "../Interaction";
import { RBTheme } from "../../styles";
import { Result } from "../Result";

const handleOnLoad = () => (
  <CircularProgress
    aria-label="Loading indicator"
    data-testid="loading"
    role="status"
  />
);

const handleOnError = (error, onError) => {
  if (onError) return onError(error);
  return (
    <Snackbar anchorOrigin={{ horizontal: "center", vertical: "top" }} open>
      <Alert data-testid="error" severity="error">
        <span role="status">{error.message}</span>
      </Alert>
    </Snackbar>
  );
};

export const Rainbird = ({
  allowCustomSession,
  allowUndoResults,
  apiKey,
  appURL,
  baseURL,
  displayEvidence,
  evidenceKey,
  kmID,
  object,
  onError,
  options,
  relationship,
  reset,
  sessionID,
  subject,
  theme = RBTheme,
}) => (
  <ThemeProvider theme={theme}>
    <ErrorBoundary onError={onError}>
      <ReactRainbird
        apiKey={apiKey}
        baseURL={baseURL}
        kmID={kmID}
        object={object}
        onError={(error) => handleOnError(error, onError)}
        onLoad={handleOnLoad()}
        options={options}
        relationship={relationship}
        sessionID={sessionID}
        subject={subject}
      >
        {({ data, sessionID: queryID, type }) => {
          if (type === RESPONSE_TYPE_QUESTION)
            return (
              <Interaction
                allowCustomSession={allowCustomSession}
                data={data}
                reset={reset}
                sessionID={sessionID || queryID}
              />
            );
          if (type === RESPONSE_TYPE_RESULT)
            return (
              <Result
                allowCustomSession={allowCustomSession}
                allowUndoResults={allowUndoResults}
                appURL={appURL}
                data={data}
                displayEvidence={displayEvidence}
                evidenceKey={evidenceKey}
                reset={reset}
                sessionID={sessionID || queryID}
              />
            );
          return <></>;
        }}
      </ReactRainbird>
    </ErrorBoundary>
  </ThemeProvider>
);
