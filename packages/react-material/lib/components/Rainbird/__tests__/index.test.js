import React from "react";
import { screen, render, waitFor } from "@testing-library/react";
// eslint-disable-next-line import/no-extraneous-dependencies
import * as base from "@rainbird/sdk";
import { Rainbird } from "..";

jest.mock("@rainbird/sdk");

describe("Rainbird", () => {
  it("Calls start with the correct params", () => {
    base.start = jest.fn();
    render(
      <Rainbird
        kmID="kmID"
        baseURL="http://test-api.baseURL.ai"
        apiKey="apiKey"
        options={{}}
        subject="Ellie"
        relationship="speaks"
        object="Language"
      />,
    );
    expect(base.start).toHaveBeenCalledWith(
      "http://test-api.baseURL.ai",
      "apiKey",
      "kmID",
      {},
    );
  });

  it("Calls query with the correct params", async () => {
    base.start = jest.fn(() => ({ sessionID: "sessionID" }));
    base.query = jest.fn();
    render(
      <Rainbird
        kmID="kmID"
        baseURL="http://test-api.baseURL.ai"
        apiKey="apiKey"
        options={{}}
        subject="Ellie"
        relationship="speaks"
        object="Language"
      />,
    );
    await waitFor(() => {
      expect(base.query).toHaveBeenCalledWith(
        "http://test-api.baseURL.ai",
        "sessionID",
        "Ellie",
        "speaks",
        "Language",
        {},
      );
    });
  });

  it("Displays error if error is thrown", () => {
    base.start = jest.fn(() => {
      throw new Error("Error");
    });
    render(
      <Rainbird
        kmID="kmID"
        baseURL="http://test-api.baseURL.ai"
        apiKey="apiKey"
        options={{}}
        subject="Ellie"
        relationship="speaks"
        object="Language"
      />,
    );
    const error = screen.getByTestId("error");
    expect(error.textContent).toEqual("Error");
  });

  it("Displays question if query comes back with question type", async () => {
    base.start = jest.fn(() => ({ sessionID: "sessionID" }));
    base.query = jest.fn(() => ({
      type: base.RESPONSE_TYPE_QUESTION,
      data: {
        question: {
          concepts: [],
          prompt: "What is the what of what?",
        },
        extraQuestions: [],
      },
    }));
    render(
      <Rainbird
        kmID="kmID"
        baseURL="http://test-api.baseURL.ai"
        apiKey="apiKey"
        options={{}}
        subject="Ellie"
        relationship="speaks"
        object="Language"
      />,
    );
    await waitFor(() => {
      const prompt = screen.getByTestId("prompt");
      expect(prompt.textContent).toEqual("What is the what of what?");
    });
  });

  it("Displays result if query comes back with result type", async () => {
    base.start = jest.fn(() => ({ sessionID: "sessionID" }));
    base.query = jest.fn(() => ({
      type: base.RESPONSE_TYPE_RESULT,
      data: [
        {
          subject: "Ellie",
          relationship: "speaks",
          object: "French",
          objectMetadata: {},
          certainty: "100",
        },
      ],
    }));
    render(
      <Rainbird
        kmID="kmID"
        baseURL="http://test-api.baseURL.ai"
        apiKey="apiKey"
        options={{}}
        subject="Ellie"
        relationship="speaks"
        object="Language"
      />,
    );
    await waitFor(() => {
      const result = screen.getByTestId("result");
      expect(result.textContent).toEqual("Ellie speaks French - 100%");
    });
  });

  it("Displays error message when no results", async () => {
    base.start = jest.fn(() => ({ sessionID: "sessionID" }));
    base.query = jest.fn(() => ({ type: base.RESPONSE_TYPE_RESULT, data: [] }));

    render(
      <Rainbird
        apiKey="apiKey"
        baseURL="http://test-api.baseURL.ai"
        kmID="kmID"
        object="Language"
        options={{}}
        relationship="speaks"
        subject="Ellie"
      />,
    );

    const expectedText =
      "Sorry I've been unable to find an answer to your question!";

    await waitFor(() => {
      const result = screen.getByTestId("error");
      expect(result.textContent).toEqual(expectedText);
    });
  });

  it("Displays multiple results if query comes back with more than one result", async () => {
    base.start = jest.fn(() => ({ sessionID: "sessionID" }));
    base.query = jest.fn(() => ({
      type: base.RESPONSE_TYPE_RESULT,
      data: [
        {
          subject: "Ellie",
          relationship: "speaks",
          object: "French",
          objectMetadata: {},
          certainty: "100",
        },
        {
          subject: "Bob",
          relationship: "speaks",
          object: "German",
          objectMetadata: {},
          certainty: "100",
        },
        {
          subject: "Someone",
          relationship: "speaks",
          object: "something",
          objectMetadata: {},
          certainty: "55",
        },
      ],
    }));
    render(
      <Rainbird
        kmID="kmID"
        baseURL="http://test-api.baseURL.ai"
        apiKey="apiKey"
        options={{}}
        subject="Ellie"
        relationship="speaks"
        object="Language"
      />,
    );
    await waitFor(() => {
      const result = screen.getAllByTestId("result");
      expect(result[0].textContent).toEqual("Ellie speaks French - 100%");
      expect(result[1].textContent).toEqual("Bob speaks German - 100%");
      expect(result[2].textContent).toEqual("Someone speaks something - 55%");
    });
  });

  it("Displays evidence tree link if requested", async () => {
    base.start = jest.fn(() => ({ sessionID: "sessionID" }));
    base.query = jest.fn(() => ({
      type: base.RESPONSE_TYPE_RESULT,
      data: [
        {
          subject: "Ellie",
          relationship: "speaks",
          object: "French",
          objectMetadata: {},
          certainty: "100",
        },
      ],
    }));
    render(
      <Rainbird
        kmID="kmID"
        baseURL="http://test-api.rainbird.ai"
        apiKey="apiKey"
        options={{}}
        subject="Ellie"
        relationship="speaks"
        object="Language"
        appURL="http://test.rainbird.ai"
        displayEvidence
      />,
    );
    await waitFor(() => {
      const evidence = screen.getByRole("form");
      expect(evidence.action).toBe(
        "http://test.rainbird.ai/evidence?id=undefined&api=http://test-api.rainbird.ai&sid=sessionID",
      );
    });
  });

  it("Hides evidence tree link if requested", async () => {
    base.start = jest.fn(() => ({ sessionID: "sessionID" }));
    base.query = jest.fn(() => ({
      type: base.RESPONSE_TYPE_RESULT,
      data: [
        {
          subject: "Ellie",
          relationship: "speaks",
          object: "French",
          objectMetadata: {},
          certainty: "100",
        },
      ],
    }));
    render(
      <Rainbird
        kmID="kmID"
        baseURL="http://test-api.rainbird.ai"
        apiKey="apiKey"
        options={{}}
        subject="Ellie"
        relationship="speaks"
        object="Language"
        appURL="http://test.rainbird.ai"
      />,
    );
    await waitFor(() => {
      expect(screen.queryByTestId("evidence")).toBeNull();
    });
  });

  it("Cant display evidence tree link if no appUrl prop present", async () => {
    base.start = jest.fn(() => ({ sessionID: "sessionID" }));
    base.query = jest.fn(() => ({
      type: base.RESPONSE_TYPE_RESULT,
      data: [
        {
          subject: "Ellie",
          relationship: "speaks",
          object: "French",
          objectMetadata: {},
          certainty: "100",
        },
      ],
    }));
    render(
      <Rainbird
        kmID="kmID"
        baseURL="http://test-api.rainbird.ai"
        apiKey="apiKey"
        options={{}}
        subject="Ellie"
        relationship="speaks"
        object="Language"
        displayEvidence
      />,
    );
    await waitFor(() => {
      expect(screen.queryByTestId("evidence")).toBeNull();
    });
  });

  it("Displays a reset button if a reset fn is provided", async () => {
    base.start = jest.fn(() => ({ sessionID: "sessionID" }));
    base.query = jest.fn(() => ({
      type: base.RESPONSE_TYPE_RESULT,
      data: [
        {
          subject: "Ellie",
          relationship: "speaks",
          object: "French",
          objectMetadata: {},
          certainty: "100",
        },
      ],
    }));
    render(
      <Rainbird
        kmID="kmID"
        baseURL="http://test-api.rainbird.ai"
        apiKey="apiKey"
        options={{}}
        subject="Ellie"
        relationship="speaks"
        object="Language"
        displayEvidence
        reset={() => {}}
      />,
    );
    await waitFor(() => {
      const reset = screen.getByTestId("reset-response");
      expect(reset.title).toEqual("Reset back to configuration");
    });
  });

  it("Doesn't display a reset button if not reset fn is provided", async () => {
    base.start = jest.fn(() => ({ sessionID: "sessionID" }));
    base.query = jest.fn(() => ({
      type: base.RESPONSE_TYPE_RESULT,
      data: [
        {
          subject: "Ellie",
          relationship: "speaks",
          object: "French",
          objectMetadata: {},
          certainty: "100",
        },
      ],
    }));
    render(
      <Rainbird
        kmID="kmID"
        baseURL="http://test-api.rainbird.ai"
        apiKey="apiKey"
        options={{}}
        subject="Ellie"
        relationship="speaks"
        object="Language"
        displayEvidence
      />,
    );
    await waitFor(() => {
      expect(screen.queryByTestId("reset-response")).toBeNull();
    });
  });
});
