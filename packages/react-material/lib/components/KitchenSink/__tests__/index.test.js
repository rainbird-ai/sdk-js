import React from "react";
import { fireEvent, render, screen, waitFor } from "@testing-library/react";
// eslint-disable-next-line import/no-extraneous-dependencies
import * as base from "@rainbird/sdk";

import { KitchenSink } from "..";

jest.mock("@rainbird/sdk");

describe("Kitchen Sink", () => {
  afterEach(() => jest.clearAllMocks());

  it("Displays the heading for the query", () => {
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject="Ellie"
        relationship="speaks"
        object="French"
      />,
    );
    const query = screen.getByTestId("query-name");
    expect(query.textContent).toEqual("Query: Ellie speaks French");
  });

  it("Displays config to fill in subject and/or object proceeding to KitchenSink", async () => {
    base.start = jest.fn(() => ({ data: {} }));
    base.query = jest.fn(() => {
      return {
        data: {
          question: {
            concepts: [],
            prompt: "What country does Christiaan live in?",
          },
          extraQuestions: [],
        },
        type: base.RESPONSE_TYPE_QUESTION,
      };
    });
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject=""
        relationship="speaks"
        object=""
      />,
    );
    let submit;
    let subjectInput;
    await waitFor(() => {
      subjectInput = screen
        .getByTestId("subject-pre-query")
        .querySelector("input");
      submit = screen.getByTestId("submit-config");
    });
    fireEvent.change(subjectInput, { target: { value: "Christiaan" } });
    fireEvent.click(submit);
    const query = screen.getByTestId("query-name");
    expect(query.textContent).toEqual("Query: Christiaan speaks ?");
  });

  it("Displays question marks in the query heading when subject or object is absent", () => {
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject=""
        relationship="speaks"
        object=""
      />,
    );
    const query = screen.getByTestId("query-name");
    expect(query.textContent).toEqual("Query: ? speaks ?");
  });

  it("Displays the loading spinner when waiting for a response from start", () => {
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject="Ellie"
        relationship="speaks"
        object="French"
      />,
    );
    screen.getByTestId("loading");
  });

  it("Displays the loading spinner when waiting for a response from query", () => {
    base.start = jest.fn(() => {
      return {};
    });
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject="Ellie"
        relationship="speaks"
        object="French"
      />,
    );
    screen.getByTestId("loading");
  });

  it("Displays error when an error is thrown", () => {
    base.start = jest.fn(() => {
      throw new Error("An error occurred!");
    });
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject="Ellie"
        relationship="speaks"
        object="French"
      />,
    );
    const error = screen.getByTestId("error-title");
    expect(error.textContent).toEqual("An error occurred!");
  });

  it("Displays the question name when returned from query", async () => {
    base.start = jest.fn(() => ({ data: {} }));
    base.query = jest.fn(() => {
      return {
        data: {
          question: {
            concepts: [],
            prompt: "What country does Ellie live in?",
          },
          extraQuestions: [],
        },
        type: base.RESPONSE_TYPE_QUESTION,
      };
    });
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject="Ellie"
        relationship="speaks"
        object="French"
      />,
    );
    await waitFor(() => {
      const prompt = screen.getByTestId("prompt");
      expect(prompt.textContent).toEqual("What country does Ellie live in?");
    });
  });

  it("Displays an error when an error is returned from query", async () => {
    base.start = jest.fn(() => ({ data: {} }));
    base.start = jest.fn(() => {
      throw new Error("An error occurred!");
    });
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject="Ellie"
        relationship="speaks"
        object="French"
      />,
    );
    await waitFor(() => {
      const error = screen.getByTestId("error-title");
      expect(error.textContent).toEqual("An error occurred!");
    });
  });

  it("Displays the back button and a disabled submit button on mount", async () => {
    base.start = jest.fn(() => ({ data: {} }));
    base.query = jest.fn(() => {
      return {
        data: {
          question: {
            concepts: [],
            prompt: "What country does Ellie live in?",
          },
          extraQuestions: [],
        },
        type: base.RESPONSE_TYPE_QUESTION,
      };
    });
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject="Ellie"
        relationship="speaks"
        object="French"
      />,
    );
    await waitFor(() => {
      const back = screen.getByTestId("undo-response");
      expect(back.textContent).toEqual("Back to previous question");
      const submit = screen.getByTestId("submit-response");
      expect(submit.disabled).toEqual(true);
      expect(submit.textContent).toEqual("Submit response");
    });
  });

  it("Displays the skip button when a question is skippable", async () => {
    base.start = jest.fn(() => ({ data: {} }));
    base.query = jest.fn(() => {
      return {
        data: {
          question: {
            concepts: [],
            prompt: "What country does Ellie live in?",
            allowUnknown: true,
          },
          extraQuestions: [],
        },
        type: base.RESPONSE_TYPE_QUESTION,
      };
    });
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject="Ellie"
        relationship="speaks"
        object="French"
      />,
    );
    await waitFor(() => {
      const skip = screen.getByTestId("skip-question");
      expect(skip.textContent).toEqual("Skip");
    });
  });

  it("Clicking undo calls the undo endpoint", async () => {
    base.start = jest.fn(() => ({ data: {} }));
    base.query = jest.fn(() => {
      return {
        data: {
          question: {
            concepts: [],
            prompt: "What country does Ellie live in?",
          },
          extraQuestions: [],
        },
        type: base.RESPONSE_TYPE_QUESTION,
      };
    });
    base.undo = jest.fn();
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject="Ellie"
        relationship="speaks"
        object="French"
      />,
    );
    let back;
    await waitFor(() => {
      back = screen.getByTestId("undo-response");
    });
    fireEvent.click(back);
    expect(base.undo).toHaveBeenCalled();
  });

  it("Clicking submit calls the response endpoint with the correct data", async () => {
    base.start = jest.fn(() => ({ data: {} }));
    base.query = jest.fn(() => {
      return {
        data: {
          question: {
            concepts: [],
            prompt: "What country does Ellie live in?",
            object: "French",
            relationship: "speaks",
            subject: "Ellie",
          },
          extraQuestions: [],
        },
        type: base.RESPONSE_TYPE_QUESTION,
      };
    });
    base.response = jest.fn();
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject="Ellie"
        relationship="speaks"
        object="French"
      />,
    );
    let respond;
    await waitFor(() => {
      respond = screen.getByTestId("submit-response");
    });
    fireEvent.submit(respond);
    expect(base.response).toHaveBeenCalledWith(
      "https://api.rainbird.ai",
      undefined,
      [{ cf: 100, object: "French", relationship: "speaks", subject: "Ellie" }],
      undefined,
    );
  });

  it("Clicking skip calls the response endpoint with unanswered: true", async () => {
    base.start = jest.fn(() => ({ data: {} }));
    base.query = jest.fn(() => {
      return {
        data: {
          question: {
            concepts: [],
            prompt: "What country does Ellie live in?",
            allowUnknown: true,
            object: "French",
            relationship: "speaks",
            subject: "Ellie",
          },
          extraQuestions: [],
        },
        type: base.RESPONSE_TYPE_QUESTION,
      };
    });
    base.response = jest.fn(() => {});
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject="Ellie"
        relationship="speaks"
        object="French"
      />,
    );
    let skip;
    await waitFor(() => {
      skip = screen.getByTestId("skip-question");
    });
    fireEvent.click(skip);
    expect(base.response).toHaveBeenCalledWith(
      "https://api.rainbird.ai",
      undefined,
      [
        {
          cf: 100,
          object: "French",
          relationship: "speaks",
          subject: "Ellie",
          unanswered: true,
        },
      ],
      undefined,
    );
  });

  it("Clicking skip with no subject calls the response endpoint with unanswered: true", async () => {
    base.start = jest.fn(() => ({ data: {} }));
    base.query = jest.fn(() => {
      return {
        data: {
          question: {
            concepts: [],
            prompt: "What country does Ellie live in?",
            allowUnknown: true,
            object: "French",
            relationship: "speaks",
          },
          extraQuestions: [],
        },
        type: base.RESPONSE_TYPE_QUESTION,
      };
    });
    base.response = jest.fn(() => {});
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        relationship="speaks"
        object="French"
      />,
    );
    let skip;
    await waitFor(() => {
      skip = screen.getByTestId("skip-question");
    });
    fireEvent.click(skip);
    expect(base.response).toHaveBeenCalledWith(
      "https://api.rainbird.ai",
      undefined,
      [
        {
          cf: 100,
          object: "French",
          relationship: "speaks",
          subject: "",
          unanswered: true,
        },
      ],
      undefined,
    );
  });

  it("Clicking skip with no object calls the response endpoint with unanswered: true", async () => {
    base.start = jest.fn(() => ({ data: {} }));
    base.query = jest.fn(() => {
      return {
        data: {
          question: {
            concepts: [],
            prompt: "What country does Ellie live in?",
            allowUnknown: true,
            relationship: "speaks",
            subject: "Ellie",
          },
          extraQuestions: [],
        },
        type: base.RESPONSE_TYPE_QUESTION,
      };
    });
    base.response = jest.fn(() => {});
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        relationship="speaks"
        subject="Ellie"
      />,
    );
    let skip;
    await waitFor(() => {
      skip = screen.getByTestId("skip-question");
    });
    fireEvent.click(skip);
    expect(base.response).toHaveBeenCalledWith(
      "https://api.rainbird.ai",
      undefined,
      [
        {
          cf: 100,
          subject: "Ellie",
          relationship: "speaks",
          object: "",
          unanswered: true,
        },
      ],
      undefined,
    );
  });

  it("Clicking skip with extra questions calls the response endpoint with unanswered: true", async () => {
    base.start = jest.fn(() => ({ data: {} }));
    base.query = jest.fn(() => {
      return {
        data: {
          question: {
            concepts: [],
            prompt: "What country does Ellie live in?",
            allowUnknown: true,
            object: "French",
            relationship: "speaks",
            subject: "Ellie",
          },
          extraQuestions: [
            {
              prompt: "What country does Ellie live in?",
              allowUnknown: true,
              object: "English",
              relationship: "speaks",
              concepts: [],
              subject: "Ellie",
            },
          ],
        },
        type: base.RESPONSE_TYPE_QUESTION,
      };
    });
    base.response = jest.fn(() => {});
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject="Ellie"
        relationship="speaks"
        object="French"
      />,
    );
    let skip;
    await waitFor(() => {
      skip = screen.getByTestId("skip-question");
    });
    fireEvent.click(skip);
    expect(base.response).toHaveBeenCalledWith(
      "https://api.rainbird.ai",
      undefined,
      [
        {
          cf: 100,
          object: "French",
          relationship: "speaks",
          subject: "Ellie",
          unanswered: true,
        },
        [
          {
            cf: 100,
            object: "English",
            relationship: "speaks",
            subject: "Ellie",
            unanswered: true,
          },
        ],
      ],
      undefined,
    );
  });

  it("Should not have subject or object, proceed with query and reset appropriately", async () => {
    base.start = jest.fn(() => ({ data: {} }));
    base.query = jest.fn(() => ({
      data: {
        question: {
          prompt: "What country does Christiaan live in?",
          concepts: [],
        },
        extraQuestions: [],
      },
      type: base.RESPONSE_TYPE_QUESTION,
    }));
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject=""
        relationship="speaks"
        object=""
      />,
    );
    let submit;
    let subjectInput;
    let resetButton;
    await waitFor(() => {
      subjectInput = screen
        .getByTestId("subject-pre-query")
        .querySelector("input");
      submit = screen.getByTestId("submit-config");
    });
    fireEvent.change(subjectInput, { target: { value: "Christiaan" } });
    fireEvent.click(submit);
    const query = screen.getByTestId("query-name");
    expect(query.textContent).toEqual("Query: Christiaan speaks ?");
    await waitFor(() => {
      resetButton = screen.getByTestId("interaction-reset-response");
    });
    fireEvent.click(resetButton);
    expect(query.textContent).toEqual("Query: ? speaks ?");
  });

  describe("allowCustomSession", () => {
    // Selectors
    const sessionID = "session id (optional)-pre-query";
    const subject = "subject-pre-query";
    const submit = "submit-config";

    const defaultProps = {
      allowCustomSession: true,
      apiKey: "apiKey",
      baseURL: "https://api.rainbird.ai",
      kmID: "12345",
      object: "",
      relationship: "speaks",
      subject: "",
    };

    const {
      getByLabelText,
      getByTestId: getByTestID,
      queryByTestId: queryByTestID,
    } = screen;
    const { change, click } = fireEvent;

    it("Session id control is hidden in the configuration by default", () => {
      render(<KitchenSink {...defaultProps} allowCustomSession={false} />);

      expect(queryByTestID(sessionID)).toBeNull();
    });

    it("Show the session id control in the configuration when allowCustomSession is true", () => {
      render(<KitchenSink {...defaultProps} />);

      expect(getByTestID(sessionID)).toBeTruthy();
      expect(getByLabelText("Session ID (Optional)")).toBeTruthy();
    });

    it("Starting a query with a custom session does not call start", async () => {
      base.start = jest.fn();
      base.query = jest.fn(() => ({
        data: {
          extraQuestions: [],
          question: { prompt: "Foo Bar Baz", concepts: [] },
        },
        type: base.RESPONSE_TYPE_QUESTION,
      }));

      let sessionIdControl;
      let subjectControl;
      let submitControl;

      render(<KitchenSink {...defaultProps} />);

      // Bind rendered controls.
      await waitFor(() => {
        sessionIdControl = getByTestID(sessionID).querySelector("input");
        subjectControl = getByTestID(subject).querySelector("input");
        submitControl = getByTestID(submit);
      });

      // Update state with valid configuration with session id.
      change(subjectControl, { target: { value: "Foo" } });
      change(sessionIdControl, { target: { value: "123" } });
      click(submitControl);

      expect(base.start).not.toHaveBeenCalled();
      expect(base.query).toHaveBeenCalledWith(
        "https://api.rainbird.ai",
        "123",
        "Foo",
        "speaks",
        "",
        undefined,
      );
    });
  });
});
