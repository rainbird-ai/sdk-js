import React, { useState } from "react";
import {
  AppBar,
  Box,
  Grid,
  Paper,
  ThemeProvider,
  Typography,
} from "@material-ui/core";

import { Configuration } from "../Configuration";
import { ErrorPage } from "../Error";
import { Rainbird } from "../Rainbird";
import { RainbirdLogo } from "./RainbirdLogo";
import { RBTheme } from "../../styles";

const handleReport = (err, errInfo) => {
  window.open(
    `mailto:support@rainbird-example.com?subject=${"Rainbird Kitchen Sink Error"}&body=${
      err.message
    }\n\n${errInfo.componentStack}`,
  );
};

const handleError = (err, errInfo, handleReset) => (
  <ErrorPage
    error={err}
    errorInfo={errInfo}
    onReset={() => handleReset()}
    onReport={() => handleReport(err, errInfo)}
  />
);

export const KitchenSink = ({
  allowCustomSession,
  allowUndoResults,
  apiKey,
  appURL,
  baseURL,
  displayEvidence,
  evidenceKey,
  kmID,
  object: origObject,
  options,
  relationship,
  subject: origSubject,
  theme = RBTheme,
}) => {
  const [object, setObject] = useState(origObject);
  const [resetKey, setResetKey] = useState();
  const [sessionID, setSessionID] = useState();
  const [subject, setSubject] = useState(origSubject);

  // If subject or object is the same with original state
  // we need to have a random key for a reset
  const handleReset = () => {
    setResetKey(Date.now());
    setObject(origObject);
    setSessionID(sessionID);
    setSubject(origSubject);
  };

  return (
    <ThemeProvider theme={theme}>
      <Grid container direction="column" alignItems="center">
        <Banner />
        <Grid
          container
          direction="column"
          justify="center"
          alignItems="center"
          style={{ height: "80vh", minHeight: "500px" }}
        >
          <Box p={3}>
            <Paper>
              <Box width="80vw">
                <Header
                  object={object}
                  relationship={relationship}
                  subject={subject}
                />
                <Agent
                  allowCustomSession={allowCustomSession}
                  allowUndoResults={allowUndoResults}
                  apiKey={apiKey}
                  appURL={appURL}
                  baseURL={baseURL}
                  displayEvidence={displayEvidence ?? true}
                  evidenceKey={evidenceKey}
                  key={resetKey}
                  kmID={kmID}
                  object={object}
                  onError={(err, errInfo) => {
                    return handleError(err, errInfo, handleReset);
                  }}
                  options={options}
                  relationship={relationship}
                  reset={() => handleReset()}
                  sessionID={sessionID}
                  setObject={setObject}
                  setSessionID={setSessionID}
                  setSubject={setSubject}
                  subject={subject}
                  theme={theme}
                />
              </Box>
            </Paper>
          </Box>
        </Grid>
      </Grid>
    </ThemeProvider>
  );
};

const Banner = () => (
  <Grid
    alignItems="center"
    aria-label="Banner"
    container
    direction="row"
    justify="flex-end"
  >
    <RainbirdLogo />
  </Grid>
);

const Header = ({ subject, relationship, object }) => (
  <AppBar position="relative" color="primary">
    <Box p={1.5}>
      <Typography
        aria-level="1"
        component="h1"
        data-testid="query-name"
        role="heading"
        variant="body1"
      >
        Query:
        {` ${subject || "?"} ${relationship} ${object || "?"}`}
      </Typography>
    </Box>
  </AppBar>
);

const Agent = ({
  allowCustomSession,
  allowUndoResults,
  apiKey,
  appURL,
  baseURL,
  displayEvidence,
  evidenceKey,
  kmID,
  object,
  onError,
  options,
  relationship,
  reset,
  resetKey,
  sessionID,
  setObject,
  setSessionID,
  setSubject,
  subject,
  theme,
}) => (
  <Box
    role="main"
    p={4}
    style={{
      overflow: "auto",
      height: "55vh",
      minHeight: "400px",
      maxHeight: "800px",
    }}
  >
    {!subject && !object && (
      <Configuration
        allowCustomSession={allowCustomSession}
        setObject={setObject}
        setSubject={setSubject}
        setSessionID={setSessionID}
      />
    )}
    {(subject || object) && (
      <Rainbird
        allowCustomSession={allowCustomSession}
        allowUndoResults={allowUndoResults}
        apiKey={apiKey}
        appURL={appURL}
        baseURL={baseURL}
        displayEvidence={displayEvidence ?? true}
        evidenceKey={evidenceKey}
        key={resetKey}
        kmID={kmID}
        object={object}
        onError={onError}
        options={options}
        relationship={relationship}
        reset={reset}
        sessionID={sessionID}
        subject={subject}
        theme={theme}
      />
    )}
  </Box>
);
