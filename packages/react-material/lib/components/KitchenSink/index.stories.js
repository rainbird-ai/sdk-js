import React from "react";
import { KitchenSink } from ".";

// eslint-disable-next-line import/no-default-export
export default {
  title: "Kitchen Sink",
  component: KitchenSink,
  argTypes: {
    allowCustomSession: { type: "boolean" },
    allowUndoResults: { type: "boolean" },
    apiKey: { type: "text" },
    appURL: { type: "text" },
    baseURL: { type: "text" },
    displayEvidence: { type: "boolean" },
    evidenceKey: { type: "text" },
    kmID: { type: "text" },
    object: { type: "text" },
    relationship: { type: "text" },
    subject: { type: "text" },
  },
};

const Template = (args) => <KitchenSink {...args} />;

export const Default = Template.bind({});
export const SessionIdEnabled = Template.bind({});
export const UndoEnabled = Template.bind({});
export const EvidenceEnabled = Template.bind({});

Default.args = {
  allowCustomSession: false,
  allowUndoResults: false,
  apiKey: "",
  appURL: "",
  baseURL: "",
  displayEvidence: false,
  evidenceKey: "",
  kmID: "",
  object: "",
  relationship: "",
  subject: "",
};
SessionIdEnabled.args = { ...Default.args, allowCustomSession: true };
UndoEnabled.args = { ...Default.args, allowUndoResults: true };
EvidenceEnabled.args = { ...Default.args, displayEvidence: true };
