import React from "react";
import { screen, render, fireEvent } from "@testing-library/react";
// eslint-disable-next-line import/no-extraneous-dependencies
import * as base from "@rainbird/sdk";
import { Interaction } from "..";

jest.mock("@rainbird/sdk");

describe("Interaction", () => {
  it("Displays an error if an error is present", () => {
    render(<Interaction data={{}} error={["There was an error"]} />);
    const error = screen.getByTestId("error");
    expect(error.textContent).toEqual("There was an error");
  });

  it("Displays the single string add component when the data matches", () => {
    render(
      <Interaction
        data={{
          question: {
            dataType: "string",
            canAdd: true,
            plural: false,
            concepts: [{}],
          },
          extraQuestions: [],
        }}
      />,
    );
    const input = screen.queryByTestId("single-string-add");
    expect(input).toBeTruthy();
  });

  it("Displays the single string add component with no instances when the data matches", () => {
    render(
      <Interaction
        data={{
          question: {
            dataType: "string",
            canAdd: true,
            plural: false,
            concepts: [],
          },
          extraQuestions: [],
        }}
      />,
    );
    const input = screen.queryByTestId("single-string-add-noInsts");
    expect(input).toBeTruthy();
  });

  it("Typing in the single string add component and submitting calls respond with the correct data", () => {
    base.response = jest.fn();
    render(
      <Interaction
        data={{
          question: {
            type: "Second Form Object",
            concepts: [{ name: "France", value: "France" }],
            dataType: "string",
            canAdd: true,
            plural: false,
            subject: "Ellie",
            relationship: "speaks",
          },
          extraQuestions: [],
        }}
      />,
    );

    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "fr" },
    });
    fireEvent.click(screen.getByText("France"));
    const submit = screen.getByTestId("submit-response");
    fireEvent.submit(submit);
    // empty strings and undefined are the sessionID and other config bits that come from the RBInteraction
    expect(base.response).toHaveBeenCalledWith(
      "",
      "",
      [{ cf: 100, object: "France", relationship: "speaks", subject: "Ellie" }],
      undefined,
    );
  });

  it("Options with the invalidResponse property are not shown", () => {
    base.response = jest.fn();
    render(
      <Interaction
        data={{
          extraQuestions: [],
          question: {
            allowCF: true,
            allowUnknown: false,
            canAdd: true,
            concepts: [
              {
                conceptType: "Person",
                invalidResponse: true,
                name: "Fred",
                type: "string",
                value: "Fred",
              },
            ],
            dataType: "string",
            knownAnswers: [],
            object: "France",
            plural: false,
            prompt: "Who has a born in of France?",
            relationship: "born in",
            type: "Second Form Subject",
          },
        }}
      />,
    );

    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "fr" },
    });

    const fred = screen.queryByText("Fred");
    expect(fred).toBeFalsy();
  });

  it("Clearing a control with a simple handler applies the correct data", () => {
    base.response = jest.fn();
    render(
      <Interaction
        data={{
          question: {
            type: "Second Form Object",
            concepts: [{ name: "France", value: "France" }],
            dataType: "string",
            canAdd: true,
            plural: false,
            subject: "Ellie",
            relationship: "speaks",
          },
          extraQuestions: [],
        }}
      />,
    );

    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "fr" },
    });
    fireEvent.click(screen.getByText("France"));
    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "" },
    });
    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "fr" },
    });
    fireEvent.click(screen.getByText("France"));
    const submit = screen.getByTestId("submit-response");
    fireEvent.submit(submit);
    // empty strings and undefined are the sessionID and other config bits that come from the RBInteraction
    expect(base.response).toHaveBeenCalledWith(
      "",
      "",
      [{ cf: 100, object: "France", relationship: "speaks", subject: "Ellie" }],
      undefined,
    );
  });

  it("Typing a custom entry into the single string add component and submitting calls respond with the correct data", () => {
    base.response = jest.fn();
    render(
      <Interaction
        data={{
          question: {
            type: "Second Form Object",
            concepts: [{ name: "France", value: "France" }],
            dataType: "string",
            canAdd: true,
            plural: false,
            subject: "Ellie",
            relationship: "speaks",
          },
          extraQuestions: [],
        }}
      />,
    );

    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "england" },
    });
    fireEvent.click(screen.getByText('Add "england"'));
    const submit = screen.getByTestId("submit-response");
    fireEvent.submit(submit);
    // empty strings and undefined are the sessionID and other config bits that come from the RBInteraction
    expect(base.response).toHaveBeenCalledWith(
      "",
      "",
      [
        {
          cf: 100,
          object: "england",
          relationship: "speaks",
          subject: "Ellie",
        },
      ],
      undefined,
    );
  });

  it("Displays the single string component when the data matches", () => {
    render(
      <Interaction
        data={{
          question: {
            dataType: "string",
            concepts: [],
            canAdd: false,
            plural: false,
          },
          extraQuestions: [],
        }}
      />,
    );
    const input = screen.queryByTestId("single-string");
    expect(input).toBeTruthy();
  });

  it("Selecting an entry from the single string component and submitting calls respond with the correct data", () => {
    base.response = jest.fn();
    render(
      <Interaction
        data={{
          question: {
            type: "Second Form Object",
            concepts: [{ name: "France", value: "France" }],
            dataType: "string",
            canAdd: false,
            plural: false,
            subject: "Ellie",
            relationship: "speaks",
          },
          extraQuestions: [],
        }}
      />,
    );

    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "fr" },
    });
    fireEvent.click(screen.getByText("France"));
    const submit = screen.getByTestId("submit-response");
    fireEvent.submit(submit);
    // empty strings and undefined are the sessionID and other config bits that come from the RBInteraction
    expect(base.response).toHaveBeenCalledWith(
      "",
      "",
      [{ cf: 100, object: "France", relationship: "speaks", subject: "Ellie" }],
      undefined,
    );
  });

  it("Custom entries cant be added to the single string component", () => {
    base.response = jest.fn();
    render(
      <Interaction
        data={{
          question: {
            type: "Second Form Object",
            concepts: [{ name: "France", value: "France" }],
            dataType: "string",
            canAdd: false,
            plural: false,
            subject: "Ellie",
            relationship: "speaks",
          },
          extraQuestions: [],
        }}
      />,
    );

    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "england" },
    });
    const england = screen.queryByText('Add "england"');
    expect(england).toBeFalsy();
  });

  it("Displays the multi string add component when the data matches", () => {
    render(
      <Interaction
        data={{
          question: {
            dataType: "string",
            concepts: [],
            canAdd: true,
            plural: true,
          },
          extraQuestions: [],
        }}
      />,
    );
    const input = screen.queryByTestId("multi-string-add");
    expect(input).toBeTruthy();
  });

  it("Multiple items can be selected and added and the respond endpoint is called with the correct data", () => {
    base.response = jest.fn();
    render(
      <Interaction
        data={{
          question: {
            type: "Second Form Object",
            concepts: [
              { name: "France", value: "France" },
              { name: "Spain", value: "Spain" },
              { name: "Zimbabwe", value: "Zimbabwe" },
            ],
            dataType: "string",
            canAdd: true,
            plural: true,
            subject: "Ellie",
            relationship: "speaks",
          },
          extraQuestions: [],
        }}
      />,
    );

    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "fr" },
    });
    fireEvent.click(screen.getByText("France"));
    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "sp" },
    });
    fireEvent.click(screen.getByText("Spain"));
    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "zi" },
    });
    fireEvent.click(screen.getByText("Zimbabwe"));
    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "Germany" },
    });
    fireEvent.click(screen.getByText('Add "Germany"'));
    const submit = screen.getByTestId("submit-response");
    fireEvent.submit(submit);
    // empty strings and undefined are the sessionID and other config bits that come from the RBInteraction
    expect(base.response).toHaveBeenCalledWith(
      "",
      "",
      [
        { cf: 100, object: "France", relationship: "speaks", subject: "Ellie" },
        { cf: 100, object: "Spain", relationship: "speaks", subject: "Ellie" },
        {
          cf: 100,
          object: "Zimbabwe",
          relationship: "speaks",
          subject: "Ellie",
        },
        {
          cf: 100,
          object: "Germany",
          relationship: "speaks",
          subject: "Ellie",
        },
      ],
      undefined,
    );
  });

  it("Multiple dates can be entered and the correct data is sent to the response endpoint on submit", () => {
    base.response = jest.fn();
    render(
      <Interaction
        data={{
          question: {
            type: "Second Form Object",
            dataType: "date",
            subject: "Ellie",
            concepts: [],
            relationship: "speaks",
            canAdd: false,
            plural: true,
          },
          extraQuestions: [],
        }}
      />,
    );
    fireEvent.click(screen.getByTitle("Open"));
    fireEvent.click(screen.getByText("OK"));
    const chipText = screen.getByTestId("md-chip").firstChild.textContent;
    expect(chipText).toEqual(new Date().toLocaleDateString());
    fireEvent.click(screen.getByTestId("submit-response"));
    expect(base.response).toHaveBeenCalled();
  });

  it("handleMultiSelect can provide correct data when no subject exists and called respond endpoint with the correct data", () => {
    base.response = jest.fn();
    render(
      <Interaction
        data={{
          question: {
            type: "Second Form Object",
            concepts: [
              { name: "France", value: "France" },
              { name: "Spain", value: "Spain" },
              { name: "Zimbabwe", value: "Zimbabwe" },
            ],
            dataType: "string",
            canAdd: true,
            plural: true,
            object: "France",
            relationship: "speaks",
          },
          extraQuestions: [],
        }}
      />,
    );

    const submit = screen.getByTestId("submit-response");
    fireEvent.submit(submit);
    // empty strings and undefined are the sessionID and other config bits that come from the RBInteraction
    expect(base.response).toHaveBeenCalledWith(
      "",
      "",
      [
        {
          cf: 100,
          object: "France",
          relationship: "speaks",
          subject: undefined,
          unanswered: true,
        },
      ],
      undefined,
    );
  });

  it("Reselecting an item from the dropdown removes the entry", () => {
    base.response = jest.fn();
    render(
      <Interaction
        data={{
          question: {
            type: "Second Form Object",
            concepts: [
              { name: "France", value: "France" },
              { name: "Spain", value: "Spain" },
              { name: "Zimbabwe", value: "Zimbabwe" },
            ],
            dataType: "string",
            canAdd: true,
            plural: true,
            subject: "Ellie",
            relationship: "speaks",
          },
          extraQuestions: [],
        }}
      />,
    );

    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "fr" },
    });
    fireEvent.click(screen.getByText("France"));
    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "sp" },
    });
    fireEvent.click(screen.getByText("Spain"));
    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "sp" },
    });
    const spainItems = screen.getAllByText("Spain");
    const spainOption = spainItems[spainItems.length - 1];
    fireEvent.click(spainOption);
    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "zi" },
    });
    fireEvent.click(screen.getByText("Zimbabwe"));
    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "Germany" },
    });
    fireEvent.click(screen.getByText('Add "Germany"'));
    const submit = screen.getByTestId("submit-response");
    fireEvent.submit(submit);
    // empty strings and undefined are the sessionID and other config bits that come from the RBInteraction
    expect(base.response).toHaveBeenCalledWith(
      "",
      "",
      [
        { cf: 100, object: "France", relationship: "speaks", subject: "Ellie" },
        {
          cf: 100,
          object: "Zimbabwe",
          relationship: "speaks",
          subject: "Ellie",
        },
        {
          cf: 100,
          object: "Germany",
          relationship: "speaks",
          subject: "Ellie",
        },
      ],
      undefined,
    );
  });

  it("Deleting an option removes it from the response", () => {
    base.response = jest.fn();
    const { container } = render(
      <Interaction
        data={{
          question: {
            type: "Second Form Object",
            concepts: [
              { name: "France", value: "France" },
              { name: "Spain", value: "Spain" },
              { name: "Zimbabwe", value: "Zimbabwe" },
            ],
            dataType: "string",
            canAdd: true,
            plural: true,
            subject: "Ellie",
            relationship: "speaks",
          },
          extraQuestions: [],
        }}
      />,
    );

    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "fr" },
    });
    fireEvent.click(screen.getByText("France"));
    fireEvent.click(container.querySelector("svg"));
    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "sp" },
    });
    fireEvent.click(screen.getByText("Spain"));
    const submit = screen.getByTestId("submit-response");
    fireEvent.submit(submit);
    // empty strings and undefined are the sessionID and other config bits that come from the RBInteraction
    expect(base.response).toHaveBeenCalledWith(
      "",
      "",
      [{ cf: 100, object: "Spain", relationship: "speaks", subject: "Ellie" }],
      undefined,
    );
  });

  it("Displays the multi string component when the data matches", () => {
    render(
      <Interaction
        data={{
          question: {
            dataType: "string",
            concepts: [],
            canAdd: false,
            plural: true,
          },
          extraQuestions: [],
        }}
      />,
    );
    const input = screen.queryByTestId("multi-string");
    expect(input).toBeTruthy();
  });

  it("Multiple items can be selected and the respond endpoint is called with the correct data", () => {
    base.response = jest.fn();
    render(
      <Interaction
        data={{
          question: {
            type: "Second Form Object",
            concepts: [
              { name: "France", value: "France" },
              { name: "Spain", value: "Spain" },
              { name: "Zimbabwe", value: "Zimbabwe" },
            ],
            dataType: "string",
            canAdd: false,
            plural: true,
            subject: "Ellie",
            relationship: "speaks",
          },
          extraQuestions: [],
        }}
      />,
    );

    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "fr" },
    });
    fireEvent.click(screen.getByText("France"));
    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "sp" },
    });
    fireEvent.click(screen.getByText("Spain"));
    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "zi" },
    });
    fireEvent.click(screen.getByText("Zimbabwe"));
    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "Germany" },
    });
    const addOption = screen.queryByText('Add "Germany"');
    expect(addOption).toBeFalsy();
    const noOptions = screen.queryByText("No options");
    expect(noOptions).toBeTruthy();
    const submit = screen.getByTestId("submit-response");
    fireEvent.submit(submit);
    // empty strings and undefined are the sessionID and other config bits that come from the RBInteraction
    expect(base.response).toHaveBeenCalledWith(
      "",
      "",
      [
        { cf: 100, object: "France", relationship: "speaks", subject: "Ellie" },
        { cf: 100, object: "Spain", relationship: "speaks", subject: "Ellie" },
        {
          cf: 100,
          object: "Zimbabwe",
          relationship: "speaks",
          subject: "Ellie",
        },
      ],
      undefined,
    );
  });

  it("Reselecting an item from the dropdown removes the entry", () => {
    base.response = jest.fn();
    render(
      <Interaction
        data={{
          question: {
            type: "Second Form Object",
            concepts: [
              { name: "France", value: "France" },
              { name: "Spain", value: "Spain" },
              { name: "Zimbabwe", value: "Zimbabwe" },
            ],
            dataType: "string",
            canAdd: false,
            plural: true,
            subject: "Ellie",
            relationship: "speaks",
          },
          extraQuestions: [],
        }}
      />,
    );

    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "fr" },
    });
    fireEvent.click(screen.getByText("France"));
    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "sp" },
    });
    fireEvent.click(screen.getByText("Spain"));
    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "sp" },
    });
    const spainItems = screen.getAllByText("Spain");
    const spainOption = spainItems[spainItems.length - 1];
    fireEvent.click(spainOption);
    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "zi" },
    });
    fireEvent.click(screen.getByText("Zimbabwe"));
    const submit = screen.getByTestId("submit-response");
    fireEvent.submit(submit);
    // empty strings and undefined are the sessionID and other config bits that come from the RBInteraction
    expect(base.response).toHaveBeenCalledWith(
      "",
      "",
      [
        { cf: 100, object: "France", relationship: "speaks", subject: "Ellie" },
        {
          cf: 100,
          object: "Zimbabwe",
          relationship: "speaks",
          subject: "Ellie",
        },
      ],
      undefined,
    );
  });

  it("Deleting an option removes it from the response", () => {
    base.response = jest.fn();
    const { container } = render(
      <Interaction
        data={{
          question: {
            type: "Second Form Object",
            concepts: [
              { name: "France", value: "France" },
              { name: "Spain", value: "Spain" },
              { name: "Zimbabwe", value: "Zimbabwe" },
            ],
            dataType: "string",
            canAdd: false,
            plural: true,
            subject: "Ellie",
            relationship: "speaks",
          },
          extraQuestions: [],
        }}
      />,
    );

    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "fr" },
    });
    fireEvent.click(screen.getByText("France"));
    fireEvent.click(container.querySelector("svg"));
    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "sp" },
    });
    fireEvent.click(screen.getByText("Spain"));
    const submit = screen.getByTestId("submit-response");
    fireEvent.submit(submit);
    // empty strings and undefined are the sessionID and other config bits that come from the RBInteraction
    expect(base.response).toHaveBeenCalledWith(
      "",
      "",
      [{ cf: 100, object: "Spain", relationship: "speaks", subject: "Ellie" }],
      undefined,
    );
  });

  it("Displays the single date component when the data matches", () => {
    render(
      <Interaction
        data={{
          question: {
            dataType: "date",
            concepts: [],
            canAdd: false,
            plural: false,
          },
          extraQuestions: [],
        }}
      />,
    );
    const input = screen.queryByTestId("single-date");
    expect(input).toBeTruthy();
  });

  it("Displays the multi date component when the data matches", () => {
    render(
      <Interaction
        data={{
          question: {
            dataType: "date",
            concepts: [],
            canAdd: false,
            plural: true,
          },
          extraQuestions: [],
        }}
      />,
    );
    const input = screen.queryByTestId("multi-date");
    expect(input).toBeTruthy();
  });

  it("A date can be entered and the correct data is sent to the response endpoint on submit", () => {
    base.response = jest.fn();
    const { container } = render(
      <Interaction
        data={{
          question: {
            type: "Second Form Object",
            dataType: "date",
            subject: "Ellie",
            relationship: "speaks",
            canAdd: false,
            plural: false,
            concepts: [],
          },
          extraQuestions: [],
        }}
      />,
    );
    fireEvent.change(container.querySelector('[id="dateInput"]'), {
      target: { value: "01/12/1992" },
    });
    const submit = screen.getByTestId("submit-response");
    fireEvent.submit(submit);
    // empty strings and undefined are the sessionID and other config bits that come from the RBInteraction
    expect(base.response).toHaveBeenCalledWith(
      "",
      "",
      [
        {
          cf: 100,
          object: 723168000000,
          relationship: "speaks",
          subject: "Ellie",
        },
      ],
      undefined,
    );
  });

  it("Displays the single number component when the data matches", () => {
    render(
      <Interaction
        data={{
          question: {
            dataType: "number",
            concepts: [],
            canAdd: false,
            plural: false,
          },
          extraQuestions: [],
        }}
      />,
    );
    const input = screen.queryByTestId("single-number");
    expect(input).toBeTruthy();
  });

  it("A number can be entered and the correct data is sent to the response endpoint on submit", () => {
    base.response = jest.fn();
    render(
      <Interaction
        data={{
          question: {
            type: "Second Form Object",
            dataType: "number",
            subject: "Ellie",
            relationship: "speaks",
            canAdd: false,
            concepts: [],
            plural: false,
          },
          extraQuestions: [],
        }}
      />,
    );
    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: 122.482 },
    });
    const submit = screen.getByTestId("submit-response");
    fireEvent.submit(submit);
    // empty strings and undefined are the sessionID and other config bits that come from the RBInteraction
    expect(base.response).toHaveBeenCalledWith(
      "",
      "",
      [
        {
          cf: 100,
          object: "122.482",
          relationship: "speaks",
          subject: "Ellie",
        },
      ],
      undefined,
    );
  });

  it("Displays the single truth component when the data matches", () => {
    render(
      <Interaction
        data={{
          question: {
            dataType: "truth",
            concepts: [],
            canAdd: false,
            plural: false,
          },
          extraQuestions: [],
        }}
      />,
    );
    const input = screen.queryByTestId("single-truth");
    expect(input).toBeTruthy();
  });

  it("A true truth can be selected and the correct data is sent to the response endpoint on submit", () => {
    base.response = jest.fn();
    render(
      <Interaction
        data={{
          question: {
            type: "Second Form Object",
            dataType: "truth",
            subject: "Ellie",
            relationship: "speaks",
            canAdd: false,
            concepts: [],
            plural: false,
          },
          extraQuestions: [],
        }}
      />,
    );
    fireEvent.click(screen.getByText("Yes"));
    const submit = screen.getByTestId("submit-response");
    fireEvent.submit(submit);
    // empty strings and undefined are the sessionID and other config bits that come from the RBInteraction
    expect(base.response).toHaveBeenCalledWith(
      "",
      "",
      [{ cf: 100, object: true, relationship: "speaks", subject: "Ellie" }],
      undefined,
    );
  });

  it("A false truth can be selected and the correct data is sent to the response endpoint on submit", () => {
    base.response = jest.fn();
    render(
      <Interaction
        data={{
          question: {
            type: "Second Form Object",
            dataType: "truth",
            subject: "Ellie",
            relationship: "speaks",
            canAdd: false,
            concepts: [],
            plural: false,
          },
          extraQuestions: [],
        }}
      />,
    );
    fireEvent.click(screen.getByText("No"));
    const submit = screen.getByTestId("submit-response");
    fireEvent.submit(submit);
    // empty strings and undefined are the sessionID and other config bits that come from the RBInteraction
    expect(base.response).toHaveBeenCalledWith(
      "",
      "",
      [{ cf: 100, object: false, relationship: "speaks", subject: "Ellie" }],
      undefined,
    );
  });

  it("Selecting no truth and submitting returns unanswered:true in the response", () => {
    base.response = jest.fn();
    render(
      <Interaction
        data={{
          question: {
            type: "Second Form Object",
            dataType: "truth",
            subject: "Ellie",
            relationship: "speaks",
            canAdd: false,
            plural: false,
            allowUnknown: true,
            concepts: [],
          },
          extraQuestions: [],
        }}
      />,
    );

    const submit = screen.getByTestId("submit-response");
    fireEvent.submit(submit);
    // empty strings and undefined are the sessionID and other config bits that come from the RBInteraction
    expect(base.response).toHaveBeenCalledWith(
      "",
      "",
      [
        {
          cf: 100,
          object: undefined,
          relationship: "speaks",
          subject: "Ellie",
          unanswered: true,
        },
      ],
      undefined,
    );
  });

  it("check no subject cases for unanswered logic returns unanswered:true in the response", () => {
    base.response = jest.fn();
    render(
      <Interaction
        data={{
          question: {
            type: "Second Form Object",
            dataType: "truth",
            object: true,
            relationship: "speaks",
            canAdd: false,
            concepts: [],
            plural: false,
            allowUnknown: true,
          },
          extraQuestions: [],
        }}
      />,
    );

    const submit = screen.getByTestId("submit-response");
    fireEvent.submit(submit);
    // empty strings and undefined are the sessionID and other config bits that come from the RBInteraction
    expect(base.response).toHaveBeenCalledWith(
      "",
      "",
      [
        {
          cf: 100,
          object: true,
          relationship: "speaks",
          subject: undefined,
          unanswered: true,
        },
      ],
      undefined,
    );
  });

  it("Displays the certainty component when the data matches", () => {
    render(
      <Interaction
        data={{
          question: {
            dataType: "string",
            canAdd: false,
            plural: false,
            allowCF: true,
            concepts: [],
          },
          extraQuestions: [],
        }}
      />,
    );
    const input = screen.queryByTestId("certainty");
    expect(input).toBeTruthy();
  });

  it("Changing the certainty amends the number returned to the response", () => {
    base.response = jest.fn();
    render(
      <Interaction
        data={{
          question: {
            type: "Second Form Object",
            dataType: "number",
            subject: "Ellie",
            relationship: "speaks",
            canAdd: false,
            plural: false,
            concepts: [],
            allowCF: true,
          },
          extraQuestions: [],
        }}
      />,
    );

    const sliderInput = screen.getByTestId("certainty");
    sliderInput.getBoundingClientRect = jest.fn(() => {
      return {
        bottom: 286.22918701171875,
        height: 28,
        left: 19.572917938232422,
        right: 583.0937919616699,
        top: 258.22918701171875,
        width: 563.5208740234375,
        x: 19.572917938232422,
        y: 258.22918701171875,
      };
    });
    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: 122 },
    });
    fireEvent.mouseDown(sliderInput, { clientX: 162, clientY: 302 });

    const submit = screen.getByTestId("submit-response");
    fireEvent.submit(submit);
    // empty strings and undefined are the sessionID and other config bits that come from the RBInteraction
    expect(base.response).toHaveBeenCalledWith(
      "",
      "",
      [{ cf: 25, object: "122", relationship: "speaks", subject: "Ellie" }],
      undefined,
    );
  });

  describe("with extra questions", () => {
    it("Displays the single string add component when the data matches", () => {
      render(
        <Interaction
          data={{
            question: {
              dataType: "string",
              canAdd: true,
              plural: true,
              concepts: [{}],
            },
            extraQuestions: [
              {
                dataType: "string",
                canAdd: true,
                plural: false,
                concepts: [{}],
              },
            ],
          }}
        />,
      );
      const input = screen.queryByTestId("single-string-add");
      expect(input).toBeTruthy();
    });

    it("Displays the single string add component with no insts when the data matches", () => {
      render(
        <Interaction
          data={{
            question: {
              dataType: "string",
              canAdd: true,
              plural: true,
              concepts: [],
            },
            extraQuestions: [
              { dataType: "string", canAdd: true, plural: false, concepts: [] },
            ],
          }}
        />,
      );
      const input = screen.queryByTestId("single-string-add-noInsts");
      expect(input).toBeTruthy();
    });

    it("Displays the single string component when the data matches", () => {
      render(
        <Interaction
          data={{
            question: {
              dataType: "string",
              concepts: [],
              canAdd: true,
              plural: true,
            },
            extraQuestions: [
              {
                dataType: "string",
                concepts: [],
                canAdd: false,
                plural: false,
              },
            ],
          }}
        />,
      );
      const input = screen.queryByTestId("single-string");
      expect(input).toBeTruthy();
    });

    it("Displays the multi string add component when the data matches", () => {
      render(
        <Interaction
          data={{
            question: {
              dataType: "string",
              concepts: [],
              canAdd: false,
              plural: true,
            },
            extraQuestions: [
              { dataType: "string", canAdd: true, concepts: [], plural: true },
            ],
          }}
        />,
      );
      const input = screen.queryByTestId("multi-string-add");
      expect(input).toBeTruthy();
    });

    it("Displays the multi number add component when the data matches", () => {
      render(
        <Interaction
          data={{
            question: {
              dataType: "number",
              concepts: [],
              canAdd: false,
              plural: true,
            },
            extraQuestions: [
              { dataType: "number", concepts: [], canAdd: true, plural: true },
            ],
          }}
        />,
      );
      const input = screen.queryByTestId("multi-number-add");
      expect(input).toBeTruthy();
    });

    it("Displays the multi string component when the data matches", () => {
      render(
        <Interaction
          data={{
            question: {
              dataType: "date",
              concepts: [],
              canAdd: false,
              plural: false,
            },
            extraQuestions: [
              { dataType: "string", concepts: [], canAdd: false, plural: true },
            ],
          }}
        />,
      );
      const input = screen.queryByTestId("multi-string");
      expect(input).toBeTruthy();
    });

    it("Displays the multi number component when the data matches", () => {
      render(
        <Interaction
          data={{
            question: {
              dataType: "number",
              canAdd: false,
              concepts: [],
              plural: false,
            },
            extraQuestions: [
              { dataType: "number", canAdd: false, concepts: [], plural: true },
            ],
          }}
        />,
      );
      const input = screen.queryByTestId("multi-number");
      expect(input).toBeTruthy();
    });

    it("Displays the single date component when the data matches", () => {
      render(
        <Interaction
          data={{
            question: {
              concepts: [],
              dataType: "number",
              canAdd: false,
              plural: false,
            },
            extraQuestions: [
              { concepts: [], dataType: "date", canAdd: false, plural: false },
            ],
          }}
        />,
      );
      const input = screen.queryByTestId("single-date");
      expect(input).toBeTruthy();
    });

    it("Displays the multi date component when the data matches", () => {
      render(
        <Interaction
          data={{
            question: {
              concepts: [],
              dataType: "number",
              canAdd: false,
              plural: false,
            },
            extraQuestions: [
              { concepts: [], dataType: "date", canAdd: true, plural: true },
            ],
          }}
        />,
      );
      const input = screen.queryByTestId("multi-date");
      expect(input).toBeTruthy();
    });

    it("Displays the single number component when the data matches", () => {
      render(
        <Interaction
          data={{
            question: {
              concepts: [],
              dataType: "truth",
              canAdd: false,
              plural: false,
            },
            extraQuestions: [
              {
                concepts: [],
                dataType: "number",
                canAdd: false,
                plural: false,
              },
            ],
          }}
        />,
      );
      const input = screen.queryByTestId("single-number");
      expect(input).toBeTruthy();
    });

    it("Displays the single truth component when the data matches", () => {
      render(
        <Interaction
          data={{
            question: {
              dataType: "string",
              canAdd: false,
              plural: false,
              allowCF: true,
              concepts: [],
            },
            extraQuestions: [
              { concepts: [], dataType: "truth", canAdd: false, plural: false },
            ],
          }}
        />,
      );
      const input = screen.queryByTestId("single-truth");
      expect(input).toBeTruthy();
    });

    it("Displays the certainty component when the data matches", () => {
      render(
        <Interaction
          data={{
            question: {
              concepts: [],
              dataType: "string",
              canAdd: true,
              plural: true,
            },
            extraQuestions: [
              {
                dataType: "string",
                canAdd: false,
                plural: false,
                allowCF: true,
                concepts: [],
              },
            ],
          }}
        />,
      );
      const input = screen.queryByTestId("certainty");
      expect(input).toBeTruthy();
    });

    it("Submit calls respond with extra questions with the correct data", () => {
      base.response = jest.fn();
      render(
        <Interaction
          data={{
            question: {
              type: "Second Form Object",
              concepts: [{ name: "France", value: "France" }],
              dataType: "string",
              canAdd: true,
              plural: false,
              subject: "Ellie",
              relationship: "speaks",
            },
            extraQuestions: [
              {
                type: "Second Form Object",
                dataType: "number",
                subject: "Ellie",
                relationship: "speaks",
                canAdd: false,
                concepts: [],
                plural: false,
                object: "122.482",
              },
            ],
          }}
        />,
      );

      fireEvent.change(screen.getAllByPlaceholderText("Answer")[0], {
        target: { value: "fr" },
      });
      fireEvent.click(screen.getByText("France"));
      const submit = screen.getByTestId("submit-response");
      fireEvent.submit(submit);
      // empty strings and undefined are the sessionID and other config bits that come from the RBInteraction
      expect(base.response).toHaveBeenCalledWith(
        "",
        "",
        [
          {
            cf: 100,
            object: "France",
            relationship: "speaks",
            subject: "Ellie",
          },
          {
            cf: 100,
            object: "122.482",
            relationship: "speaks",
            subject: "Ellie",
          },
        ],
        undefined,
      );
    });

    it("Selecting an entry from the single string component and submitting calls respond with the correct data", () => {
      base.response = jest.fn();
      render(
        <Interaction
          data={{
            question: {
              type: "Second Form Object",
              concepts: [{ name: "France", value: "France" }],
              dataType: "string",
              canAdd: false,
              plural: false,
              subject: "Ellie",
              relationship: "speaks",
              object: "France",
            },
            extraQuestions: [
              {
                type: "Second Form Object",
                concepts: [{ name: "England", value: "England" }],
                dataType: "string",
                canAdd: false,
                plural: false,
                subject: "Ellie",
                relationship: "speaks",
              },
            ],
          }}
        />,
      );

      fireEvent.change(screen.getAllByPlaceholderText("Answer")[1], {
        target: { value: "eng" },
      });
      fireEvent.click(screen.getByText("England"));
      const submit = screen.getByTestId("submit-response");
      fireEvent.submit(submit);
      // empty strings and undefined are the sessionID and other config bits that come from the RBInteraction
      expect(base.response).toHaveBeenCalledWith(
        "",
        "",
        [
          {
            cf: 100,
            object: "France",
            relationship: "speaks",
            subject: "Ellie",
          },
          {
            cf: 100,
            object: "England",
            relationship: "speaks",
            subject: "Ellie",
          },
        ],
        undefined,
      );
    });

    it("Check submit state between active and disabled", () => {
      base.response = jest.fn();
      const data = {
        extraQuestions: [],
        question: {
          type: "Second Form Object",
          dataType: "string",
          subject: "England",
          relationship: "has national language",
          canAdd: true,
          plural: true,
          allowCF: true,
          allowUnknown: false,
          prompt: "Which language is the national language of England?",
          concepts: [
            {
              conceptType: "Language",
              name: "English",
              type: "string",
              value: "English",
            },
            {
              conceptType: "Language",
              name: "French",
              type: "string",
              value: "French",
            },
          ],
        },
      };
      const { rerender } = render(<Interaction data={data} />);
      const submit = screen.getByTestId("submit-response");
      expect(submit.disabled).toBeTruthy();

      data.question.knownAnswers = [
        {
          subject: "England",
          object: "English",
          cf: 100,
        },
      ];

      rerender(<Interaction data={data} />);
      expect(submit.disabled).toBeFalsy();
    });
  });

  it("Don't trigger responses with invalid dates", () => {
    base.response = jest.fn();

    const { container } = render(
      <Interaction
        data={{
          question: {
            type: "Second Form Object",
            dataType: "date",
            subject: "David",
            relationship: "speaks",
            canAdd: false,
            plural: false,
            concepts: [],
          },
          extraQuestions: [],
        }}
      />,
    );
    const submit = screen.getByTestId("submit-response");

    fireEvent.change(container.querySelector('[id="dateInput"]'), {
      target: { value: "13/09/198" },
    });
    fireEvent.submit(submit);

    expect(base.response).not.toHaveBeenCalled();
  });

  it("Handle date responses with timezone adjustments", () => {
    base.response = jest.fn();

    const { container } = render(
      <Interaction
        data={{
          question: {
            type: "Second Form Object",
            dataType: "date",
            subject: "David",
            relationship: "speaks",
            canAdd: false,
            plural: false,
            concepts: [],
          },
          extraQuestions: [],
        }}
      />,
    );
    const submit = screen.getByTestId("submit-response");

    fireEvent.change(container.querySelector('[id="dateInput"]'), {
      target: { value: "13/09/1989" },
    });
    fireEvent.submit(submit);

    expect(base.response).toHaveBeenCalledWith(
      "",
      "",
      [
        {
          cf: 100,
          object: 621648000000,
          relationship: "speaks",
          subject: "David",
        },
      ],
      undefined,
    );
  });

  it("Typing a custom entry into the single string add component and pressing enter responds with the correct data", () => {
    base.response = jest.fn();
    const data = {
      question: {
        canAdd: true,
        concepts: [{ name: "France", value: "France" }],
        dataType: "string",
        plural: false,
        relationship: "speaks",
        subject: "David",
        type: "Second Form Object",
      },
      extraQuestions: [],
    };
    render(<Interaction data={data} />);

    fireEvent.change(screen.getByPlaceholderText("Answer"), {
      target: { value: "england" },
    });
    fireEvent.keyDown(screen.getByPlaceholderText("Answer"), {
      key: "Enter",
      code: "Enter",
      keyCode: 13,
      charCode: 13,
    });
    const submit = screen.getByTestId("submit-response");
    fireEvent.submit(submit);
    // empty strings and undefined are the sessionID and other config bits that come from the RBInteraction
    expect(base.response).toHaveBeenCalledWith(
      "",
      "",
      [
        {
          cf: 100,
          object: "england",
          relationship: "speaks",
          subject: "David",
        },
      ],
      undefined,
    );
  });

  it("Typing custom entries into a multi string add component and pressing enter responds with the correct data", () => {
    base.response = jest.fn();
    const data = {
      question: {
        canAdd: true,
        concepts: [{ name: "Zimbabwe", value: "Zimbabwe" }],
        dataType: "string",
        plural: true,
        relationship: "speaks",
        subject: "David",
        type: "Second Form Object",
      },
      extraQuestions: [],
    };
    render(<Interaction data={data} />);

    const answer = screen.getByPlaceholderText("Answer");
    const submit = screen.getByTestId("submit-response");
    const enter = { key: "Enter", code: "Enter", keyCode: 13, charCode: 13 };

    fireEvent.change(answer, { target: { value: "zi" } });
    fireEvent.click(screen.getByText("Zimbabwe"));
    fireEvent.change(answer, { target: { value: "Germany" } });
    fireEvent.keyDown(answer, enter);
    fireEvent.submit(submit);

    // empty strings and undefined are the sessionID and other config bits that come from the RBInteraction
    expect(base.response).toHaveBeenCalledWith(
      "",
      "",
      [
        {
          cf: 100,
          object: "Zimbabwe",
          relationship: "speaks",
          subject: "David",
        },
        {
          cf: 100,
          object: "Germany",
          relationship: "speaks",
          subject: "David",
        },
      ],
      undefined,
    );
  });
});
