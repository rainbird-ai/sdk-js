import { Alert } from "@material-ui/lab";
import { FormControl, useResponse, useUndo } from "@rainbird/sdk-react";
import { Snackbar, Typography, Box, Grid, Button } from "@material-ui/core";
import dayjs from "dayjs";
import dayjsPluginUTC from "dayjs-plugin-utc";
import React, { useState } from "react";
import ReactMarkdown from "react-markdown";

import {
  Certainty,
  InputBase,
  MultiDate,
  MultiNumber,
  MultiNumberAdd,
  MultiString,
  MultiStringAdd,
  SingleDate,
  SingleNumber,
  SingleString,
  SingleStringAdd,
  SingleTruth,
} from "../Select";
import { copyToClipboard, getOptions } from "../../utils/utils";

dayjs.extend(dayjsPluginUTC);

/**
 * No Array Index In Keys:
 * Questions don't have ID's, so using index is acceptable provided we're not
 * reordering or adding/deleting items.
 */

export const Interaction = ({
  allowCustomSession = false,
  data,
  error,
  reset,
  sessionID,
}) => {
  const { question, extraQuestions } = data;
  const respond = useResponse();
  const undo = useUndo();

  const initialState = data.question
    ? [
        [
          {
            cf: 100,
            object: question.object,
            relationship: question.relationship,
            subject: question.subject,
          },
        ],
        ...extraQuestions.map(({ subject, relationship, object }) => [
          { subject, relationship, object, cf: 100 },
        ]),
      ]
    : undefined;
  const [response, setResponse] = useState(initialState);

  const getQuestionTarget = (q) => {
    const qType = q.type && q.type.toLowerCase();
    if (qType === "first form") return "answer";
    return qType === "second form object" ? "object" : "subject";
  };

  const submitDisabled =
    response &&
    response
      .map((group, i) =>
        group
          .map((r) => {
            const q = i > 0 ? extraQuestions[i - 1] : question;
            const target = r[getQuestionTarget(q)];
            const knownAnswers =
              Array.isArray(q.knownAnswers) && q.knownAnswers.length;
            if ((target === undefined && q.allowUnknown) || knownAnswers)
              return false;
            if (target === undefined) return true;
            return false;
          })
          .includes(true),
      )
      .includes(true);

  const flattenResponse = () =>
    response.reduce((result, g) => result.concat(g), []);

  const submit = (e) => {
    e.preventDefault();
    if (submitDisabled) return;
    const res = flattenResponse().map((r) => {
      const item = r;
      const unanswered =
        (!r.subject && typeof r.subject !== "boolean") ||
        (!r.object && typeof r.object !== "boolean");
      if (unanswered) item.unanswered = true;
      return item;
    });
    respond(res);
  };

  const handleSkip = () =>
    respond([
      {
        cf: 100,
        object: question.object || "",
        relationship: question.relationship,
        subject: question.subject || "",
        unanswered: true,
      },
      ...extraQuestions.map(({ subject, relationship, object }) => [
        {
          cf: 100,
          object,
          relationship,
          subject,
          unanswered: true,
        },
      ]),
    ]);

  // Rebuild res and update the target question group
  const handleResChange = (group, i) =>
    setResponse((prevRes) =>
      prevRes.map((prevGroup, j) => (i === j ? group : prevGroup)),
    );

  return (
    <form
      style={{
        alignItems: "center",
        display: "flex",
        height: "100%",
        justifyContent: "center",
        overflow: "auto",
      }}
      onSubmit={submit}
    >
      {error && (
        <Snackbar open anchorOrigin={{ horizontal: "center", vertical: "top" }}>
          <Alert data-testid="error" severity="error">
            {error.map((e) => (
              <Typography key={e}>{e}</Typography>
            ))}
          </Alert>
        </Snackbar>
      )}
      {data && data.question && (
        <Grid
          container
          direction="column"
          justify="space-between"
          style={{ height: "100%" }}
          wrap="nowrap"
        >
          <BoxContainer
            question={question}
            response={response[0]}
            handleResChange={(g) => handleResChange(g, 0)}
            target={getQuestionTarget(question)}
          />
          {extraQuestions &&
            extraQuestions.map((q, i) => (
              <BoxContainer
                // eslint-disable-next-line react/no-array-index-key
                key={`extraQuestionGroup-${i}`}
                question={q}
                response={response[i + 1]}
                handleResChange={(g) => handleResChange(g, i + 1)}
                target={getQuestionTarget(question)}
              />
            ))}
          <Grid container direction="row" justify="space-between">
            <Grid>
              <Button
                aria-label="Back to previous question"
                color="primary"
                data-testid="undo-response"
                onClick={undo}
                title="Back to previous question"
                type="button"
                variant="outlined"
              >
                Back
                <span style={{ display: "none" }}> to previous question</span>
              </Button>
              {reset !== undefined ? (
                <Button
                  aria-label="Reset back to configuration"
                  color="primary"
                  data-testid="interaction-reset-response"
                  onClick={reset}
                  style={{ marginLeft: "10px" }}
                  title="Reset back to configuration"
                  type="button"
                  variant="outlined"
                >
                  Reset
                  <span style={{ display: "none" }}>
                    {" "}
                    back to configuration
                  </span>
                </Button>
              ) : null}
              {allowCustomSession ? (
                <Button
                  aria-label="Copy session ID"
                  color="primary"
                  data-testid="interaction-copy-session-id-response"
                  onClick={() => copyToClipboard(sessionID)}
                  style={{ marginLeft: "10px" }}
                  title="Copy session ID"
                  type="button"
                  variant="outlined"
                >
                  Copy Session ID
                </Button>
              ) : null}
            </Grid>
            <Grid>
              {question.allowUnknown && (
                <Button
                  color="primary"
                  data-testid="skip-question"
                  onClick={handleSkip}
                  type="button"
                  variant="outlined"
                >
                  Skip
                </Button>
              )}
              <Button
                aria-disabled={submitDisabled}
                aria-label="Submit response"
                color="primary"
                data-testid="submit-response"
                disabled={submitDisabled}
                style={{ marginLeft: "10px" }}
                title="Submit response"
                type="submit"
                variant="contained"
              >
                Submit
                <span style={{ display: "none" }}> response</span>
              </Button>
            </Grid>
          </Grid>
        </Grid>
      )}
    </form>
  );
};

const BoxContainer = ({ handleResChange, question, response, target }) => {
  const getValue = response[0][target] ? new Date(response[0][target]) : null;

  // Replace item in the response array on change
  const handleSimpleChange = (_, newValue) => {
    const newResponse = response[0];
    newResponse[target] = (newValue && newValue.value) || undefined;
    return handleResChange([newResponse]);
  };

  // Replace multi-item in the response array on change
  const handleMultiChange = (_, v) => {
    if (v.length === 0) {
      let newResponse = response[0];
      newResponse = [
        {
          subject: question.subject,
          relationship: question.relationship,
          object: question.object,
          cf: 100,
        },
      ];
      return handleResChange(newResponse);
    }

    const uniqueItems = Array.from(new Set(v.map((i) => i.value))).map((name) =>
      v.find((i) => i.value === name),
    );
    const newResponse = uniqueItems.map((inst) => ({
      object: question.object || inst.value,
      relationship: question.relationship,
      subject: question.subject || inst.value,
      cf: response[0] && response[0].cf,
    }));
    return handleResChange(newResponse);
  };

  const handleSingleTruthOnChange = (change) => {
    const { value } = change.target;
    const valueIsTrue = value === "true";
    const answer = valueIsTrue ? "yes" : "no";

    // If the answer is first form, we want to reply yes/no to answer
    const isFirstForm =
      question && question.type && question.type.toLowerCase() === "first form";

    const newResponse = response[0];
    newResponse[target] = isFirstForm ? answer : valueIsTrue;
    return handleResChange([newResponse]);
  };

  const handleChange = (timestamp) => {
    // Escape if no timestamp recorded.
    if (!timestamp) return;

    const newResponse = response[0];
    newResponse[target] = timestamp;

    handleResChange([newResponse]);
  };

  const handleCertaintyChange = (_, v) =>
    handleResChange(response.map((r) => ({ ...r, cf: v })));

  return (
    <Box>
      {question && question.prompt && (
        <Typography
          component="h2"
          data-testid="prompt"
          id="prompt"
          variant="body1"
        >
          <ReactMarkdown>{question.prompt}</ReactMarkdown>
        </Typography>
      )}
      <Grid
        container
        direction="column"
        justify="space-between"
        style={{ width: "calc(100% - 2.4rem)" }}
      >
        <Box paddingTop={2} paddingBottom={2}>
          <FormControl
            data={question}
            singleStringAdd={
              question.concepts && question.concepts.length > 0 ? (
                <SingleStringAdd
                  data-testid="single-string-add"
                  value={response[0][target]}
                  options={getOptions(question.concepts)}
                  onChange={handleSimpleChange}
                />
              ) : (
                <InputBase
                  fullWidth
                  onChange={(e) => handleSimpleChange("", e.target)}
                  value={response[0][target]}
                  data-testid="single-string-add-noInsts"
                />
              )
            }
            singleString={
              <SingleString
                data-testid="single-string"
                value={response[0][target]}
                options={getOptions(question.concepts)}
                onChange={handleSimpleChange}
              />
            }
            multiStringAdd={
              <MultiStringAdd
                data-testid="multi-string-add"
                question={question}
                onChange={handleMultiChange}
                response={response}
                target={target}
              />
            }
            multiString={
              <MultiString
                data-testid="multi-string"
                question={question}
                onChange={handleMultiChange}
                response={response}
                target={target}
              />
            }
            singleDate={
              <SingleDate
                minDate={new Date("1000/01/01")}
                maxDate={new Date("5000/12/31")}
                data-testid="single-date"
                format="DD/MM/YYYY"
                initialFocusedDate={new Date()}
                value={getValue}
                placeholder="dd/mm/yyyy"
                onChange={handleChange}
              />
            }
            multiDate={
              <MultiDate
                data-testid="multi-date"
                question={question}
                response={response}
                onChange={handleMultiChange}
                target={target}
              />
            }
            singleNumber={
              <SingleNumber
                data-testid="single-number"
                value={response[0][target]}
                onChange={(e) => {
                  const newResponse = response[0];
                  newResponse[target] = e.target.value;
                  return handleResChange([newResponse]);
                }}
              />
            }
            multiNumber={
              <MultiNumber
                data-testid="multi-number"
                question={question}
                onChange={handleMultiChange}
                response={response}
                target={target}
              />
            }
            multiNumberAdd={
              <MultiNumberAdd
                data-testid="multi-number-add"
                question={question}
                onChange={handleMultiChange}
                response={response}
                target={target}
              />
            }
            singleTruth={
              <SingleTruth
                data-testid="single-truth"
                onChange={handleSingleTruthOnChange}
                value={response[0][target]}
              />
            }
            certaintySelect={
              <Box paddingTop={3}>
                <Certainty
                  data-testid="certainty"
                  value={response[0].cf}
                  onChange={handleCertaintyChange}
                />
              </Box>
            }
            fallback={<p>something went wrong</p>}
          />
        </Box>
      </Grid>
    </Box>
  );
};
