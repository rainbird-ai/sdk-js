import React from "react";
import { Interaction } from ".";

// eslint-disable-next-line import/no-default-export
export default {
  title: "Interaction",
  component: Interaction,
  argTypes: {
    data: { type: "object" },
  },
};

const Template = (args) => <Interaction {...args} />;

export const Default = Template.bind({});
Default.args = {
  data: {
    question: {
      prompt: "What is the speaks of Ellie?",
      type: "Second Form Object",
      concepts: [{ name: "France", value: "France" }],
      dataType: "string",
      canAdd: true,
      plural: false,
      subject: "Ellie",
      relationship: "speaks",
    },
    extraQuestions: [],
  },
  reset: () => {},
};
