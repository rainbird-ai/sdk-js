import React from "react";
import { screen, render } from "@testing-library/react";
import { ErrorBoundary } from "..";

describe("Error Boundary", () => {
  it("If an error is thrown in the children, then the onError prop is called", () => {
    const ErrorComponent = () => {
      throw new Error("Error thrown");
    };

    render(
      <ErrorBoundary
        onError={(err) => <p data-testid="errmessage">{err.message}</p>}
      >
        <ErrorComponent />
      </ErrorBoundary>,
    );

    const err = screen.getByTestId("errmessage");
    expect(err.textContent).toEqual("Error thrown");
  });
});
