import React from "react";
import { screen, render } from "@testing-library/react";
import { ErrorPage } from "..";

describe("Error Page", () => {
  it("The refresh and report buttons call the correct props", () => {
    const onReset = jest.fn();
    render(
      <ErrorPage onReset={onReset} error={{ message: "An error occurred" }} />,
    );
    screen.getByTestId("error-reset").click();

    expect(onReset).toHaveBeenCalled();
  });
});
