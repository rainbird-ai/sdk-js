import React from "react";
import { Typography, Box, Button } from "@material-ui/core";

export const ErrorPage = ({ onReset, error }) => (
  <Box
    padding={3}
    display="flex"
    justifyContent="center"
    alignItems="center"
    flexDirection="column"
  >
    <Typography data-testid="error-title" variant="h5">
      An error occurred!
    </Typography>
    <Typography data-testid="error-message" variant="caption">
      {error.message}
    </Typography>

    <Box padding={3}>
      <Button
        aria-label="Reset component"
        color="primary"
        data-testid="error-reset"
        onClick={onReset}
        title="Reset"
        type="button"
        style={{ marginRight: "10px" }}
        variant="outlined"
      >
        Reset
      </Button>
    </Box>
  </Box>
);
