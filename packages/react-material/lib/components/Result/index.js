import { RainbirdContext, useUndo } from "@rainbird/sdk-react";
import { Typography, Button, Grid } from "@material-ui/core";
import Info from "@material-ui/icons/Info";
import React, { useContext } from "react";
import ReactMarkdown from "react-markdown";
import Tooltip from "@material-ui/core/Tooltip";

import { constructEvidenceURL, copyToClipboard } from "../../utils/utils";

/**
 * No Array Index In Keys:
 * Neither the results or metadata from data have ID's, so using index is
 * acceptable provided we're not reordering or adding/deleting items.
 */

export const Result = ({
  allowCustomSession = false,
  allowUndoResults = false,
  appURL,
  data = [],
  displayEvidence = true,
  evidenceKey,
  reset,
  sessionID,
}) => (
  <Grid
    alignItems="center"
    container
    direction="column"
    style={{
      height: "100%",
      position: "relative",
      paddingBottom: "4rem",
    }}
  >
    <Grid
      container
      direction="column"
      justifyContent="flex-start"
      role="main"
      wrap="nowrap"
      style={{ overflow: "auto" }}
    >
      <ResultHeader resultsExist={data.length} />
      <ul>
        {data.map((result, index) => (
          <ResultItem
            appURL={appURL}
            displayEvidence={displayEvidence}
            evidenceKey={evidenceKey}
            factID={data[index].factID}
            key={`result-${index}`} // eslint-disable-line react/no-array-index-key
            result={result}
          />
        ))}
      </ul>
    </Grid>
    <Buttons
      allowCustomSession={allowCustomSession}
      allowUndo={allowUndoResults}
      reset={reset}
      sessionID={sessionID}
    />
  </Grid>
);

const ResultHeader = ({ resultsExist }) =>
  resultsExist ? (
    <Typography
      aria-level="2"
      data-testid="result-header"
      role="heading"
      variant="h5"
    >
      Your results:
    </Typography>
  ) : (
    <Typography data-testid="error">
      Sorry I&apos;ve been unable to find an answer to your question!
    </Typography>
  );

const ResultItem = ({
  appURL,
  displayEvidence,
  evidenceKey,
  factID,
  result,
}) => {
  const { certainty, object, objectMetadata, relationship, subject } = result;
  const { baseURL, sessionID } = useContext(RainbirdContext);
  const evidenceURL = constructEvidenceURL(factID, appURL, baseURL, sessionID);

  return (
    <li>
      <Typography
        aria-label="Results"
        data-testid="result"
        style={{ lineHeight: "1.7", position: "relative" }}
      >
        {`${subject} ${relationship} ${object} - ${certainty}%`}
        {displayEvidence && (
          <EvidenceLink url={evidenceURL} evidenceKey={evidenceKey} />
        )}
      </Typography>
      {objectMetadata && objectMetadata.en && (
        <ObjectMetaData metaData={objectMetadata.en} />
      )}
    </li>
  );
};

const EvidenceLink = ({ url, evidenceKey }) => (
  <Tooltip title="Show Evidence Tree" placement="top">
    <form
      data-testid="evidence"
      style={{ display: "inline", verticalAlign: "middle" }}
      name="evidence"
      action={url}
      method="POST"
    >
      <input type="hidden" name="key" value={evidenceKey} />
      <button
        style={{ background: "white", border: "none" }}
        type="submit"
        formTarget="_blank"
      >
        <Info />
      </button>
    </form>
  </Tooltip>
);

const ObjectMetaData = ({ metaData }) => (
  <Typography
    aria-label="Object Metadata"
    component="div"
    data-testid="object-metadata"
    style={{
      clear: "both",
      margin: "10px 0",
    }}
  >
    {metaData.map(({ dataType, data }, index) => (
      <div
        key={`object-metadata-${index}`} // eslint-disable-line react/no-array-index-key
      >
        {dataType === "md" ? <ReactMarkdown>{data}</ReactMarkdown> : data}
      </div>
    ))}
  </Typography>
);

const Buttons = ({ reset, allowUndo, sessionID, allowCustomSession }) => {
  const undo = useUndo();
  return (
    <Grid
      container
      direction="row"
      justifyContent="flex-start"
      style={{ bottom: "0", position: "absolute" }}
    >
      {allowUndo ? (
        <Button
          aria-label="Back to previous question"
          color="primary"
          data-testid="undo-response"
          onClick={undo}
          title="Back to previous question"
          type="button"
          variant="outlined"
        >
          Back
          <span style={{ display: "none" }}>to previous question</span>
        </Button>
      ) : (
        <div />
      )}
      {reset ? (
        <Button
          aria-label="Reset back to configuration"
          color="primary"
          data-testid="reset-response"
          onClick={reset}
          style={{ marginLeft: "10px" }}
          title="Reset back to configuration"
          type="button"
          variant="outlined"
        >
          Reset
          <span style={{ display: "none" }}>back to configuration</span>
        </Button>
      ) : (
        <div />
      )}
      {allowCustomSession ? (
        <Button
          aria-label="Copy session ID"
          color="primary"
          data-testid="interaction-copy-session-id-response"
          onClick={() => copyToClipboard(sessionID)}
          style={{ marginLeft: "10px" }}
          title="Copy session ID"
          type="button"
          variant="outlined"
        >
          Copy Session ID
        </Button>
      ) : null}
    </Grid>
  );
};
