import React from "react";
import { Result } from ".";

// eslint-disable-next-line import/no-default-export
export default {
  argTypes: { data: { type: "array" } },
  component: Result,
  title: "Result",
};

const Template = (args) => <Result {...args} />;

export const Default = Template.bind({});
export const SingleResult = Template.bind({});
export const MultipleResults = Template.bind({});
export const Error = Template.bind({});
export const ResetEnabled = Template.bind({});
export const BackEnabled = Template.bind({});
export const EvidenceEnabled = Template.bind({});
export const ObjectMetaDataEnabled = Template.bind({});

Default.args = {
  data: [
    {
      certainty: 75,
      factID:
        "WA:RF:dfde01bc36e9fd58139040826e30c0bfdc77a1242f2183f498c6657f5c7afbce",
      object: "English",
      objectMetadata: {},
      relationship: "speaks",
      relationshipType: "speaks",
      subject: "David",
    },
  ],
};

SingleResult.args = {
  displayEvidence: false,
  data: [
    {
      certainty: 75,
      factID:
        "WA:RF:dfde01bc36e9fd58139040826e30c0bfdc77a1242f2183f498c6657f5c7afbce",
      object: "English",
      objectMetadata: {},
      relationship: "speaks",
      relationshipType: "speaks",
      subject: "David",
    },
  ],
};

MultipleResults.args = {
  displayEvidence: false,
  data: [
    {
      certainty: 75,
      factID:
        "WA:RF:dfde01bc36e9fd58139040826e30c0bfdc77a1242f2183f498c6657f5c7afbce",
      object: "English",
      objectMetadata: {},
      relationship: "speaks",
      relationshipType: "speaks",
      subject: "David",
    },
    {
      certainty: 50,
      factID:
        "WA:RF:dfde01bc36e9fd58139040826e30c0bfdc77a1242f2183f498c6657f5c7afbce",
      object: "French",
      objectMetadata: {},
      relationship: "speaks",
      relationshipType: "speaks",
      subject: "David",
    },
    {
      certainty: 85,
      factID:
        "WA:RF:dfde01bc36e9fd58139040826e30c0bfdc77a1242f2183f498c6657f5c7afbce",
      object: "Spanish",
      objectMetadata: {},
      relationship: "speaks",
      relationshipType: "speaks",
      subject: "David",
    },
  ],
};

Error.args = {
  displayEvidence: false,
  data: [],
};

ResetEnabled.args = {
  displayEvidence: false,
  reset: () => {},
  data: [
    {
      certainty: 75,
      factID:
        "WA:RF:dfde01bc36e9fd58139040826e30c0bfdc77a1242f2183f498c6657f5c7afbce",
      object: "English",
      objectMetadata: {},
      relationship: "speaks",
      relationshipType: "speaks",
      subject: "David",
    },
  ],
};

BackEnabled.args = {
  displayEvidence: false,
  allowUndoResults: true,
  data: [
    {
      certainty: 75,
      factID:
        "WA:RF:dfde01bc36e9fd58139040826e30c0bfdc77a1242f2183f498c6657f5c7afbce",
      object: "English",
      objectMetadata: {},
      relationship: "speaks",
      relationshipType: "speaks",
      subject: "David",
    },
  ],
};

EvidenceEnabled.args = {
  data: [
    {
      certainty: 75,
      factID:
        "WA:RF:dfde01bc36e9fd58139040826e30c0bfdc77a1242f2183f498c6657f5c7afbce",
      object: "English",
      objectMetadata: {},
      relationship: "speaks",
      relationshipType: "speaks",
      subject: "David",
    },
  ],
};

ObjectMetaDataEnabled.args = {
  displayEvidence: false,
  data: [
    {
      certainty: 75,
      factID:
        "WA:RF:dfde01bc36e9fd58139040826e30c0bfdc77a1242f2183f498c6657f5c7afbce",
      object: "English",
      objectMetadata: {
        en: [
          {
            dataType: "md",
            data: "A West Germanic language that was first spoken in early medieval England and is now a global lingua franca.",
          },
          {
            dataType: "md",
            data: `A paragraph with *emphasis* and **strong importance**.`,
          },
        ],
      },
      relationship: "speaks",
      relationshipType: "speaks",
      subject: "David",
    },
  ],
};
