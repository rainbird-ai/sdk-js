import { render, screen, fireEvent, waitFor } from "@testing-library/react";
import React from "react";

import { Result } from "..";

jest.mock("@rainbird/sdk");

const {
  getByText,
  getByTestId: getByTestID,
  queryAllByTestId: queryAllByTestID,
  queryByTestId: queryByTestID,
  getByRole,
} = screen;
const { mouseOver, click } = fireEvent;
const MOCK_RESULT = [
  {
    certainty: 75,
    factID:
      "WA:RF:dfde01bc36e9fd58139040826e30c0bfdc77a1242f2183f498c6657f5c7afbce",
    object: "English",
    objectMetadata: {
      en: [
        {
          dataType: "md",
          data: "A West Germanic language that was first spoken in early medieval England and is now a global lingua franca.",
        },
      ],
    },
    relationship: "speaks",
    relationshipType: "speaks",
    subject: "David",
  },
];

describe("<Result />", () => {
  it("Renders no results from data", () => {
    render(<Result />);

    const results = queryAllByTestID("result");
    expect(results).toHaveLength(0);
  });

  it("Renders a singleton result from data", () => {
    render(<Result data={MOCK_RESULT} />);

    const results = queryAllByTestID("result");
    expect(results).toHaveLength(1);
  });

  it("Renders multiple results from data", () => {
    render(<Result data={[MOCK_RESULT[0], MOCK_RESULT[0], MOCK_RESULT[0]]} />);

    const results = queryAllByTestID("result");
    expect(results).toHaveLength(3);
  });

  describe("<ResultHeader />", () => {
    it("Displays results header if a result exists", () => {
      render(<Result data={MOCK_RESULT} />);

      const header = getByTestID("result-header");
      expect(header).toBeTruthy();
    });

    it("Displays results header if results exist", () => {
      render(
        <Result data={[MOCK_RESULT[0], MOCK_RESULT[0], MOCK_RESULT[0]]} />,
      );

      const header = getByTestID("result-header");
      expect(header).toBeTruthy();
    });

    it("Displays error if data exists but results are empty", () => {
      render(<Result data={[]} />);

      const { textContent: error } = getByTestID("error");
      expect(error).toEqual(
        "Sorry I've been unable to find an answer to your question!",
      );
    });

    it("Displays error when data doesn't exist", () => {
      render(<Result />);

      const { textContent: error } = getByTestID("error");
      expect(error).toEqual(
        "Sorry I've been unable to find an answer to your question!",
      );
    });

    it("Displays correct header copy", () => {
      render(<Result data={MOCK_RESULT} />);

      const { textContent: header } = getByTestID("result-header");
      expect(header).toEqual("Your results:");
    });

    it("Displays correct error copy", () => {
      render(<Result data={[]} />);

      const { textContent: error } = getByTestID("error");
      expect(error).toEqual(
        "Sorry I've been unable to find an answer to your question!",
      );
    });
  });

  describe("<ResultItem />", () => {
    it("Renders a single result", () => {
      render(<Result data={MOCK_RESULT} />);

      const result = getByTestID("result");
      expect(result).toBeTruthy();
    });

    it("Displays result string as `Subject Relationship Object - Certainty`", () => {
      render(<Result data={MOCK_RESULT} />);

      const { textContent: result } = getByTestID("result");
      expect(result).toEqual("David speaks English - 75%");
    });

    it("Displays evidence icon as default", () => {
      render(<Result data={MOCK_RESULT} />);

      const evidence = getByTestID("evidence");
      expect(evidence).toBeTruthy();
    });

    it("Hides evidence icon when displayEvidence is false", () => {
      render(<Result data={MOCK_RESULT} displayEvidence={false} />);

      const evidence = queryByTestID("evidence");
      expect(evidence).toBeNull();
    });

    it("Renders object metadata if present in the result", () => {
      render(<Result data={MOCK_RESULT} />);

      const metadata = getByTestID("object-metadata");
      expect(metadata).toBeTruthy();
    });
  });

  describe("<EvidenceLink />", () => {
    it("Shows correct mouseover copy", async () => {
      render(<Result data={MOCK_RESULT} />);

      mouseOver(getByTestID("evidence"));
      await waitFor(() => getByText("Show Evidence Tree"));

      const tooltip = getByText("Show Evidence Tree");
      expect(tooltip).toBeTruthy();
    });

    it("Correctly provides correct evidence href prefix", async () => {
      render(<Result appURL="mockURL" data={MOCK_RESULT} />);

      const form = getByRole("form");
      const expectedURL =
        "mockURL/evidence?id=WA:RF:dfde01bc36e9fd58139040826e30c0bfdc77a1242f2183f498c6657f5c7afbce";
      expect(form.getAttribute("action").startsWith(expectedURL)).toBeTruthy();
    });
  });

  describe("<Buttons />", () => {
    it("Renders reset button if reset function is provided", async () => {
      const reset = jest.fn();
      render(<Result reset={reset} />);

      const button = getByTestID("reset-response");
      expect(button).toBeTruthy();
    });

    it("Renders no reset button if no function provided", async () => {
      render(<Result />);

      const button = queryByTestID("reset-response");
      expect(button).toBeNull();
    });

    it("Clicking reset calls the provided function", async () => {
      const reset = jest.fn();
      render(<Result reset={reset} />);

      click(getByTestID("reset-response"));
      expect(reset.mock.calls.length).toBe(1);
    });
  });

  it("Renders back button if allowUndoResults is true", async () => {
    render(<Result allowUndoResults />);

    const button = getByTestID("undo-response");
    expect(button).toBeTruthy();
  });

  it("Renders no back button if allowUndoResults is false", async () => {
    render(<Result allowUndoResults={false} />);

    const button = queryByTestID("undo-response");
    expect(button).toBeNull();
  });
});
