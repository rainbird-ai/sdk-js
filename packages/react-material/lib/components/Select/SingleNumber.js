import React from "react";
import { TextField } from "@material-ui/core";

export const SingleNumber = ({ onChange, value, ...props }) => (
  // label is associated with control but linting falls over
  // eslint-disable-next-line jsx-a11y/label-has-associated-control
  <label aria-label="number input" htmlFor="numberInput">
    <TextField
      {...props}
      aria-label="Number input"
      fullWidth
      id="numberInput"
      inputProps={{ step: "any" }}
      onChange={onChange}
      placeholder="Answer"
      title="Number input"
      type="number"
      value={value}
    />
  </label>
);
