import React, { useCallback } from "react";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import DayJsUtils from "@date-io/dayjs";

import { convertToISO8601 } from "../../utils/utils";

export const SingleDate = ({
  ariaLabel = "Date input",
  id = "dateInput",
  onChange,
  title = "Date input",
  value,
  ...props
}) => {
  // Allow dates in the local timezone but display as ISO8601 strings.
  const handleChange = useCallback(
    (localDate) => {
      const isoDate = convertToISO8601(localDate);
      if (isoDate) onChange(Date.parse(convertToISO8601(localDate)));
    },
    [onChange],
  );

  return (
    // label is associated with control but linting falls over
    // eslint-disable-next-line jsx-a11y/label-has-associated-control
    <label htmlFor={id} aria-label={ariaLabel}>
      <MuiPickersUtilsProvider utils={DayJsUtils}>
        <KeyboardDatePicker
          aria-label={ariaLabel}
          fullWidth
          id={id}
          onChange={handleChange}
          title={title}
          value={value}
          {...props}
        />
      </MuiPickersUtilsProvider>
    </label>
  );
};
