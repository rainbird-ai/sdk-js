import React, { useCallback, useState } from "react";
import { Autocomplete, createFilterOptions } from "@material-ui/lab";

import { InputBase } from "./InputBase";

const ENTER_KEY_CODE = 13;

const filter = createFilterOptions();

// Returns the option if selected with enter from input, else an add user
// provided option. Regular option is fallback.
const getOptionLabel = (option) => {
  if (typeof option === "string") return option;
  if (option.overrideTitle) return option.overrideTitle;
  return option.name;
};

// Provide filtered options formatted for MUI Autocomplete.
const filterOptions = (opts, params) => {
  const filtered = filter(opts, params);

  if (params.inputValue !== "")
    filtered.push({
      name: `Add "${params.inputValue}"`,
      overrideTitle: params.inputValue,
      type: "string",
      value: params.inputValue,
    });

  return filtered;
};

export const SingleStringAdd = ({ options, value, onChange, ...props }) => {
  const [menuOpen, setMenuOpen] = useState(false);

  // On enter, simulate MUI Autocomplete selection.
  const onKeyDown = useCallback(
    (event) => {
      const {
        keyCode,
        target: { value: input },
      } = event;

      if (keyCode === ENTER_KEY_CODE && input) {
        if (menuOpen) event.preventDefault();
        onChange(null, {
          name: `Add "${input}"`,
          overrideTitle: input,
          type: "string",
          value: input,
        });
        setMenuOpen(false);
      }
    },
    [menuOpen],
  );

  // Handle dynamic user generated inputs without resorting to controlling
  // internal value in MUI Autocomplete.
  const renderInput = useCallback(
    (params) => {
      if (params.inputProps.value === "") {
        const newParams = {
          ...params,
          inputProps: { ...params.inputProps, value },
        };
        return <InputBase {...newParams} />;
      }
      return <InputBase {...params} />;
    },
    [value],
  );

  return (
    <Autocomplete
      aria-label="Single string select"
      filterOptions={filterOptions}
      getOptionLabel={getOptionLabel}
      onChange={onChange}
      onClose={() => setMenuOpen(false)}
      onKeyDown={onKeyDown}
      onOpen={() => setMenuOpen(true)}
      open={menuOpen}
      options={options || []}
      renderInput={renderInput}
      renderOption={(option) => option.name}
      title="Single string select"
      value={value}
      {...props}
    />
  );
};
