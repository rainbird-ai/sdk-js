import React, { useState } from "react";
import { TextField, Chip, makeStyles } from "@material-ui/core";
import { Autocomplete } from "@material-ui/lab";
import { Event } from "@material-ui/icons";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
  Day,
} from "@material-ui/pickers";
import DayJsUtils from "@date-io/dayjs";
import dayjs from "dayjs";
import dayjsPluginUTC from "dayjs-plugin-utc";
import { toTimestampAtStartOfDay } from "../../utils/utils";

dayjs.extend(dayjsPluginUTC);

const useStyles = makeStyles({
  container: {
    display: "flex",
  },
  autocomplete: {
    width: "100%",
  },
  endAdornment: {
    top: "auto",
  },
  popupIndicator: {
    padding: "6px",
  },
});

const toOption = (timestamp) => {
  return {
    name: new Date(timestamp).toLocaleDateString(),
    value: timestamp,
  };
};

export const MultiDate = ({
  question,
  response,
  onChange,
  target,
  title = "Multi Date Input",
  id = "multi-date-input",
  ...props
}) => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);

  const knownAnswers =
    question.knownAnswers && question.knownAnswers[0]
      ? question.knownAnswers.map((a) =>
          a[target] ? toOption(a[target]) : null,
        )
      : [];
  const knownNames = knownAnswers.map((ka) => ka.name);

  const value =
    response[0] && response[0][target]
      ? response.map((r) => toOption(r[target]))
      : [];

  const values = [...knownAnswers, ...value];
  const dateNames = values.map((v) => v.name);

  const renderInput = (params) => (
    <TextField
      {...params}
      label="Answer"
      variant="outlined"
      placeholder="Answer"
    />
  );

  const renderTags = (v, getTagProps) =>
    v.map((option, index) => (
      <Chip
        data-testid="md-chip"
        label={option.name}
        {...getTagProps({ index })}
        disabled={knownNames.includes(option.name)}
      />
    ));

  const renderDays = (day, selectedDate, isInCurrentMonths, dayComponent) => {
    const curr = new Date(day);
    if (dateNames.includes(curr.toLocaleDateString())) {
      return <Day selected>{curr.getDate()}</Day>;
    }
    return dayComponent;
  };

  const handleChange = (e, v) =>
    onChange(
      e,
      v.filter((o) => typeof o !== "string" && !knownNames.includes(o.name)),
    );

  return (
    <div
      {...props}
      className={classes.container}
      id={id}
      aria-label={title}
      title={title}
    >
      <Autocomplete
        className={classes.autocomplete}
        classes={{
          endAdornment: classes.endAdornment,
          popupIndicator: classes.popupIndicator,
        }}
        options={[]}
        value={values}
        multiple
        getOptionLabel={(opt) => opt.name}
        onChange={handleChange}
        renderInput={renderInput}
        renderTags={renderTags}
        onOpen={() => setOpen(true)}
        popupIcon={<Event fontSize="small" />}
      />
      <MuiPickersUtilsProvider utils={DayJsUtils}>
        <KeyboardDatePicker
          minDate={new Date("1000/01/01")}
          maxDate={new Date("5000/12/31")}
          open={open}
          onOpen={() => setOpen(true)}
          onClose={() => setOpen(false)}
          TextFieldComponent={() => null}
          format="DD/MM/YYYY"
          placeholder="dd/mm/yyyy"
          onChange={(d) => {
            const localeWithTimestamp = toTimestampAtStartOfDay(d);
            const newValue = {
              name: localeWithTimestamp.locale,
              value: localeWithTimestamp.timestamp,
            };
            if (dateNames.includes(newValue.name)) {
              return handleChange(null, value);
            }
            return handleChange(null, [...value, newValue]);
          }}
          renderDay={renderDays}
        />
      </MuiPickersUtilsProvider>
    </div>
  );
};
