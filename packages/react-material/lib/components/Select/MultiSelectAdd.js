import { Autocomplete, createFilterOptions } from "@material-ui/lab";
import { TextField, Chip } from "@material-ui/core";
import React, { useCallback } from "react";

import { maybeToString, getFactSuggestions } from "../../utils/utils";

const ENTER_KEY_CODE = 13;

const filter = createFilterOptions();

const MultiSelectAdd = ({
  title,
  type,
  onChange,
  question,
  response,
  target,
  ...props
}) => {
  // ToDo refactor this component and MultiSelect to share more code.
  const knownAnswers =
    question.knownAnswers && question?.knownAnswers[0]
      ? question.knownAnswers.map((a) => maybeToString(a[target]))
      : [];
  const opts = getFactSuggestions(question);
  const value =
    response[0] && response[0][target]
      ? response.map((r) => ({
          name: maybeToString(r[target]),
          value: maybeToString(r[target]),
        }))
      : [];

  const handleChange = (e, v) => {
    onChange(
      e,
      v.filter((o) => typeof o !== "string" && !knownAnswers.includes(o.value)),
    );
  };

  // On enter, simulate MUI Autocomplete selection.
  const onKeyDown = useCallback(
    (event) => {
      const {
        keyCode,
        target: { value: input },
      } = event;

      if (keyCode === ENTER_KEY_CODE && input) {
        event.preventDefault();
        handleChange(null, [
          ...value,
          {
            known: false,
            overrideTitle: `Add "${input}"`,
            value: input,
          },
        ]);
      }
    },
    [handleChange, value],
  );

  const renderInput = (params) => (
    <TextField
      {...params}
      label="Answer"
      onKeyDown={onKeyDown}
      variant="outlined"
      placeholder="Answer"
    />
  );

  const getTags = (tagValue, getTagProps) =>
    tagValue.map((option, index) => (
      <Chip
        label={option.name}
        {...getTagProps({ index })}
        disabled={knownAnswers.includes(option.name)}
      />
    ));

  const filterOptions = (options, params) => {
    const filtered = filter(options, params);

    if (params.inputValue !== "") {
      filtered.push({
        known: false,
        overrideTitle: `Add "${params.inputValue}"`,
        value: params.inputValue,
      });
    }

    return filtered;
  };

  return (
    <Autocomplete
      title={title}
      aria-label={title}
      multiple
      value={[...opts.filter((o) => knownAnswers.includes(o.value)), ...value]}
      options={opts}
      openOnFocus
      getOptionLabel={(option) => {
        // Value selected with enter, right from the input
        if (typeof option === "string") {
          return option;
        }

        // // Add "xxx" option created dynamically
        if (option.overrideTitle) {
          return option.overrideTitle;
        }
        // Regular option
        return option.name;
      }}
      getOptionSelected={(opt, v) => opt.name === v.name}
      onChange={handleChange}
      filterOptions={filterOptions}
      renderTags={getTags}
      renderInput={renderInput}
      {...props}
    />
  );
};

const optionsFilter = (options, params, inputCheck) => {
  const filtered = filter(options, params);

  if (params.inputValue !== "" && inputCheck) {
    filtered.push({
      known: false,
      overrideTitle: `Add "${params.inputValue}"`,
      value: params.inputValue,
    });
  }

  return filtered;
};

const filterAllOptions = (options, params) =>
  optionsFilter(options, params, true);

const filterNumberOptions = (options, params) =>
  optionsFilter(options, params, !Number.isNaN(Number(params.inputValue)));

export const MultiStringAdd = ({
  onChange,
  question,
  response,
  target,
  filterOptions,
  ...props
}) =>
  MultiSelectAdd({
    title: "Multi string select",
    onChange,
    question,
    response,
    target,
    filterOptions: filterAllOptions,
    ...props,
  });

export const MultiNumberAdd = ({
  onChange,
  question,
  response,
  target,
  filterOptions,
  ...props
}) =>
  MultiSelectAdd({
    title: "Multi number select",
    onChange,
    question,
    response,
    target,
    filterOptions: filterNumberOptions,
    ...props,
  });
