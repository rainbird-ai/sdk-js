export const maybeToString = (maybeValue) =>
  maybeValue ? maybeValue.toString() : maybeValue;

// Convert a date in local-time to an ISO-8601 Date string.
export const convertToISO8601 = (local) => {
  const date = new Date(local);

  // Escape if not valid.
  if (Number.isNaN(date.valueOf())) return null;

  // Picker selects dates in local time, not in UTC. If we use toISOString(),
  // that would convert to UTC and then to String; but if we convert to UTC,
  // that changes the date. Bypass using ISO-8601 until picker lib is updated.
  const year = String(date.getFullYear());
  const month = String(101 + date.getMonth()).substring(1);
  const day = String(100 + date.getDate()).substring(1);

  return `${year}-${month}-${day}`;
};

export const toTimestampAtStartOfDay = (dateString) => {
  const date = new Date(dateString);
  date.setUTCHours(0, 0, 0, 0);
  return {
    locale: date.toLocaleDateString(),
    timestamp: date.valueOf(),
  };
};

export const constructEvidenceURL = (factID, appURL, baseURL, sessionID) =>
  `${appURL}/evidence?id=${factID}&api=${baseURL}&sid=${sessionID}`;

// Filter out options already known by the engine.
export const getOptions = (concepts) => {
  // Guard against missing question options.
  if (!concepts) return [];

  return concepts.filter((concept) => !concept.invalidResponse);
};

export const copyToClipboard = (value) => {
  // Create element in document, so we have something to copy from.
  const dummyTextArea = document.createElement("textarea");
  document.body.appendChild(dummyTextArea);

  // Load the session ID.
  dummyTextArea.value = value;
  dummyTextArea.select();

  // Copy then remove from document.
  document.execCommand("copy");
  document.body.removeChild(dummyTextArea);
};

export const getFactSuggestions = ({ dataType, concepts }) => {
  // We don't want to provide suggestions for number facts.
  if (dataType === "number") return [];

  // Fallback, questions should always have concepts.
  if (!Array.isArray(concepts)) return [];

  return concepts.map(({ value }) => ({
    name: maybeToString(value),
    value: maybeToString(value),
  }));
};
