import {
  constructEvidenceURL,
  convertToISO8601,
  getFactSuggestions,
  getOptions,
  toTimestampAtStartOfDay,
} from "./utils";

describe("utils", () => {
  it("converts date to timestamp at start of day", () => {
    const date = "2021-12-01";
    const startOfDay = toTimestampAtStartOfDay(date);
    expect(startOfDay.timestamp).toEqual(1638316800000);
  });

  it("creates a correct evidence url to the evidence tree for community url", () => {
    const baseURL = "https://api.rainbird.ai";
    const appURL = "https://app.rainbird.ai";
    const sessionID = "1aaaaa11-2222-33c3-4dd4-55555555";
    const factID = "WA:RF:aaabbbccc";

    const evidenceURL = constructEvidenceURL(
      factID,
      appURL,
      baseURL,
      sessionID,
    );
    expect(evidenceURL).toEqual(
      "https://app.rainbird.ai/evidence?id=WA:RF:aaabbbccc&api=https://api.rainbird.ai&sid=1aaaaa11-2222-33c3-4dd4-55555555",
    );
  });

  describe("getOptions()", () => {
    it("correctly filters out concepts where invalidResponse is present", () => {
      const concepts = [
        {
          conceptType: "Person",
          invalidResponse: true,
          name: "David",
          type: "string",
          value: "David",
        },
        {
          conceptType: "Person",
          invalidResponse: false,
          name: "Christiaan",
          type: "string",
          value: "Christiaan",
        },
        {
          conceptType: "Person",
          invalidResponse: true,
          name: "Dan",
          type: "string",
          value: "Dan",
        },
        {
          conceptType: "Person",
          invalidResponse: false,
          name: "James",
          type: "string",
          value: "James",
        },
        {
          conceptType: "Person",
          invalidResponse: true,
          name: "Mathias",
          type: "string",
          value: "Mathias",
        },
      ];
      const expectedResult = [
        {
          conceptType: "Person",
          invalidResponse: false,
          name: "Christiaan",
          type: "string",
          value: "Christiaan",
        },
        {
          conceptType: "Person",
          invalidResponse: false,
          name: "James",
          type: "string",
          value: "James",
        },
      ];

      const result = getOptions(concepts);
      expect(result.length).toEqual(2);
      expect(result).toEqual(expectedResult);
    });

    it("handles missing question options", () => {
      const question = { foo: "bar" };

      const result = getOptions(question.concepts);
      expect(result.length).toEqual(0);
      expect(result).toEqual([]);
    });
  });

  describe("convertToISO8601()", () => {
    it("return null with invalid dates", () => {
      const result = convertToISO8601("13/09/198");
      expect(result).toEqual(null);
    });
  });

  describe("getFactSuggestions()", () => {
    it("returns suggestions for questions with a non-number data-type", () => {
      const question = {
        dataType: "string",
        concepts: [
          {
            conceptType: "Country",
            name: "England",
            type: "string",
            value: "England",
          },
          {
            conceptType: "Country",
            name: "France",
            type: "string",
            value: "France",
          },
        ],
      };
      const result = getFactSuggestions(question);

      expect(result.length).toEqual(2);
      expect(result).toEqual([
        { name: "England", value: "England" },
        { name: "France", value: "France" },
      ]);
    });

    it("returns no suggestions for questions with a number data-type", () => {
      const question = {
        dataType: "number",
        concepts: [
          { conceptType: "Age", name: 10, type: "number", value: 10 },
          { conceptType: "Age", name: 40, type: "number", value: 40 },
          { conceptType: "Age", name: 70, type: "number", value: 70 },
        ],
      };
      const result = getFactSuggestions(question);

      expect(result.length).toEqual(0);
      expect(result).toEqual([]);
    });

    it("returns no suggestions for questions with no concepts", () => {
      const question = { dataType: "string", concepts: [] };
      const result = getFactSuggestions(question);

      expect(result.length).toEqual(0);
      expect(result).toEqual([]);
    });
  });
});
